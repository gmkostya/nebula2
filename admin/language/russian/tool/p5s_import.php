<?php
// Heading
$_['heading_title']    = 'Импорт товаров P5s.com.ua';

// Text
$_['text_backup']      = 'Download Backup';
$_['text_success']     = 'Восстановление таблиц прошло успешно!';
$_['text_list']        = 'Upload List';

// Entry
$_['entry_config_p5s_import_key']     = 'Ключ API';
$_['entry_name']       = 'Название';
$_['entry_limit']      = 'Лимит';
$_['entry_image']      = 'Изображение (Ширина x Высота)';
$_['entry_width']      = 'Ширина';
$_['entry_height']     = 'Высота';
$_['entry_status']     = 'Статус';

// Entry
$_['button_import_products']     = 'Импорт товаров c обновлением фото';
$_['button_import_products_without_image']     = 'Импорт товаров без обновления фото';
$_['button_import_products_prices']     = 'Обновление Цен';
$_['button_import_products_stock']     = 'Обновление остатков';
$_['button_import_categories']     = 'Импорт категорий ';

// Error
$_['error_permission'] = 'У Вас нет прав на управление резервной копией!';
$_['error_backup']     = 'Для резервного копирования необходимо выбрать хотя бы одну таблицу!';
$_['error_empty']      = 'Внимание: Загруженный файл пустой!';