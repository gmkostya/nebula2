<?php echo $oc_header ?>
<div class="tab-pane active" id="tab-settings">
    <div class="row">
        <div class="col-sm-2">
            <ul class="nav nav-pills nav-stacked" id="tabs-vertical">
                <li class="active"><a href="#tab-info" data-toggle="tab"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $tab_instruction; ?></a></li>
                <?php $group_row = 1; ?>
                <?php /*foreach ($groups as $group) { ?>
                    <li><a href="#tab-group<?php echo $group_row; ?>" data-toggle="tab"><i class="fa fa-minus-circle" onclick="$('#tabs-vertical a:first').tab('show'); $('#tabs-vertical a[href=\'#tab-group<?php echo $group_row; ?>\']').parent().remove(); $('#tab-address<?php echo $group_row; ?>').remove();"></i> <?php echo $tab_group . ' ' . $group_row; ?></a></li>
                    <?php $group_row++; ?>
                <?php }*/ ?>
                <li id="group-add"><a onclick="addOcGroup();"><i class="fa fa-plus-circle" onclick=""></i>&nbsp;<?php echo $add_oc_group; ?></a></li>
            </ul>
        </div>
        <div class="col-sm-10">
            <div class="tab-content">
                <div class="tab-pane active" id="tab-info">
                    <div class="form-group">
                        Інструкція заповнення
                    </div>
                </div>
                <?php /*
                <div class="tab-pane" id="tab-header">
                    <div class="form-group">
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="field<?php echo $for?>">Телефон 1</label>
                            <div class="col-sm-10">
                                <input type="text" id="field<?php echo $for?>" name="wsh[settings][header][header_phone1]" value="<?php echo $wsh['settings']['header']['header_phone1']; ?>" class="form-control" />
                            </div>
                            <?php $for++; ?>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="field<?php echo $for?>">Телефон иконка 1</label>
                            <div class="col-sm-10">
                                <input type="text" id="field<?php echo $for?>" name="wsh[settings][header][header_phone_icon1]" value="<?php echo $wsh['settings']['header']['header_phone_icon1']; ?>" class="form-control" />
                            </div>
                            <?php $for++; ?>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="field<?php echo $for?>">Телефон 2</label>
                            <div class="col-sm-10">
                                <input type="text" id="field<?php echo $for?>" name="wsh[settings][header][header_phone2]" value="<?php echo $wsh['settings']['header']['header_phone2']; ?>" class="form-control" />
                            </div>
                            <?php $for++; ?>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="field<?php echo $for?>">Телефон иконка 2</label>
                            <div class="col-sm-10">
                                <input type="text" id="field<?php echo $for?>" name="wsh[settings][header][header_phone_icon2]" value="<?php echo $wsh['settings']['header']['header_phone_icon2']; ?>" class="form-control" />
                            </div>
                            <?php $for++; ?>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="field<?php echo $for?>">Email:</label>
                            <div class="col-sm-10">
                                <input type="text" id="field<?php echo $for?>" name="wsh[settings][header][header_email]" value="<?php echo $wsh['settings']['header']['header_email']; ?>" class="form-control" />
                            </div>
                            <?php $for++; ?>
                        </div>
                        <ul class="nav nav-tabs" id="languages<?php echo $lan; ?>">
                            <?php foreach ($languages as $language) { ?>
                                <li><a href="#language<?php echo $lan; ?>_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                            <?php } ?>
                        </ul>
                        <div class="tab-content">
                            <?php foreach ($languages as $language) { ?>
                                <div class="tab-pane" id="language<?php echo $lan; ?>_<?php echo $language['language_id']; ?>">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="field<?php echo $for?>">Тест</label>
                                        <div class="col-sm-10">
                                            <input type="text" id="field<?php echo $for?>" name="wsh[settings][header][langs][<?php echo $language['language_id']; ?>][header_seo]" value="<?php echo $wsh['settings']['header']['langs'][$language['language_id']]['header_seo']; ?>" class="form-control" />
                                            <?php $for++; ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php $lan++; ?>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab-footer">
                    <div class="form-group">
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="field<?php echo $for?>">URL на Twitter</label>
                            <div class="col-sm-10">
                                <input type="text" id="field<?php echo $for?>" name="wsh[settings][footer][social_twitter]" value="<?php echo $wsh['settings']['footer']['social_twitter']; ?>" class="form-control" />
                            </div>
                            <?php $for++; ?>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="field<?php echo $for?>">URL на Facebook</label>
                            <div class="col-sm-10">
                                <input type="text" id="field<?php echo $for?>" name="wsh[settings][footer][social_facebook]" value="<?php echo $wsh['settings']['footer']['social_facebook']; ?>" class="form-control" />
                            </div>
                            <?php $for++; ?>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="field<?php echo $for?>">URL на ВКонтакте</label>
                            <div class="col-sm-10">
                                <input type="text" id="field<?php echo $for?>" name="wsh[settings][footer][social_vk]" value="<?php echo $wsh['settings']['footer']['social_vk']; ?>" class="form-control" />
                            </div>
                            <?php $for++; ?>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="field<?php echo $for?>">URL на Однокласники</label>
                            <div class="col-sm-10">
                                <input type="text" id="field<?php echo $for?>" name="wsh[settings][footer][social_ok]" value="<?php echo $wsh['settings']['footer']['social_ok']; ?>" class="form-control" />
                            </div>
                            <?php $for++; ?>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="field<?php echo $for?>">Код метрики</label>
                            <div class="col-sm-10">
                                <textarea id="field<?php echo $for?>" name="wsh[settings][footer][metrics]" class="form-control" style="height: 150px"><?php echo $wsh['settings']['footer']['metrics']; ?></textarea>
                            </div>
                            <?php $for++; ?>
                        </div>
                        <ul class="nav nav-tabs" id="languages<?php echo $lan; ?>">
                            <?php foreach ($languages as $language) { ?>
                                <li><a href="#language<?php echo $lan; ?>_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                            <?php } ?>
                        </ul>
                        <div class="tab-content">
                            <?php foreach ($languages as $language) { ?>
                                <div class="tab-pane" id="language<?php echo $lan; ?>_<?php echo $language['language_id']; ?>">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="field<?php echo $for?>">Заголовок "Контакты"</label>
                                        <div class="col-sm-10">
                                            <input type="text" id="field<?php echo $for?>" name="wsh[settings][footer][langs][<?php echo $language['language_id']; ?>][footer_title1]" value="<?php echo $wsh['settings']['footer']['langs'][$language['language_id']]['footer_title1']; ?>" class="form-control" />
                                        </div>
                                        <?php $for++; ?>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="field<?php echo $for?>">Текст Адрес</label>
                                        <div class="col-sm-10">
                                            <input type="text" id="field<?php echo $for?>" name="wsh[settings][footer][langs][<?php echo $language['language_id']; ?>][footer_address]" value="<?php echo $wsh['settings']['footer']['langs'][$language['language_id']]['footer_address']; ?>" class="form-control" />
                                        </div>
                                        <?php $for++; ?>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="field<?php echo $for?>">Текст Наш телефон</label>
                                        <div class="col-sm-10">
                                            <input type="text" id="field<?php echo $for?>" name="wsh[settings][footer][langs][<?php echo $language['language_id']; ?>][footer_phone]" value="<?php echo $wsh['settings']['footer']['langs'][$language['language_id']]['footer_phone']; ?>" class="form-control" />
                                        </div>
                                        <?php $for++; ?>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="field<?php echo $for?>">Текст Email</label>
                                        <div class="col-sm-10">
                                            <input type="text" id="field<?php echo $for?>" name="wsh[settings][footer][langs][<?php echo $language['language_id']; ?>][footer_email]" value="<?php echo $wsh['settings']['footer']['langs'][$language['language_id']]['footer_email']; ?>" class="form-control" />
                                        </div>
                                        <?php $for++; ?>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="field<?php echo $for?>">Заголовок "В помощь"</label>
                                        <div class="col-sm-10">
                                            <input type="text" id="field<?php echo $for?>" name="wsh[settings][footer][langs][<?php echo $language['language_id']; ?>][footer_title2]" value="<?php echo $wsh['settings']['footer']['langs'][$language['language_id']]['footer_title2']; ?>" class="form-control" />
                                        </div>
                                        <?php $for++; ?>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="field<?php echo $for?>">Заголовок "Следуйте за нами"</label>
                                        <div class="col-sm-10">
                                            <input type="text" id="field<?php echo $for?>" name="wsh[settings][footer][langs][<?php echo $language['language_id']; ?>][footer_title3]" value="<?php echo $wsh['settings']['footer']['langs'][$language['language_id']]['footer_title3']; ?>" class="form-control" />
                                        </div>
                                        <?php $for++; ?>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="field<?php echo $for?>">Копирайт</label>
                                        <div class="col-sm-10">
                                            <input type="text" id="field<?php echo $for?>" name="wsh[settings][footer][langs][<?php echo $language['language_id']; ?>][copyright]" value="<?php echo $wsh['settings']['footer']['langs'][$language['language_id']]['copyright']; ?>" class="form-control" />
                                        </div>
                                        <?php $for++; ?>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="field<?php echo $for?>">SEO строчка в футере</label>
                                        <div class="col-sm-10">
                                            <input type="text" id="field<?php echo $for?>" name="wsh[settings][footer][langs][<?php echo $language['language_id']; ?>][footer_seo]" value="<?php echo $wsh['settings']['footer']['langs'][$language['language_id']]['home_seo']; ?>" class="form-control" />
                                        </div>
                                        <?php $for++; ?>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php $lan++; ?>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab-home">
                    <div class="form-group">
                        <ul class="nav nav-tabs" id="languages<?php echo $lan; ?>">
                            <?php foreach ($languages as $language) { ?>
                                <li><a href="#language<?php echo $lan; ?>_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                            <?php } ?>
                        </ul>
                        <div class="tab-content">
                            <?php foreach ($languages as $language) { ?>
                                <div class="tab-pane" id="language<?php echo $lan; ?>_<?php echo $language['language_id']; ?>">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="field<?php echo $for?>">Сео текст</label>
                                        <div class="col-sm-10">
                                            <textarea id="field<?php echo $for?>" name="wsh[settings][home][langs][<?php echo $language['language_id']; ?>][seo_text]" class="form-control summernote" style="height: 150px"><?php echo $wsh['settings']['home']['langs'][$language['language_id']]['seo_text']; ?></textarea>
                                        </div>
                                        <?php $for++; ?>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="field<?php echo $for?>">Сео меню</label>
                                        <div class="col-sm-10">
                                            <textarea id="field<?php echo $for?>" name="wsh[settings][home][langs][<?php echo $language['language_id']; ?>][seo_menu]" class="form-control summernote" style="height: 150px"><?php echo $wsh['settings']['home']['langs'][$language['language_id']]['seo_menu']; ?></textarea>
                                        </div>
                                        <?php $for++; ?>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php $lan++; ?>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab-404">
                    <div class="form-group">
                        <ul class="nav nav-tabs" id="languages<?php echo $lan; ?>">
                            <?php foreach ($languages as $language) { ?>
                                <li><a href="#language<?php echo $lan; ?>_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                            <?php } ?>
                        </ul>
                        <div class="tab-content">
                            <?php foreach ($languages as $language) { ?>
                                <div class="tab-pane" id="language<?php echo $lan; ?>_<?php echo $language['language_id']; ?>">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="field<?php echo $for?>">Заголовок страницы</label>
                                        <div class="col-sm-10">
                                            <input type="text" id="field<?php echo $for?>" name="wsh[settings][not_found][langs][<?php echo $language['language_id']; ?>][not_found_title]" value="<?php echo $wsh['settings']['not_found']['langs'][$language['language_id']]['not_found_title']; ?>" class="form-control" />
                                        </div>
                                        <?php $for++; ?>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="field<?php echo $for?>">Текст страницы</label>
                                        <div class="col-sm-10">
                                            <input type="text" id="field<?php echo $for?>" name="wsh[settings][not_found][langs][<?php echo $language['language_id']; ?>][not_found_text]" value="<?php echo $wsh['settings']['not_found']['langs'][$language['language_id']]['not_found_text']; ?>" class="form-control" />
                                        </div>
                                        <?php $for++; ?>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php $lan++; ?>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab-wework">
                    <div class="form-group">
                        <ul class="nav nav-tabs" id="languages<?php echo $lan; ?>">
                            <?php foreach ($languages as $language) { ?>
                                <li><a href="#language<?php echo $lan; ?>_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                            <?php } ?>
                        </ul>
                        <div class="tab-content">
                            <?php foreach ($languages as $language) { ?>
                                <div class="tab-pane" id="language<?php echo $lan; ?>_<?php echo $language['language_id']; ?>">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="field<?php echo $for?>">Сео меню</label>
                                        <div class="col-sm-10">
                                            <textarea id="field<?php echo $for?>" name="wsh[settings][wework][langs][<?php echo $language['language_id']; ?>][seo_menu]" class="form-control summernote" style="height: 150px"><?php echo $wsh['settings']['wework']['langs'][$language['language_id']]['seo_menu']; ?></textarea>
                                        </div>
                                        <?php $for++; ?>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php $lan++; ?>
                        </div>
                    </div>
                </div>*/?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var group_row = <?php echo $group_row; ?>;
    function addOcGroup() {
        html  = '<div class="tab-pane" id="tab-group' + group_row + '">';
        html += '  <input type="hidden" name="address[' + group_row + '][address_id]" value="" />';

        html += '  <div class="form-group required">';
        html += '    <label class="col-sm-2 control-label" for="input-firstname' + group_row + '"><?php echo $entry_firstname; ?></label>';
        html += '    <div class="col-sm-10"><input type="text" name="address[' + group_row + '][firstname]" value="" placeholder="<?php echo $entry_firstname; ?>" id="input-firstname' + group_row + '" class="form-control" /></div>';
        html += '  </div>';

        html += '  <div class="form-group required">';
        html += '    <label class="col-sm-2 control-label" for="input-lastname' + group_row + '"><?php echo $entry_lastname; ?></label>';
        html += '    <div class="col-sm-10"><input type="text" name="address[' + group_row + '][lastname]" value="" placeholder="<?php echo $entry_lastname; ?>" id="input-lastname' + group_row + '" class="form-control" /></div>';
        html += '  </div>';

        html += '  <div class="form-group">';
        html += '    <label class="col-sm-2 control-label" for="input-company' + group_row + '"><?php echo $entry_company; ?></label>';
        html += '    <div class="col-sm-10"><input type="text" name="address[' + group_row + '][company]" value="" placeholder="<?php echo $entry_company; ?>" id="input-company' + group_row + '" class="form-control" /></div>';
        html += '  </div>';

        html += '  <div class="form-group required">';
        html += '    <label class="col-sm-2 control-label" for="input-address-1' + group_row + '"><?php echo $entry_address_1; ?></label>';
        html += '    <div class="col-sm-10"><input type="text" name="address[' + group_row + '][address_1]" value="" placeholder="<?php echo $entry_address_1; ?>" id="input-address-1' + group_row + '" class="form-control" /></div>';
        html += '  </div>';

        html += '  <div class="form-group">';
        html += '    <label class="col-sm-2 control-label" for="input-address-2' + group_row + '"><?php echo $entry_address_2; ?></label>';
        html += '    <div class="col-sm-10"><input type="text" name="address[' + group_row + '][address_2]" value="" placeholder="<?php echo $entry_address_2; ?>" id="input-address-2' + group_row + '" class="form-control" /></div>';
        html += '  </div>';

        html += '  <div class="form-group required">';
        html += '    <label class="col-sm-2 control-label" for="input-city' + group_row + '"><?php echo $entry_city; ?></label>';
        html += '    <div class="col-sm-10"><input type="text" name="address[' + group_row + '][city]" value="" placeholder="<?php echo $entry_city; ?>" id="input-city' + group_row + '" class="form-control" /></div>';
        html += '  </div>';

        html += '  <div class="form-group required">';
        html += '    <label class="col-sm-2 control-label" for="input-postcode' + group_row + '"><?php echo $entry_postcode; ?></label>';
        html += '    <div class="col-sm-10"><input type="text" name="address[' + group_row + '][postcode]" value="" placeholder="<?php echo $entry_postcode; ?>" id="input-postcode' + group_row + '" class="form-control" /></div>';
        html += '  </div>';

        // Custom Fields
        <?php /*foreach ($custom_fields as $custom_field) { ?>
        <?php if ($custom_field['location'] == 'address') { ?>
        <?php if ($custom_field['type'] == 'select') { ?>

        html += '  <div class="form-group custom-field custom-field<?php echo $custom_field['custom_field_id']; ?>" data-sort="<?php echo $custom_field['sort_order'] + 1; ?>">';
        html += '  		<label class="col-sm-2 control-label" for="input-address' + group_row + '-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo addslashes($custom_field['name']); ?></label>';
        html += '  		<div class="col-sm-10">';
        html += '  		  <select name="address[' + group_row + '][custom_field][<?php echo $custom_field['custom_field_id']; ?>]" id="input-address' + group_row + '-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control">';
        html += '  			<option value=""><?php echo $text_select; ?></option>';

        <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
        html += '  			<option value="<?php echo $custom_field_value['custom_field_value_id']; ?>"><?php echo addslashes($custom_field_value['name']); ?></option>';
        <?php } ?>

        html += '  		  </select>';
        html += '  		</div>';
        html += '  	  </div>';
        <?php } ?>

        <?php if ($custom_field['type'] == 'radio') { ?>
        html += '  	  <div class="form-group custom-field custom-field<?php echo $custom_field['custom_field_id']; ?>">';
        html += '  		<label class="col-sm-2 control-label"><?php echo addslashes($custom_field['name']); ?></label>';
        html += '  		<div class="col-sm-10">';
        html += '  		  <div>';

        <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
        html += '  			<div class="radio"><label><input type="radio" name="address[' + group_row + '][custom_field][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" /> <?php echo addslashes($custom_field_value['name']); ?></label></div>';
        <?php } ?>

        html += '		  </div>';
        html += '		</div>';
        html += '	  </div>';
        <?php } ?>

        <?php if ($custom_field['type'] == 'checkbox') { ?>
        html += '	  <div class="form-group custom-field custom-field<?php echo $custom_field['custom_field_id']; ?>" data-sort="<?php echo $custom_field['sort_order'] + 1; ?>">';
        html += '		<label class="col-sm-2 control-label"><?php echo addslashes($custom_field['name']); ?></label>';
        html += '		<div class="col-sm-10">';
        html += '		  <div>';

        <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
        html += '			<div class="checkbox"><label><input type="checkbox" name="address[<?php echo $group_row; ?>][custom_field][<?php echo $custom_field['custom_field_id']; ?>][]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" /> <?php echo addslashes($custom_field_value['name']); ?></label></div>';
        <?php } ?>

        html += '		  </div>';
        html += '		</div>';
        html += '	  </div>';
        <?php } ?>

        <?php if ($custom_field['type'] == 'text') { ?>
        html += '	  <div class="form-group custom-field custom-field<?php echo $custom_field['custom_field_id']; ?>" data-sort="<?php echo $custom_field['sort_order'] + 1; ?>">';
        html += '		<label class="col-sm-2 control-label" for="input-address' + group_row + '-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo addslashes($custom_field['name']); ?></label>';
        html += '		<div class="col-sm-10">';
        html += '		  <input type="text" name="address[' + group_row + '][custom_field][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo addslashes($custom_field['value']); ?>" placeholder="<?php echo addslashes($custom_field['name']); ?>" id="input-address' + group_row + '-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />';
        html += '		</div>';
        html += '	  </div>';
        <?php } ?>

        <?php if ($custom_field['type'] == 'textarea') { ?>
        html += '	  <div class="form-group custom-field custom-field<?php echo $custom_field['custom_field_id']; ?>" data-sort="<?php echo $custom_field['sort_order'] + 1; ?>">';
        html += '		<label class="col-sm-2 control-label" for="input-address' + group_row + '-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo addslashes($custom_field['name']); ?></label>';
        html += '		<div class="col-sm-10">';
        html += '		  <textarea name="address[' + group_row + '][custom_field][<?php echo $custom_field['custom_field_id']; ?>]" rows="5" placeholder="<?php echo addslashes($custom_field['name']); ?>" id="input-address' + group_row + '-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control"><?php echo addslashes($custom_field['value']); ?></textarea>';
        html += '		</div>';
        html += '	  </div>';
        <?php } ?>

        <?php if ($custom_field['type'] == 'file') { ?>
        html += '	  <div class="form-group custom-field custom-field<?php echo $custom_field['custom_field_id']; ?>" data-sort="<?php echo $custom_field['sort_order'] + 1; ?>">';
        html += '		<label class="col-sm-2 control-label"><?php echo addslashes($custom_field['name']); ?></label>';
        html += '		<div class="col-sm-10">';
        html += '		  <button type="button" id="button-address' + group_row + '-custom-field<?php echo $custom_field['custom_field_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>';
        html += '		  <input type="hidden" name="address[' + group_row + '][<?php echo $custom_field['custom_field_id']; ?>]" value="" id="input-address' + group_row + '-custom-field<?php echo $custom_field['custom_field_id']; ?>" />';
        html += '		</div>';
        html += '	  </div>';
        <?php } ?>

        <?php if ($custom_field['type'] == 'date') { ?>
        html += '	  <div class="form-group custom-field custom-field<?php echo $custom_field['custom_field_id']; ?>" data-sort="<?php echo $custom_field['sort_order'] + 1; ?>">';
        html += '		<label class="col-sm-2 control-label" for="input-address' + group_row + '-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo addslashes($custom_field['name']); ?></label>';
        html += '		<div class="col-sm-10">';
        html += '		  <div class="input-group date"><input type="text" name="address[' + group_row + '][custom_field][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo addslashes($custom_field['value']); ?>" placeholder="<?php echo addslashes($custom_field['name']); ?>" data-date-format="YYYY-MM-DD" id="input-address' + group_row + '-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div>';
        html += '		</div>';
        html += '	  </div>';
        <?php } ?>

        <?php if ($custom_field['type'] == 'time') { ?>
        html += '	  <div class="form-group custom-field custom-field<?php echo $custom_field['custom_field_id']; ?>" data-sort="<?php echo $custom_field['sort_order'] + 1; ?>">';
        html += '		<label class="col-sm-2 control-label" for="input-address' + group_row + '-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo addslashes($custom_field['name']); ?></label>';
        html += '		<div class="col-sm-10">';
        html += '		  <div class="input-group time"><input type="text" name="address[' + group_row + '][custom_field][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field['value']; ?>" placeholder="<?php echo addslashes($custom_field['name']); ?>" data-date-format="HH:mm" id="input-address' + group_row + '-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div>';
        html += '		</div>';
        html += '	  </div>';
        <?php } ?>

        <?php if ($custom_field['type'] == 'datetime') { ?>
        html += '	  <div class="form-group custom-field custom-field<?php echo $custom_field['custom_field_id']; ?>" data-sort="<?php echo $custom_field['sort_order'] + 1; ?>">';
        html += '		<label class="col-sm-2 control-label" for="input-address' + group_row + '-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo addslashes($custom_field['name']); ?></label>';
        html += '		<div class="col-sm-10">';
        html += '		  <div class="input-group datetime"><input type="text" name="address[' + group_row + '][custom_field][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo addslashes($custom_field['value']); ?>" placeholder="<?php echo addslashes($custom_field['name']); ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-address' + group_row + '-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div>';
        html += '		</div>';
        html += '	  </div>';
        <?php } ?>

        <?php } ?>
        <?php }*/ ?>

        html += '  <div class="form-group">';
        html += '    <label class="col-sm-2 control-label"><?php echo $entry_default; ?></label>';
        html += '    <div class="col-sm-10"><label class="radio"><input type="radio" name="address[' + group_row + '][default]" value="1" /></label></div>';
        html += '  </div>';

        html += '</div>';

        console.log('111');
        $('#tab-settings .tab-content').append(html);

        $('#group-add').before('<li><a href="#tab-group' + group_row + '" data-toggle="tab"><i class="fa fa-minus-circle" onclick="$(\'#tab-settings a:first\').tab(\'show\'); $(\'a[href=\\\'#tab-group' + group_row + '\\\']\').parent().remove(); $(\'#tab-group' + group_row + '\').remove();"></i> <?php echo $tab_group; ?> ' + group_row + '</a></li>');

        $('#tab-settings a[href=\'#tab-group' + group_row + '\']').tab('show');

        <?php /*$('select[name=\'customer_group_id\']').trigger('change');

        $('select[name=\'address[' + group_row + '][country_id]\']').trigger('change');

        $('#address-add').before('<li><a href="#tab-address' + group_row + '" data-toggle="tab"><i class="fa fa-minus-circle" onclick="$(\'#address a:first\').tab(\'show\'); $(\'a[href=\\\'#tab-address' + group_row + '\\\']\').parent().remove(); $(\'#tab-address' + group_row + '\').remove();"></i> <?php //echo $tab_address; ?> ' + group_row + '</a></li>');

        $('#address a[href=\'#tab-address' + group_row + '\']').tab('show');

        $('.date').datetimepicker({
            pickTime: false
        });

        $('.datetime').datetimepicker({
            pickDate: true,
            pickTime: true
        });

        $('.time').datetimepicker({
            pickDate: false
        });

        $('#tab-address' + group_row + ' .form-group[data-sort]').detach().each(function() {
            if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#tab-address' + group_row + ' .form-group').length) {
                $('#tab-address' + group_row + ' .form-group').eq($(this).attr('data-sort')).before(this);
            }

            if ($(this).attr('data-sort') > $('#tab-address' + group_row + ' .form-group').length) {
                $('#tab-address' + group_row + ' .form-group:last').after(this);
            }

            if ($(this).attr('data-sort') < -$('#tab-address' + group_row + ' .form-group').length) {
                $('#tab-address' + group_row + ' .form-group:first').before(this);
            }
        });*/?>

        group_row++;
    }
</script>
<?php echo $oc_footer ?>