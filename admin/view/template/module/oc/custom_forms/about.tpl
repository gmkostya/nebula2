<?php echo $oc_header ?>
    <div class="tab-pane active" id="about-tab">
        <div class="form-group">
            <div class="form-group">
                <div class="col-sm-2"></div>
                <div class="col-sm-10">#OpenCart# - Custom Forms</div>
            </div>
            <div class="form-group">
                <div class="col-sm-2">Версія:</div>
                <div class="col-sm-10"><?php echo $_version?></div>
            </div>
            <div class="form-group">
                <div class="col-sm-2">Команда:</div>
                <div class="col-sm-10">
                    <a href="https://web-systems.solutions/" class="webstudio_link" target="_blank">
                        <span class="webstudio_link-icon">
                        <span class=""></span>
                        <span class=""></span>
                        <span class=""></span>
                        <span class=""></span>
                        </span>
                        <span class="webstudio_link-name">web-systems.solutions</span>
                    </a>
                </div>
            </div>
            <?php /*
            <div class="form-group">
                <div class="col-sm-2">Автор:</div>
                <div class="col-sm-10">Саня Петляк - (Skype: sanya_petlyak, email: <a href="mailto:sanya.petlyak@gmail.com">sanya.petlyak@gmail.com</a>)</div>
            </div>*/?>
        </div>
    </div>
<?php echo $oc_footer ?>