<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<button type="submit" form="form-category" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default">
					<i class="fa fa-reply"></i>
				</a>
			</div>
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-pencil"></i> Добавление сео данных к странице</h3>
			</div>
			<div class="panel-body">
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-category" class="form-horizontal">
					<?php if(isset($seo_page['link_id'])){ ?>
						<input type="hidden" value="<?php echo $seo_page['link_id'];?>" name="seo_page[link_id]">
					<?php } ?>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-link">Ссылка на страницу</label>
						<div class="col-sm-10">
							<input type="text" name="seo_page[link]" value="<?php echo isset($seo_page['link']) ? $seo_page['link'] : ''; ?>" placeholder="Ссылка на страницу" id="input-link" class="form-control" />
						</div>
					</div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-robots">Поле роботс</label>
                        <div class="col-sm-10">
                            <select name="seo_page[robots]" id="input-robots" class="form-control">
                                <?php foreach ($robots as $robot) { ?>
                                    <?php if ($robot['index'] == $seo_page['robots']) { ?>
                                        <option value="<?php echo $robot['index']; ?>" selected="selected"><?php echo $robot['index']; ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo $robot['index']; ?>"><?php echo $robot['index']; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>



                    <div class="tab-pane active" id="tab-general">
						<ul class="nav nav-tabs" id="language">
							<?php foreach ($languages as $language) { ?>
								<li><a href="#language<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
							<?php } ?>
						</ul>
						<div class="tab-content">
							<?php foreach ($languages as $language) { ?>
								<div class="tab-pane" id="language<?php echo $language['language_id']; ?>">
									<div class="form-group">
										<label class="col-sm-2 control-label" for="input-meta-title<?php echo $language['language_id']; ?>">Meta Title</label>
										<div class="col-sm-10">
											<input type="text" name="seo_page[item][<?php echo $language['language_id']; ?>][meta_title]" value="<?php echo isset($seo_page['item'][$language['language_id']]['meta_title']) ? $seo_page['item'][$language['language_id']]['meta_title'] : ''; ?>" placeholder="Meta Title" id="input-meta-title<?php echo $language['language_id']; ?>" class="form-control" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="input-meta-description<?php echo $language['language_id']; ?>">Meta Description</label>
										<div class="col-sm-10">
											<textarea name="seo_page[item][<?php echo $language['language_id']; ?>][meta_description]" rows="5" placeholder="Meta Description" id="input-meta-description<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($seo_page['item'][$language['language_id']]['meta_description']) ? $seo_page['item'][$language['language_id']]['meta_description'] : '' ; ?></textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="input-meta-keywords<?php echo $language['language_id']; ?>">Meta Keywords</label>
										<div class="col-sm-10">
											<input type="text" name="seo_page[item][<?php echo $language['language_id']; ?>][meta_keywords]" value="<?php echo isset($seo_page['item'][$language['language_id']]['meta_keywords']) ? $seo_page['item'][$language['language_id']]['meta_keywords'] : ''; ?>" placeholder="Meta Keywords" id="input-meta-keywords<?php echo $language['language_id']; ?>" class="form-control" />
										</div>
									</div>
								</div>
							<?php } ?>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<script type="text/javascript"><!--
		$('#language a:first').tab('show');
		//--></script></div>
<?php echo $footer; ?>