<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-save" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" form="form-backup" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-exchange"></i> <?php echo $heading_title; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-save" class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-import"><?php echo $entry_config_p5s_import_key; ?></label>
            <div class="col-sm-10">
              <input type="text" name="config_p5s_import_key" id="input-config_p5s_import_key"  value="<?php echo $config_p5s_import_key; ?>" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-import_url">Адрес источника</label>
            <div class="col-sm-10">
              <input type="text" name="config_p5s_import_url" id="input-config_p5s_import_url"  value="<?php echo $config_p5s_import_url; ?>" />
            </div>
          </div>



          <div class="form-group">
            <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Тестовый режим дроп заказов">Тестовый режим дроп заказов</span></label>
            <div class="col-sm-10">
              <label class="radio-inline">
                <?php   if ($config_p5s_import_mode) { ?>
                  <input type="radio" name="config_p5s_import_mode" value="1" checked="checked" />
                  да
                <?php } else { ?>
                  <input type="radio" name="config_p5s_import_mode" value="1" />
                  да
                <?php } ?>
              </label>
              <label class="radio-inline">
                <?php if (!$config_p5s_import_mode) { ?>
                  <input type="radio" name="config_p5s_import_mode" value="0" checked="checked" />
                  нет
                <?php } else { ?>
                  <input type="radio" name="config_p5s_import_mode" value="0" />
                  нет
                <?php } ?>
              </label>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-config_p5s_import_status"><?php echo $entry_status; ?></label>
            <div class="col-sm-10">
              <select name="config_p5s_import_status" id="input-config_p5s_import_status" class="form-control">
                <?php if ($config_p5s_import_status) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>



        </form>
         </div>
    </div>

    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-exchange"></i> <?php echo $heading_title; ?> (ручной режим)</h3>
      </div>
        <a href="<?php echo  $import_products; ?>" title="<?php echo $button_import_products; ?>" class="btn btn-default"><i class="fa fa-upload"></i><span><?php echo $button_import_products; ?></span></a>
        <a href="<?php echo  $import_products_without_image; ?>" title="<?php echo $button_import_products_without_image; ?>" class="btn btn-default"><i class="fa fa-upload"></i><span><?php echo $button_import_products_without_image; ?></span></a>
        <a href="<?php echo  $test_order; ?>"  class="btn btn-default"><i class="fa fa-upload"></i><span>test_order</span></a>
    </div>
  </div>
</div>
<?php echo $footer; ?>
