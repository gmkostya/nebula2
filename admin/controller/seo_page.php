<?php
class ControllerModuleSeoPage extends Controller {
	private $error = array();
	private function install(){
		$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "seo_page` (
			`link_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
			`link` varchar(255) NOT NULL,
			`robots` varchar(255) NOT NULL,
			PRIMARY KEY (`link_id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;
		");
		$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "seo_page_desck` (
			`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
			`link_id` int(11) unsigned,
			`language_id` int(11) unsigned,
			`meta_title` varchar(255) NOT NULL,
			`meta_description` varchar(255) NOT NULL,
			`meta_keywords` varchar(255) NOT NULL,
			PRIMARY KEY (`id`)
			) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;
		");
	}
	public function index() {
		$this->install();
		$this->load->language('module/seo_page');
		$this->document->setTitle("Сео Описание страниц");

		$this->load->model('module/seo_page');
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			if(isset($this->request->post['seo_page']['link_id']))
				$this->model_module_seo_page->editSeoPage($this->request->post['seo_page']);
			else
				$this->model_module_seo_page->addSeoPage($this->request->post['seo_page']);
			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('module/seo_page', 'token=' . $this->session->data['token'], 'SSL'));
		}
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
		);

		if (!isset($this->request->get['link_id']) && !isset($this->request->get['add'])) {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('module/seo_page', 'token=' . $this->session->data['token'], 'SSL')
			);
		} else {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('module/seo_page', 'token=' . $this->session->data['token'], 'SSL')
			);
			$data['breadcrumbs'][] = array(
				'text' => 'Модуль Seo кастомных страниц добавление',
				'href' => $this->url->link('module/seo_page', 'token=' . $this->session->data['token'] . '&add=1', 'SSL')
			);
		}


		$data['heading_title'] = $this->language->get('heading_title');

		$this->load->model('localisation/language');
		$data['languages'] = $this->model_localisation_language->getLanguages();
		$data['lang'] = $this->language->get('lang');

		$data['text_confirm'] = $this->language->get('text_confirm');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['text_no_results'] = $this->language->get('text_no_results');

		$data['action'] = $this->url->link('module/seo_page', 'token=' . $this->session->data['token'], 'SSL');
		$data['add'] = $this->url->link('module/seo_page', 'token=' . $this->session->data['token'] . "&add=1", 'SSL');
		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
		$data['delete'] = $this->url->link('module/seo_page/delete', 'token=' . $this->session->data['token'], 'SSL');
		$data['token'] = $this->session->data['token'];

		/*GET ONLY SEO LINK*/
		$this->load->model('module/seo_page');
		$data['seo_pages'] = $this->model_module_seo_page->getOnlySeoPagesLink();
		//echo "<pre>";print_r($data['seo_pages']);exit;


        $data['robots'] = array(
            '' => array(
                'index' => ''
            ),
            '1' => array(
                'index' => 'index, follow'
            ),
            '2' => array(
                'index' => 'noindex, follow'
            ),
            '3' => array(
                'index' => 'index, nofollow'
            ),
            '4' => array(
                'index' => 'noindex, nofollow'
            ),

        );





		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		if(isset($this->request->get['link_id']) || isset($this->request->get['add'])){
			if(isset($this->request->get['link_id']))
				$data['seo_page'] = $this->model_module_seo_page->getSeoPage($this->request->get['link_id']);
//echo "<pre>";print_r($data['seo_page']);exit;
			$this->response->setOutput($this->load->view('module/seo_page.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('module/seo_page_list.tpl', $data));
		}
	}


	public function delete(){
		$this->language->load('module/seo_page');
		$this->load->model('module/seo_page');
		if (isset($this->request->post['selected'])) {
			foreach ($this->request->post['selected'] as $link_id) {
				$this->model_module_seo_page->deleteSeoPage($link_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('module/seo_page', 'token=' . $this->session->data['token'], 'SSL'));
		} else {
			$this->response->redirect($this->url->link('module/seo_page', 'token=' . $this->session->data['token'], 'SSL'));
		}
	}
}