<?php

class ControllerToolP5sImport extends Controller
{
    private $error = array();
    private $key = '';

    public function __construct($registry)
    {
        $this->registry = $registry;
        $this->key = $this->config->get('config_p5s_import_key');
        $this->limit = '800';
        $this->log_file = 'p5s_log.txt';

    }



    public function index()
    {
        $this->load->language('tool/p5s_import');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('tool/p5s_import');
        $this->load->model('setting/setting');

        if (isset($this->request->post['config_p5s_import_key'])) {
            $data['config_p5s_import_key'] = $this->request->post['config_p5s_import_key'];
        } elseif ($this->config->has('config_p5s_import_key')) {
            $data['config_p5s_import_key'] = $this->config->get('config_p5s_import_key');
        } else {
            $data['config_p5s_import_key'] = '';
        }


        if (isset($this->request->post['config_p5s_import_mode'])) {
            $data['config_p5s_import_mode'] = $this->request->post['config_p5s_import_mode'];
        } elseif ($this->config->has('config_p5s_import_mode')) {
            $data['config_p5s_import_mode'] = $this->config->get('config_p5s_import_mode');
        } else {
            $data['config_p5s_import_mode'] = '';
        }


        if (isset($this->request->post['config_p5s_import_status'])) {
            $data['config_p5s_import_status'] = $this->request->post['config_p5s_import_status'];
        } elseif ($this->config->has('config_p5s_import_mode')) {
            $data['config_p5s_import_status'] = $this->config->get('config_p5s_import_status');
        } else {
            $data['config_p5s_import_status'] = '';
        }


        if (isset($this->request->post['config_p5s_import_url'])) {
            $data['config_p5s_import_url'] = $this->request->post['config_p5s_import_url'];
        } elseif ($this->config->has('config_p5s_import_key')) {
            $data['config_p5s_import_url'] = $this->config->get('config_p5s_import_url');
        } else {
            $data['config_p5s_import_url'] = '';
        }

        //var_dump($this->request->post);

        $data['action'] = $this->url->link('tool/p5s_import', 'token=' . $this->session->data['token'], 'SSL');


        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->user->hasPermission('modify', 'tool/p5s_import')) {
            $this->model_setting_setting->editSetting('config_p5s_import', $this->request->post);
            $this->response->redirect($this->url->link('tool/p5s_import', 'token=' . $this->session->data['token'], 'SSL'));

        }


        $data['heading_title'] = $this->language->get('heading_title');

        $data['entry_config_p5s_import_key'] = $this->language->get('entry_config_p5s_import_key');

        $data['entry_import'] = $this->language->get('entry_import');
        $data['entry_import_products'] = $this->language->get('entry_import_products');
        $data['entry_import_products_prices'] = $this->language->get('entry_import_products_prices');
        $data['entry_import_products_stock'] = $this->language->get('entry_import_products_stock');
        $data['entry_import_categories'] = $this->language->get('entry_import_categories');

        $data['button_import_products_without_image'] = $this->language->get('button_import_products_without_image');
        $data['button_import_products'] = $this->language->get('button_import_products');
        $data['button_import_products_prices'] = $this->language->get('button_import_products_prices');
        $data['button_import_products_stock'] = $this->language->get('button_import_products_stock');
        $data['button_import_categories'] = $this->language->get('button_import_categories');

        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['entry_status'] = $this->language->get('entry_status');


        $data['import_categories'] = $this->url->link('tool/p5s_import/getCategories', 'manual=1&token=' . $this->session->data['token'], 'SSL');
        $data['import_products'] = $this->url->link('tool/p5s_import/getProducts', 'manual=1&token=' . $this->session->data['token'], 'SSL');
        $data['import_products_without_image'] = $this->url->link('tool/p5s_import/getProducts', 'manual=1&with_image=0&token=' . $this->session->data['token'], 'SSL');
        $data['import_products_prices'] = $this->url->link('tool/p5s_import/getPrices', 'manual=1&token=' . $this->session->data['token'], 'SSL');
        $data['import_products_stock'] = $this->url->link('tool/p5s_import/getStock', 'manual=1&token=' . $this->session->data['token'], 'SSL');
        $data['test_order'] = $this->url->link('tool/p5s_import/testOrder', 'token=' . $this->session->data['token'], 'SSL');

        $data['button_save'] = $this->language->get('button_save');

        if (isset($this->session->data['error'])) {
            $data['error_warning'] = $this->session->data['error'];

            unset($this->session->data['error']);
        } elseif (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('tool/p5s_import', 'token=' . $this->session->data['token'], 'SSL')
        );


        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');


       // var_dump($data); exit;
        $this->response->setOutput($this->load->view('tool/p5s_import.tpl', $data));
    }



    public function testOrder() {

        $this->P5sdrop->addOrderToP2s();
    }



    /**
     * Очистка лога
     */
    private function clearLog() {
        $file = DIR_LOGS . $this->log_file;
        $handle = fopen($file, 'w+');
        fclose($handle);
    }


    public function getProductsCron($data)
    {

        $with_image = 1;
        $only_price_qty = 0;
        $this->getProducts($with_image, $only_price_qty);
    }


    public function getProducts($with_image = 1, $only_price_qty = 0)
    {

        $setting = array();
        $setting['language_id'] = 1;
        $setting['parent_id'] = 14; // head parrent cetegory
        $setting['limit'] = $this->limit;
        $setting['product_disable'] = 1;
        $setting['product_store'] = array(0);
        $ftime = time();

        $service_url = $this->config->get('config_p5s_import_url');
        $data['file_name'] = DIR_UPLOAD . 'p5s.csv';


        $alog = NEW log($this->log_file);

        $this->load->model('tool/p5s_import');
        $this->load->language('tool/p5s_import');


        if (isset($this->request->get['manual'])) {
            $setting['manual'] = $this->request->get['manual'];
        } else {
            $setting['manual'] = '';
        }


        if (isset($this->request->get['with_image'])) {
            $setting['image'] = $this->request->get['with_image'];
        } elseif ($with_image == 0) {
            $setting['image'] = 0;
        } else {
            $setting['image'] = 1;
        }


        if (isset($this->request->get['only_price_qty'])) {
            $setting['only_price_qty'] = $this->request->get['only_price_qty'];
        } elseif ($only_price_qty == 1) {
            $setting['only_price_qty'] = 1;
        } else {
            $setting['only_price_qty'] = 0;
        }

        if (isset($this->request->get['ftell'])) {
            $data['ftell'] = $this->request->get['ftell'];
        } else {

            $this->clearLog();
            $data['ftell'] = 0;
            $file_data = file_get_contents($service_url);
            $file_data = @iconv('WINDOWS-1251', "UTF-8//IGNORE", $file_data);
            file_put_contents($data['file_name'], $file_data);
            $alog->write("Файл импорта загружен");
        }


        if (isset($this->request->get['total'])) {
            $data['total'] = $this->request->get['total'];
        } else {
            $data['total'] = 0;
        }

        if (isset($this->request->get['update'])) {
            $data['update'] = $this->request->get['update'];
        } else {
            $data['update'] = 0;
        }

        if (isset($this->request->get['insert'])) {
            $data['insert'] = $this->request->get['insert'];
        } else {
            $data['insert'] = 0;
        }

        if (isset($this->request->get['error'])) {
            $data['error'] = $this->request->get['error'];
        } else {
            $data['error'] = 0;
        }

        if (file_exists($data['file_name'])) {
            $alog->write("Файл открыт . Парметры обработки dsta:" . print_r($data, 1) . "\t\n setting: " . print_r($setting,
                    1));

           // $result = $this->model_tool_p5s_import->import($data, $setting);

            $result = $this->model_tool_p5s_import->import2($data, $setting);

            if (isset($result['ftell'])) {
                $this->response->redirect($this->url->link('tool/p5s_import/getproducts', 'token=' .
                    $this->session->data['token']
                    . '&total=' . (int)$result['total']
                    . '&manual=' . (int)$setting['manual']
                    . '&with_image=' . (int)$setting['image']
                    . '&update=' . (int)$result['update']
                    . '&insert=' . (int)$result['insert']
                    . '&error=' . (int)$result['error']
                    . '&ftell=' . $result['ftell']
                    . '&ftime=' . $ftime, 'SSL'));
                // $this->response->redirect($this->url->link('tool/p5s_import', 'token=' . $this->session->data['token'], 'SSL'));
            } else {
                unlink($data['file_name']);
                if (empty($result)) {
                    $this->session->data['error'] = $this->language->get('error_import');
                } else {
                    $this->session->data['success'] = sprintf($this->language->get('text_success_import'), (int)$result['total'], (int)$result['update'], (int)$result['insert'], (int)$result['error']);

                }

                //-------------------------------------------------------------------------
                $this->cache->delete('manufacturer');
                $this->cache->delete('category');
                $this->cache->delete('product');
                $this->response->redirect($this->url->link('tool/p5s_import', 'token=' . $this->session->data['token'], 'SSL'));
            }

        } else {
            $alog->write("Отсувствует файл импорта");
            $this->session->data['error'] = $this->language->get('error_feed');
            $this->response->redirect($this->url->link('tool/p5s_import', 'token=' . $this->session->data['token'], 'SSL'));

        }

    }

}
