<?php
class ModelToolP5sImport extends Model {
    private $CSV_SEPARATOR = ';';
    private $CSV_ENCLOSURE = '"';
    private $CSV_CATEGORY_DEL = '|';

    private $field_caption = NULL;
    private $setting = array();
    private $attributes = array();
    private $CoreType = NULL;
    private $PathNameByCategory = array();
    private $CustomFields = array();
    private $Categories = array();
    private $CategoriesString = '';
    private $FindCategory = 0;
    // File
    private $f_tell = 0;

    //-------------------------------------------------------------------------
    // Import
    //-------------------------------------------------------------------------
    public function import($data, $setting) {

        //var_dump($this->alog); exit;

        $alog = NEW log('p5s_log.txt');

        // Set Setting
        //-------------------------------------------------------------------------
        $this->setting = $setting;

        // Get Field Caption
        //-------------------------------------------------------------------------
        if (($this->field_caption = $this->getFieldCaption($data['file_name'])) === NULL) {
            return NULL;
        }

        $alog->write('начало обработки ', FILE_APPEND);

        // Check Iterator
        //-------------------------------------------------------------------------
            // Set ftell
            if($data['ftell'] != 0) {
                $this->f_tell = $data['ftell'] - 20*1024;
            }

        // Disable All Products
        //-------------------------------------------------------------------------
        if($this->setting['product_disable'] == 1  && $data['ftell'] == 0) {
            $this->db->query('UPDATE `' . DB_PREFIX . 'product` SET status = 0');
        }

        // IMPORT CSV FORMAT
        //-------------------------------------------------------------------------
        $total = $data['total'];
        $update_count = $data['update'];
        $insert_count = $data['insert'];
        $error_count = $data['error'];
        $result = NULL;

        $this->load->model('catalog/product');

        if (($handle = fopen($data['file_name'], 'r')) !== FALSE) {

            fseek($handle, $this->f_tell); // set position before CSV Caption

            $item_count =0;
            while(($items = fgetcsv($handle, 100*1024, $this->CSV_SEPARATOR, $this->CSV_ENCLOSURE)) !== FALSE) {

                $item_count++;
                $total++;

                $alog->write('начало обработки стрки'.$total." - ".$item_count." - ".$this->setting['limit']." \t\n", FILE_APPEND);

                if ( (count($items)) == count($this->field_caption)) {

                    $alog->write('обработка модели: '.$items[$this->field_caption['_MODEL_']]." \t\n", FILE_APPEND);

                    $product_prepear_data = array();

                    $product_id = $this->getProductIdByModel($items[$this->field_caption['_MODEL_']]);

                    $product_prepear_data = $this->prepearProduct($items, $product_id);



                    if ($product_id  !== FALSE ) {
                            if($this->editProduct($product_id, $product_prepear_data) ) {
                                $update_count++;
                            } else {
                                $error_count ++;
                                $alog->write('1!!!!!ошибка при обновлении модель:'
                                    .$items[$this->field_caption['_MODEL_']]." product_id ".$product_id." product_prepear_data "
                                    .print_r($product_prepear_data,1)." \t\n",
                                    FILE_APPEND);

                            }
                    } else {
                        // check import mode
                            if($this->addProduct($product_prepear_data) ) {
                                $insert_count++;
                            } else {
                                $error_count ++;
                                $alog->write('2!!!!!ошибка при добавлении модель:'
                                    .$items[$this->field_caption['_MODEL_']]." product_prepear_data "
                                    .print_r($product_prepear_data,1)." \t\n",
                                    FILE_APPEND);
                            }
                    }

                } else {
                    $error_count ++;
                    $alog->write('3!!!!!ошибка соответствия количества ячеек количеству ячеек заглавия \t\n',FILE_APPEND);
                    $alog->write('#################################################### \t\n', FILE_APPEND);
                    $alog->write('количество items'.count($items) .'- количество field_caption'. count
                    ($this->field_caption).'  \t\n', FILE_APPEND);
                    $alog->write('items'.print_r($items,1).' \t\n', FILE_APPEND);

                    $alog->write('#################################################### \t\n', FILE_APPEND);
                }

                $alog->write('конец обработки стрки'.$total." - ".$item_count." - ".$this->setting['limit']." \t\n",
                    FILE_APPEND);
//                // Check Iterator
//                //-------------------------------------------------------------------------
                if ($item_count == $this->setting['limit'] AND $this->setting['limit'] > 0  AND $this->setting['manual']=='1') {
                    $result['ftell'] = ftell($handle);
                    $alog->write('_result '.$result['ftell'], FILE_APPEND);
                    break;
                }
            }

            fclose($handle);
            $result['total'] = $data['total'] + $item_count;
            $result['update'] = $update_count;
            $result['insert'] = $insert_count;
            $result['error'] = $error_count;
        }

        return $result;
    }
    public function import2($data, $setting) {

        //var_dump($this->alog); exit;

        $alog = NEW log('p5s_log.txt');

        // Set Setting
        //-------------------------------------------------------------------------
        $this->setting = $setting;

        // Get Field Caption
        //-------------------------------------------------------------------------
        if (($this->field_caption = $this->getFieldCaption($data['file_name'])) === NULL) {
            return NULL;
        }

        $alog->write('начало обработки ', FILE_APPEND);

        // Check Iterator
        //-------------------------------------------------------------------------
            // Set ftell
            if($data['ftell'] != 0) {
                $this->f_tell = $data['ftell'] - 20*1024;
            }

        // Disable All Products
        //-------------------------------------------------------------------------
        if($this->setting['product_disable'] == 1  && $data['ftell'] == 0) {
            $this->db->query('UPDATE `' . DB_PREFIX . 'product` SET status = 0');
        }

        // IMPORT CSV FORMAT
        //-------------------------------------------------------------------------
        $total = $data['total'];
        $update_count = $data['update'];
        $insert_count = $data['insert'];
        $error_count = $data['error'];
        $result = NULL;

        $this->load->model('catalog/product');

        if (($handle = fopen($data['file_name'], 'r')) !== FALSE) {

            fseek($handle, $this->f_tell); // set position before CSV Caption

            $item_count =0;
            while(($items = fgetcsv($handle, 100*1024, $this->CSV_SEPARATOR, $this->CSV_ENCLOSURE)) !== FALSE) {

                $item_count++;
                $total++;

                $alog->write('начало обработки стрки'.$total." - ".$item_count." - ".$this->setting['limit']." \t\n", FILE_APPEND);

                if ( (count($items)) == count($this->field_caption)) {

                    $alog->write('обработка позиции: '.$items[$this->field_caption['aID']]." \t\n", FILE_APPEND);

                    $product_prepear_data = array();

                    $product_id = $this->getProductIdBySku($items[$this->field_caption['aID']]);

                  //  var_dump($items); exit;

                    $product_prepear_data = $this->prepearProduct2($items, $product_id);
                   // var_dump($product_prepear_data);exit;
                    if ($product_id  !== FALSE ) {
                            if($this->editProduct($product_id, $product_prepear_data) ) {
                                $update_count++;
                            } else {
                                $error_count ++;
                                $alog->write('1!!!!!ошибка при обновлении модель:'
                                    .$items[$this->field_caption['aID']]." product_id ".$product_id." product_prepear_data "
                                    .print_r($product_prepear_data,1)." \t\n",
                                    FILE_APPEND);

                            }
                    } else {
                        // check import mode
                            if($this->addProduct($product_prepear_data) ) {
                                $insert_count++;
                            } else {
                                $error_count ++;
                                $alog->write('2!!!!!ошибка при добавлении модель:'
                                    .$items[$this->field_caption['aID']]." product_prepear_data "
                                    .print_r($product_prepear_data,1)." \t\n",
                                    FILE_APPEND);
                            }
                    }

                } else {
                    $error_count ++;
                    $alog->write('3!!!!!ошибка соответствия количества ячеек количеству ячеек заглавия \t\n',FILE_APPEND);
                    $alog->write('#################################################### \t\n', FILE_APPEND);
                    $alog->write('количество items'.count($items) .'- количество field_caption'. count
                    ($this->field_caption).'  \t\n', FILE_APPEND);
                    $alog->write('items'.print_r($items,1).' \t\n', FILE_APPEND);

                    $alog->write('#################################################### \t\n', FILE_APPEND);
                }

                $alog->write('конец обработки стрки'.$total." - ".$item_count." - ".$this->setting['limit']." \t\n",
                    FILE_APPEND);
//                // Check Iterator
//                //-------------------------------------------------------------------------
                if ($item_count == $this->setting['limit'] AND $this->setting['limit'] > 0  AND $this->setting['manual']=='1') {
                    $result['ftell'] = ftell($handle);
                    $alog->write('_result '.$result['ftell'], FILE_APPEND);
                    break;
                }
            }

            fclose($handle);
            $result['total'] = $data['total'] + $item_count;
            $result['update'] = $update_count;
            $result['insert'] = $insert_count;
            $result['error'] = $error_count;
        }

        return $result;
    }


    //-------------------------------------------------------------------------
    // Field Name
    //-------------------------------------------------------------------------
    private function getFieldCaption($file_name) {
        $result = NULL;

        if (($handle = fopen($file_name, 'r')) !== FALSE) {
            if (($field_caption = fgetcsv($handle, 1000, $this->CSV_SEPARATOR, $this->CSV_ENCLOSURE)) !== FALSE) {
                for($i = 0; $i < count($field_caption); $i++) {
                    $field_caption[$i] = trim($field_caption[$i], " \t\n");
                }
                $result = array_flip($field_caption); // перевёртыш
                //$result = $field_caption;
                $this->f_tell = ftell($handle);
            }
            fclose($handle);
        }

        return $result;
    }


    public function getProductDescriptions($product_id) {
        $product_description_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$product_id . "'");

        foreach ($query->rows as $result) {
            $product_description_data[$result['language_id']] = array(
                'name'             => $result['name'],
                'description'      => $result['description'],
                'short_description' => $result['short_description'],
                'meta_title'       => $result['meta_title'],
                'meta_h1'          => $result['meta_h1'],
                'meta_description' => $result['meta_description'],
                'meta_keyword'     => $result['meta_keyword'],
                'tag'              => $result['tag']
            );
        }

        return $product_description_data;
    }
    public function getProduct($product_id) {
        $query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'product_id=" . (int)$product_id . "') AS keyword FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id = '" . (int)$product_id . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

        return $query->row;
    }
    public function getProductIdByModel($model) {
        $query = $this->db->query("SELECT p.product_id FROM " . DB_PREFIX . "product p  WHERE p.model = '" . $model . "'");

        if (isset($query->row['product_id'])) {
            return $query->row['product_id'];
        } else {
            return false;
        }
    }
    public function getProductIdBySku($sku) {
        $query = $this->db->query("SELECT p.product_id FROM " . DB_PREFIX . "product p  WHERE p.sku = '" . $sku . "'");

        if (isset($query->row['product_id'])) {
            return $query->row['product_id'];
        } else {
            return false;
        }
    }

    //-------------------------------------------------------------------------
    // Field Name
    //-------------------------------------------------------------------------
    private function prepearProduct($data, $product_id=false) {

        $items_data = array();
        $old_product_data = array();

        if ($product_id) {
          //  $old_product_data_description = $this->getProductDescriptions($product_id);
            $old_product_data = $this->getProduct($product_id);
        }
        $items_data = array(
        'product_description' =>
  array (
      1 =>
          array (
              'name' => isset($old_product_data['name'])? $old_product_data['name'] : '',
              'short_description' =>  isset($old_product_data['short_description'])? $old_product_data['short_description'] : '',
              'description' =>  isset($old_product_data['description'])? $old_product_data['description'] : '',
              'short_description' =>  isset($old_product_data['short_description'])? $old_product_data['short_description'] : '',
              'meta_title' =>  isset($old_product_data['meta_title'])? $old_product_data['meta_title'] : '',
              'meta_h1' =>  isset($old_product_data['meta_h1'])? $old_product_data['meta_h1'] : '',
              'meta_description' =>  isset($old_product_data['meta_description'])? $old_product_data['meta_description'] : '',
              'meta_keyword' =>  isset($old_product_data['meta_keyword'])? $old_product_data['meta_keyword'] : '',
              'tag' =>  isset($old_product_data['tag'])? $old_product_data['tag'] : '',
          ),
  ),
  'image' => isset($old_product_data['image'])? $old_product_data['image'] : '',
  'model' =>  isset($old_product_data['model'])? $old_product_data['model'] : '',
  'sku' =>  isset($old_product_data['sku'])? $old_product_data['sku'] : '',
  'upc' =>  isset($old_product_data['upc'])? $old_product_data['upc'] : '',
  'ean' =>  isset($old_product_data['ean'])? $old_product_data['ean'] : '',
  'jan' =>  isset($old_product_data['jan'])? $old_product_data['jan'] : 'p5s',
  'isbn' =>  isset($old_product_data['isbn'])? $old_product_data['isbn'] : '',
  'mpn' =>  isset($old_product_data['mpn'])? $old_product_data['mpn'] : '',
  'location' =>  isset($old_product_data['location'])? $old_product_data['location'] : '',
  'price' =>  isset($old_product_data['price'])? $old_product_data['price'] : '',
  'for_whom' => isset($old_product_data['for_whom'])? $old_product_data['for_whom'] : '',
  'tax_class_id' =>isset($old_product_data['tax_class_id'])? $old_product_data['tax_class_id'] : 0,
  'quantity' => isset($old_product_data['quantity'])? $old_product_data['quantity'] : '',
  'minimum' =>  isset($old_product_data['minimum'])? $old_product_data['minimum'] : '1',
  'subtract' => isset($old_product_data['subtract'])? $old_product_data['subtract'] : '',
  'stock_status_id' => isset($old_product_data['stock_status_id'])? $old_product_data['stock_status_id'] : '7',
  'shipping' => isset($old_product_data['shipping'])? $old_product_data['shipping'] : '1',
  'keyword' =>  isset($old_product_data['keyword'])? $old_product_data['keyword'] : '',
  'date_available' =>  isset($old_product_data['date_available'])? $old_product_data['date_available'] : '',
  'length' =>  isset($old_product_data['length'])? $old_product_data['length'] : '',
  'width' =>  isset($old_product_data['width'])? $old_product_data['width'] : '',
  'height' => isset($old_product_data['height'])? $old_product_data['height'] : '',
  'length_class_id' => isset($old_product_data['length_class_id'])? $old_product_data['length_class_id'] : '1',
  'weight' => isset($old_product_data['weight'])? $old_product_data['weight'] : '',
  'weight_class_id' => isset($old_product_data['weight_class_id'])? $old_product_data['weight_class_id'] : '1',
  'status' =>isset($old_product_data['status'])? $old_product_data['status'] : '',
  'sort_order' => isset($old_product_data['sort_order'])? $old_product_data['sort_order'] : '',
  'manufacturer_id' => isset($old_product_data['manufacturer_id'])? $old_product_data['manufacturer_id'] : '',
  'main_category_id' =>  '',
  'product_category' =>   array (),
  'filter' => '',
  'product_store' => array(0=>'0'),
  'download' => '',
  'related' => '',
  'product_related' =>   array (),
  'product_attribute' =>   array ( ),
  'option' => '',
  'product_option' =>   array ( ),
  'product_special' =>   array (),
  'points' => '0',
  'product_reward' =>   array ( ),
  'product_layout' =>   array(0=>'0'),
);

        if(isset($this->field_caption['_STATUS_'])) {
            $items_data['status'] = $data[$this->field_caption['_STATUS_']];
        }

        if(isset($this->field_caption['_MODEL_'])) {
            $items_data['model'] = $data[$this->field_caption['_MODEL_']];
        }

        if(isset($this->field_caption['_SKU_'])) {
            $items_data['sku'] = $data[$this->field_caption['_SKU_']];
        }

        if(isset($this->field_caption['_PRICE_'])) {
            $items_data['price'] = $data[$this->field_caption['_PRICE_']];
        }

        if(isset($this->field_caption['_QUANTITY_'])) {
            $items_data['quantity'] = $data[$this->field_caption['_QUANTITY_']];
        }

        if(isset($this->field_caption['_WEIGHT_'])) {
            $items_data['weight'] = $data[$this->field_caption['_WEIGHT_']];
        }

        if(isset($this->field_caption['_IMAGE_']) AND ($this->setting['image']==1)) {
            $images = array(0=>$data[$this->field_caption['_IMAGE_']]);
            $image_data = $this->setImages($images);
            $items_data['image'] = $image_data[0]['image'];
        }

        if(isset($this->field_caption['_IMAGES_']) AND ($this->setting['image']==1)) {
            $images = explode(',',$data[$this->field_caption['_IMAGES_']]);
            $items_data['product_image'] = $this->setImages($images);
        }

        if(isset($this->field_caption['_CATEGORY_'])) {

            $categories = $this->setCategory($data[$this->field_caption['_CATEGORY_']]);

            $items_data['product_category'] = $categories;
            $items_data['main_category_id'] = $categories[count($categories)-1];
        }


        if(isset($this->field_caption['_SHIPPING_'])) {
            $items_data['shipping'] = $data[$this->field_caption['_SHIPPING_']];
        }

        if(isset($this->field_caption['_subtract_'])) {
            $items_data['subtract'] = $data[$this->field_caption['_subtract_']];
        }

        if(isset($this->field_caption['_LENGTH_'])) {
            $items_data['lenght'] = $data[$this->field_caption['_LENGTH_']];
        }

        if(isset($this->field_caption['_NAME_'])) {
            $items_data['product_description'][1]['name'] = $data[$this->field_caption['_NAME_']];
         }

        if(isset($this->field_caption['_HTML_H1_'])) {
            $items_data['product_description'][1]['meta_h1'] = $data[$this->field_caption['_HTML_H1_']];
         }

        if(isset($this->field_caption['_HTML_TITLE_'])) {
            $items_data['product_description'][1]['meta_title'] = $data[$this->field_caption['_HTML_TITLE_']];
         }


        if(isset($this->field_caption['_DESCRIPTION_'])) {
            $items_data['product_description'][1]['description'] = $data[$this->field_caption['_DESCRIPTION_']];
         }

        if(isset($this->field_caption['_OPTIONS_'])) {
             $items_data['product_option'] = $this->setOptions($data[$this->field_caption['_OPTIONS_']], $product_id);
        }


        if( isset($this->field_caption['_ATTRIBUTES_'])) {

            $attributes_data = array();
            $attributes = explode("\n", $data[$this->field_caption['_ATTRIBUTES_']]);

            if(!empty($attributes)) {
                if ($product_id) {
                    $this->db->query('DELETE FROM `' . DB_PREFIX . 'product_attribute` WHERE product_id = \'' . (int)$product_id . '\'');
                }
                $tmp_product_attributes = array(); // added in v2.2.2a
                foreach ($attributes as $attribute_date) {
                    $attribute = explode('|', $attribute_date);
                    if(count($attribute) == 3) {
                        $attribute[0] = trim($attribute[0]);
                        $attribute[1] = trim($attribute[1]);
                        $attribute[2] = trim($attribute[2]);
                        // check
                        if(!isset($this->attributes[mb_strtolower($attribute[0].$attribute[1])])) {
                            $attribute_id = $this->addProductAttribute($attribute[0], $attribute[1]);
                        } else {
                            $attribute_id = $this->attributes[mb_strtolower($attribute[0].$attribute[1])];
                        }

                        $attributes_data[] = array (
                                    'name' => $attribute[1],
                                    'attribute_id' => $attribute_id,
                                    'product_attribute_description' =>
                                        array (
                                            $this->setting['language_id'] =>
                                                array (
                                                    'text' => $attribute[2],
                                                ),
                                        ),
                                );
                    }
                }
            }
            $items_data['product_attribute'] = $attributes_data;
        }

        if(isset($this->field_caption['_MANUFACTURER_'])) {
            $items_data['manufacturer'] = $data[$this->field_caption['_MANUFACTURER_']];
            $manufacturer_id = $this->GetManufacturerIdByname($data[$this->field_caption['_MANUFACTURER_']]);

            if (!$manufacturer_id) {
                $manufacturer_id = $this->addManufacturer($data[$this->field_caption['_MANUFACTURER_']]);
            }

             $items_data['manufacturer_id'] = $manufacturer_id;
         }

        return $items_data;
    }

    private function prepearProduct2($data, $product_id=false) {

        $items_data = array();
        $old_product_data = array();

        if ($product_id) {
          //  $old_product_data_description = $this->getProductDescriptions($product_id);
            $old_product_data = $this->getProduct($product_id);
        }
        $items_data = array(
        'product_description' =>
  array (
      1 =>
          array (
              'name' => isset($old_product_data['name'])? $old_product_data['name'] : '',
              'short_description' =>  isset($old_product_data['short_description'])? $old_product_data['short_description'] : '',
              'description' =>  isset($old_product_data['description'])? $old_product_data['description'] : '',
              'short_description' =>  isset($old_product_data['short_description'])? $old_product_data['short_description'] : '',
              'meta_title' =>  isset($old_product_data['meta_title'])? $old_product_data['meta_title'] : '',
              'meta_h1' =>  isset($old_product_data['meta_h1'])? $old_product_data['meta_h1'] : '',
              'meta_description' =>  isset($old_product_data['meta_description'])? $old_product_data['meta_description'] : '',
              'meta_keyword' =>  isset($old_product_data['meta_keyword'])? $old_product_data['meta_keyword'] : '',
              'tag' =>  isset($old_product_data['tag'])? $old_product_data['tag'] : '',
          ),
  ),
  'image' => isset($old_product_data['image'])? $old_product_data['image'] : '',
  'model' =>  isset($old_product_data['model'])? $old_product_data['model'] : '',
  'sku' =>  isset($old_product_data['sku'])? $old_product_data['sku'] : '',
  'upc' =>  isset($old_product_data['upc'])? $old_product_data['upc'] : '',
  'ean' =>  isset($old_product_data['ean'])? $old_product_data['ean'] : '',
  'jan' =>  isset($old_product_data['jan'])? $old_product_data['jan'] : 'p5s',
  'isbn' =>  isset($old_product_data['isbn'])? $old_product_data['isbn'] : '',
  'mpn' =>  isset($old_product_data['mpn'])? $old_product_data['mpn'] : '',
  'location' =>  isset($old_product_data['location'])? $old_product_data['location'] : '',
  'price' =>  isset($old_product_data['price'])? $old_product_data['price'] : '',
  'for_whom' => isset($old_product_data['for_whom'])? $old_product_data['for_whom'] : '',
  'tax_class_id' =>isset($old_product_data['tax_class_id'])? $old_product_data['tax_class_id'] : 0,
  'quantity' => isset($old_product_data['quantity'])? $old_product_data['quantity'] : '',
  'minimum' =>  isset($old_product_data['minimum'])? $old_product_data['minimum'] : '1',
  'subtract' => isset($old_product_data['subtract'])? $old_product_data['subtract'] : '',
  'stock_status_id' => isset($old_product_data['stock_status_id'])? $old_product_data['stock_status_id'] : '7',
  'shipping' => isset($old_product_data['shipping'])? $old_product_data['shipping'] : '1',
  'keyword' =>  isset($old_product_data['keyword'])? $old_product_data['keyword'] : '',
  'date_available' =>  isset($old_product_data['date_available'])? $old_product_data['date_available'] : '',
  'length' =>  isset($old_product_data['length'])? $old_product_data['length'] : '',
  'width' =>  isset($old_product_data['width'])? $old_product_data['width'] : '',
  'height' => isset($old_product_data['height'])? $old_product_data['height'] : '',
  'length_class_id' => isset($old_product_data['length_class_id'])? $old_product_data['length_class_id'] : '1',
  'weight' => isset($old_product_data['weight'])? $old_product_data['weight'] : '',
  'weight_class_id' => isset($old_product_data['weight_class_id'])? $old_product_data['weight_class_id'] : '1',
  'status' =>isset($old_product_data['status'])? $old_product_data['status'] : '',
  'sort_order' => isset($old_product_data['sort_order'])? $old_product_data['sort_order'] : '',
  'manufacturer_id' => isset($old_product_data['manufacturer_id'])? $old_product_data['manufacturer_id'] : '',
  'main_category_id' =>  '',
  'product_category' =>   array (),
  'filter' => '',
  'product_store' => array(0=>'0'),
  'download' => '',
  'related' => '',
  'product_related' =>   array (),
  'product_attribute' =>   array ( ),
  'option' => '',
  'product_option' =>   array ( ),
  'product_special' =>   array (),
  'points' => '0',
  'product_reward' =>   array ( ),
  'product_layout' =>   array(0=>'0'),
);


        $items_data['status'] = 1;
        $items_data['jan'] = 'p5s';
        $items_data['shipping'] = 1;

        if(isset($this->field_caption['prodID'])) {
            $items_data['model'] = $data[$this->field_caption['prodID']];
        }

        if(isset($this->field_caption['aID'])) {
            $items_data['sku'] = $data[$this->field_caption['aID']];
        }

        if(isset($this->field_caption['Barcode'])) {
            $items_data['upc'] = $data[$this->field_caption['Barcode']];
        }

        if(isset($this->field_caption['VendorCode'])) {
            $items_data['ean'] = $data[$this->field_caption['VendorCode']];
        }

        if(isset($this->field_caption['RetailPrice'])) {
            $items_data['price'] = $data[$this->field_caption['RetailPrice']];
        }

        if(isset($this->field_caption['InSale'])) {
            $items_data['quantity'] = $data[$this->field_caption['InSale']];
        }

        if(isset($this->field_caption['Brutto'])) {
            $items_data['weight'] = $data[$this->field_caption['Brutto']];
        }

        if(isset($this->field_caption['Image']) AND ($this->setting['image']==1)) {
            $images = explode(' ',$data[$this->field_caption['Image']]);
//            $images = array(0=>$data[$this->field_caption['image']]);
            $image_data = $this->setImages($images);
            $items_data['image'] = $image_data[0]['image'];
            array_shift($image_data);
            $items_data['product_image'] = $image_data;
        }

        if(isset($this->field_caption['Category']) AND isset($this->field_caption['SubCategory'])) {

           // echo $data[$this->field_caption['Category'].'|'.$data[$this->field_caption['SubCategory']];

            $categories = $this->setCategory($data[$this->field_caption['Category']].'|'.$data[$this->field_caption['SubCategory']]);

            $items_data['product_category'] = $categories;
            $items_data['main_category_id'] = $categories[count($categories)-1];
        }


//        if(isset($this->field_caption['_SHIPPING_'])) {
//            $items_data['shipping'] = $data[$this->field_caption['_SHIPPING_']];
//        }


        if(isset($this->field_caption['ShippingDate'])) {
            $items_data['date_available'] = $data[$this->field_caption['ShippingDate']];
        }

//        if(isset($this->field_caption['_subtract_'])) {
//            $items_data['subtract'] = $data[$this->field_caption['_subtract_']];
//        }
//
//        if(isset($this->field_caption['_LENGTH_'])) {
//            $items_data['lenght'] = $data[$this->field_caption['_LENGTH_']];
//        }

        if(isset($this->field_caption['Name'])) {
            $items_data['product_description'][1]['name'] = $data[$this->field_caption['Name']];
         }

//        if(isset($this->field_caption['_HTML_H1_'])) {
//            $items_data['product_description'][1]['meta_h1'] = $data[$this->field_caption['_HTML_H1_']];
//         }
//
//        if(isset($this->field_caption['_HTML_TITLE_'])) {
//            $items_data['product_description'][1]['meta_title'] = $data[$this->field_caption['_HTML_TITLE_']];
//         }


        if(isset($this->field_caption['Description'])) {
            $items_data['product_description'][1]['description'] = $data[$this->field_caption['Description']];
         }


        ################################

        $items_data['product_option'] = array();
        if(isset($this->field_caption['Color']) OR isset($this->field_caption['Size'])) {
            if ($product_id) {
                // Delete old product option
                $this->db->query('DELETE FROM `' . DB_PREFIX . 'product_option` WHERE  product_id = \'' . (int)$product_id . '\'');
                $this->db->query('DELETE FROM `' . DB_PREFIX . 'product_option_value` WHERE  product_id = \'' . (int)$product_id . '\'');
            }
        }

        if(isset($this->field_caption['Color']) AND !empty($this->field_caption['Color'])) {
            $items_data['product_option'][] = $this->setOptions2('Цвет', $data[$this->field_caption['Color']], $product_id);
        }

        if(isset($this->field_caption['Size']) AND !empty($this->field_caption['Color'])) {
           $items_data['product_option'][] = $this->setOptions2('Размер',$data[$this->field_caption['Size']], $product_id);
        }

################################# Attr
        $attributes_data=array();
        $attr_group_name = 'Параметры';

        if ($product_id) {
            $this->db->query('DELETE FROM `' . DB_PREFIX . 'product_attribute` WHERE product_id = \'' . (int)$product_id . '\'');
        }

        if( isset($this->field_caption['Color']) AND !empty($data[$this->field_caption['Color']])) {
                $attr_name = 'Цвет';
                $attributes_data[] = $this->addToDataAttrParam($attr_group_name, $attr_name, $data[$this->field_caption['Color']]);

            $items_data['product_description'][1]['name'] .= ' (цвет -'.$data[$this->field_caption['Color']].') ';
            }
        if( isset($this->field_caption['Size']) AND !empty($data[$this->field_caption['Size']])) {
                $attr_name = 'Размер';
                $attributes_data[] = $this->addToDataAttrParam($attr_group_name, $attr_name, $data[$this->field_caption['Size']]);
            $items_data['product_description'][1]['name'] .= ' (размер -'.$data[$this->field_caption['Size']].') ';

        }
        if( isset($this->field_caption['Brutto']) AND !empty($data[$this->field_caption['Brutto']])) {
                $attr_name = 'Вес в килограммах';
                $attributes_data[] = $this->addToDataAttrParam($attr_group_name, $attr_name, $data[$this->field_caption['Brutto']]);
            }

        if( isset($this->field_caption['Batteries']) AND !empty($data[$this->field_caption['Batteries']])) {
            $attr_name = 'Тип и количество батареек';
            $attributes_data[] = $this->addToDataAttrParam($attr_group_name, $attr_name, $data[$this->field_caption['Batteries']]);
        }

        if( isset($this->field_caption['Pack']) AND !empty($data[$this->field_caption['Pack']])) {
            $attr_name = 'Тип упаковки';
            $attributes_data[] = $this->addToDataAttrParam($attr_group_name, $attr_name, $data[$this->field_caption['Pack']]);
        }

        if( isset($this->field_caption['Material']) AND !empty($data[$this->field_caption['Material']])) {
            $attr_name = 'Материал';
            $attributes_data[] = $this->addToDataAttrParam($attr_group_name, $attr_name, $data[$this->field_caption['Material']]);
        }

        if( isset($this->field_caption['Lenght']) AND !empty($data[$this->field_caption['Lenght']])) {
            $attr_name = 'Длина в сантиметрах';
            $attributes_data[] = $this->addToDataAttrParam($attr_group_name, $attr_name, $data[$this->field_caption['Lenght']]);
        }

        if( isset($this->field_caption['Diameter']) AND !empty($data[$this->field_caption['Diameter']])) {
            $attr_name = 'Диаметр в сантиметрах';
            $attributes_data[] = $this->addToDataAttrParam($attr_group_name, $attr_name, $data[$this->field_caption['Diameter']]);
        }

        if( isset($this->field_caption['Collection']) AND !empty($data[$this->field_caption['Collection']])) {
            $attr_name = 'Колекция';
            $attributes_data[] = $this->addToDataAttrParam($attr_group_name, $attr_name, $data[$this->field_caption['Collection']]);
        }
        $items_data['product_attribute'] = $attributes_data;

        if(isset($this->field_caption['Vendor'])) {
            $items_data['manufacturer'] = $data[$this->field_caption['Vendor']];
            $manufacturer_id = $this->GetManufacturerIdByname($data[$this->field_caption['Vendor']]);

            if (!$manufacturer_id) {
                $manufacturer_id = $this->addManufacturer($data[$this->field_caption['Vendor']]);
            }

             $items_data['manufacturer_id'] = $manufacturer_id;
         }

        return $items_data;
    }


    private function addToDataAttrParam($attr_group_name, $attr_name,  $attr_value) {

        if(!isset($this->attributes[mb_strtolower($attr_group_name.$attr_name)])) {
            $attribute_id = $this->addProductAttribute($attr_group_name, $attr_name);
        } else {
            $attribute_id = $this->attributes[mb_strtolower($attr_group_name.$attr_name)];
        }

        $attribute_data = array (
            'name' => $attr_name,
            'attribute_id' => $attribute_id,
            'product_attribute_description' =>
                array (
                    $this->setting['language_id'] =>
                        array (
                            'text' => $attr_value,
                        ),
                ),
        );
        return $attribute_data;
    }


    // Add Manufacturer
    //-------------------------------------------------------------------------
    private function getManufacturer(&$name) {
        $name = trim($name, " \t\n");

        if(empty($name)) return 0;

        $result = $this->db->query('SELECT manufacturer_id FROM `' . DB_PREFIX . 'manufacturer` WHERE LOWER(name) = LOWER(\'' . $this->db->escape($name) . '\') LIMIT 1');

        if(isset($result->num_rows) AND $result->num_rows > 0 ) {
            return $result->row['manufacturer_id'];
        } else {
            $this->db->query('INSERT INTO `' . DB_PREFIX . 'manufacturer` SET name = \'' . $this->db->escape($name) . '\', sort_order = 0');

            $manufacturer_id = $this->db->getLastId();

            if (isset($this->setting['product_store'])) {
                foreach ($this->setting['product_store'] as $store_id) {
                    $this->db->query('INSERT INTO `' . DB_PREFIX . 'manufacturer_to_store` SET manufacturer_id = \'' . (int)$manufacturer_id . '\', store_id = \'' . (int)$store_id . '\'');
                }
            }
            if ($this->CoreType == 'ocstore') {
                $this->db->query('INSERT INTO `' . DB_PREFIX . 'manufacturer_description` SET seo_h1 = \'' . $this->db->escape($name) . '\', manufacturer_id = \'' . (int)$manufacturer_id . '\', language_id = \'' . (int)$this->setting['language_id'] . '\'');
            }
            return $manufacturer_id;
        }
    }

    public function setImages($images) {

        $product_images = array();
            if (!empty($images)) {
                $k = 0;
                foreach ($images as $image) {
                    $k++;
                    $good_image =true;
                    $name = basename($image); // to get file name
                    $image_loaded = 'catalog/p5s/' . $name;

                    if (file_exists(DIR_IMAGE . $image_loaded)) {
                        $file_size_mgb = stat(DIR_IMAGE . $image_loaded)['size'];
                        //  $this->log->write("размер фото sku: " .$data->sku .'-'  .print_r($file_size_mgb,1) . "\n");
                        if ($file_size_mgb == 0) {
                            $good_image =false;
                        }
                    }

                    if (file_exists(DIR_IMAGE . $image_loaded) AND $good_image) {
                       $product_images[] = array(
                            'image' => $image_loaded,
                            'sort_order' => 0,
                        );
                        continue;
                    } else {


                        $image_data = file_get_contents($image);

                        if (file_put_contents(DIR_IMAGE . $image_loaded, $image_data)) {

                            $product_images[] = array(
                                'image' => $image_loaded,
                                'sort_order' => 0,
                            );
                        }
                    }
                }
            }

            return $product_images;
    }


    // Add Attribute
    //-------------------------------------------------------------------------
    private function addProductAttribute($group_name, $attribute_name) {
        $query = $this->db->query('SELECT attribute_group_id FROM `' . DB_PREFIX . 'attribute_group_description` WHERE LOWER(name) = LOWER(\'' .$this->db->escape($group_name) . '\') AND language_id = \'' . (int)$this->setting['language_id'] . '\' LIMIT 1');

        if(isset($query->row['attribute_group_id'])) {
            $attribute_group_id = $query->row['attribute_group_id'];
        } else {
            $this->db->query('INSERT INTO `' . DB_PREFIX . 'attribute_group` SET sort_order = 1');
            $attribute_group_id = $this->db->getLastId();
            $this->db->query('INSERT INTO `' . DB_PREFIX . 'attribute_group_description`
				SET attribute_group_id = '.(int)$attribute_group_id.',
				language_id = \'' . (int)$this->setting['language_id'] . '\',
				name = \'' .$this->db->escape($group_name) . '\'
			');
        }

        $query = $this->db->query('SELECT ad.attribute_id FROM `' . DB_PREFIX . 'attribute_description` ad
			LEFT JOIN `' . DB_PREFIX . 'attribute` a ON (ad.attribute_id = a.attribute_id)
			WHERE LOWER(ad.name) = LOWER(\'' .$this->db->escape($attribute_name) . '\') AND ad.language_id = \'' . (int)$this->setting['language_id'] . '\' AND a.attribute_group_id = \'' . (int)$attribute_group_id . '\' LIMIT 1
			');

        if(isset($query->row['attribute_id'])) {
            $attribute_id = $query->row['attribute_id'];
        } else {
            $this->db->query('INSERT INTO `' . DB_PREFIX . 'attribute` SET sort_order = 1, attribute_group_id = '. (int)$attribute_group_id);
            $attribute_id = $this->db->getLastId();
            $this->db->query('INSERT INTO `' . DB_PREFIX . 'attribute_description`
				SET attribute_id = '.(int)$attribute_id.',
				language_id = \'' . (int)$this->setting['language_id'] . '\',
				name = \'' .$this->db->escape($attribute_name) . '\'
			');
        }

        return $attribute_id;
    }


    // Add Options
    //-------------------------------------------------------------------------
    private function setOptions($options, $product_id) {

        $options_data = array();

        if ($product_id) {
            // Delete old product option
            $this->db->query('DELETE FROM `' . DB_PREFIX . 'product_option` WHERE  product_id = \'' . (int)$product_id . '\'');
            $this->db->query('DELETE FROM `' . DB_PREFIX . 'product_option_value` WHERE  product_id = \'' . (int)$product_id . '\'');
        }
        if(empty($options)){
            return;
        }
        $data = explode("\n", $options);



        if(!empty($data)) {

            foreach($data as $option_string){
                $option = explode('|', $option_string);

                if(empty($option)) {
                    continue;
                }

                $option[0] = trim($option[0]);
                $option[1] = trim($option[1]);
                $option[2] = trim($option[2]);
                $price = trim($option[3]);
                $price_prefix = '+';
                $points = 0;
                $points_prefix = '+';
                $weight = 0;
                $weight_prefix = '+';
                $option_required = 0;

                ##################

                $query = $this->db->query('SELECT option_id FROM `' . DB_PREFIX . 'option_description` WHERE LOWER(name) = LOWER(\'' .$this->db->escape($option[1]) . '\') AND language_id = \'' . (int)$this->setting['language_id'] . '\' LIMIT 1');

                if(isset($query->row['option_id'])) {
                    $option_id = $query->row['option_id'];
                } else {
                    // Add new Option
                    $this->db->query('INSERT INTO `' . DB_PREFIX . 'option` SET type = \'radio\', sort_order = 0');
                    $option_id = $this->db->getLastId();
                    $this->db->query('INSERT INTO `' . DB_PREFIX . 'option_description` SET language_id = \'' . (int)$this->setting['language_id'] . '\', option_id = \'' . $option_id . '\', name = \'' .$this->db->escape($option[1]) . '\'');
                }

                $query = $this->db->query('SELECT option_value_id FROM `' . DB_PREFIX . 'option_value_description` WHERE LOWER(name) = LOWER(\'' .$this->db->escape($option[2]) . '\') AND option_id = \'' . $option_id . '\' AND language_id = \'' . (int)$this->setting['language_id'] . '\' LIMIT 1');

                if(isset($query->row['option_value_id'])) {
                    $option_value_id = $query->row['option_value_id'];
                } else {
                    // Add new Option
                    $this->db->query('INSERT INTO `' . DB_PREFIX . 'option_value` SET option_id = \'' . $option_id . '\', sort_order = 0');
                    $option_value_id = $this->db->getLastId();
                    $this->db->query('INSERT INTO `' . DB_PREFIX . 'option_value_description` SET language_id = \'' . (int)$this->setting['language_id'] . '\', option_value_id = \'' . $option_value_id . '\', option_id = \'' . $option_id . '\', name = \'' .$this->db->escape($option[2]) . '\'');
                }

                $options_data[] = array(
                        'product_option_id' => '',
                        'name' => $option[1],
                        'option_id' => $option_id,
                        'type' => 'radio',
                        'required' => '1',
                        'product_option_value' =>
                            array (
                                0 =>
                                    array (
                                        'option_value_id' => $option_value_id,
                                        'product_option_value_id' => '',
                                        'quantity' =>  $option[4],
                                        'subtract' => '1',
                                        'price_prefix' => '+',
                                        'price' => '',
                                        'points_prefix' => '+',
                                        'points' => '',
                                        'weight_prefix' => '+',
                                        'weight' => '',
                                    ),
                            ),
                );

            }
        }

        return $options_data;

    }

    // Add Options
    //-------------------------------------------------------------------------
    private function setOptions2($option_name, $option_value, $product_id) {

        $options_data = array();

        if(empty($option_value)){
            return;
        }

        $option[0] = 'radio';
        $option[1] = $option_name;
        $option[2] = $option_value;
        $price = '';
        $price_prefix = '+';
        $points = 0;
        $points_prefix = '+';
        $weight = 0;
        $weight_prefix = '+';
        $option_required = 0;

                ##################

                $query = $this->db->query('SELECT option_id FROM `' . DB_PREFIX . 'option_description` WHERE LOWER(name) = LOWER(\'' .$this->db->escape($option[1]) . '\') AND language_id = \'' . (int)$this->setting['language_id'] . '\' LIMIT 1');

                if(isset($query->row['option_id'])) {
                    $option_id = $query->row['option_id'];
                } else {
                    // Add new Option
                    $this->db->query('INSERT INTO `' . DB_PREFIX . 'option` SET type = \'radio\', sort_order = 0');
                    $option_id = $this->db->getLastId();
                    $this->db->query('INSERT INTO `' . DB_PREFIX . 'option_description` SET language_id = \'' . (int)$this->setting['language_id'] . '\', option_id = \'' . $option_id . '\', name = \'' .$this->db->escape($option[1]) . '\'');
                }

                $query = $this->db->query('SELECT option_value_id FROM `' . DB_PREFIX . 'option_value_description` WHERE LOWER(name) = LOWER(\'' .$this->db->escape($option[2]) . '\') AND option_id = \'' . $option_id . '\' AND language_id = \'' . (int)$this->setting['language_id'] . '\' LIMIT 1');

                if(isset($query->row['option_value_id'])) {
                    $option_value_id = $query->row['option_value_id'];
                } else {
                    // Add new Option
                    $this->db->query('INSERT INTO `' . DB_PREFIX . 'option_value` SET option_id = \'' . $option_id . '\', sort_order = 0');
                    $option_value_id = $this->db->getLastId();
                    $this->db->query('INSERT INTO `' . DB_PREFIX . 'option_value_description` SET language_id = \'' . (int)$this->setting['language_id'] . '\', option_value_id = \'' . $option_value_id . '\', option_id = \'' . $option_id . '\', name = \'' .$this->db->escape($option[2]) . '\'');
                }

                $options_data = array(
                        'product_option_id' => '',
                        'name' => $option[1],
                        'option_id' => $option_id,
                        'type' => 'radio',
                        'required' => '0',
                        'product_option_value' =>
                            array (
                                0 =>
                                    array (
                                        'option_value_id' => $option_value_id,
                                        'product_option_value_id' => '',
                                        'quantity' =>  100,
                                        'subtract' => '1',
                                        'price_prefix' => '+',
                                        'price' => '',
                                        'points_prefix' => '+',
                                        'points' => '',
                                        'weight_prefix' => '+',
                                        'weight' => '',
                                    ),
                            ),
                );

        return $options_data;
    }


    public function setCategory($data=array()) {

            if(empty($data)) {
                return 0;
            }
            $categories_id = array();

            $categories = explode($this->CSV_CATEGORY_DEL, $data);

                $parent_id = $this->setting['parent_id'] ;
                $categories_id[] = $parent_id;
                foreach ($categories as $category) {
                    $category = trim($category, " \n\t");

                      $result = $this->db->query('SELECT cd.category_id FROM `' . DB_PREFIX . 'category_description` cd LEFT JOIN `' . DB_PREFIX . 'category` c ON (c.category_id = cd.category_id) WHERE LOWER(cd.name) = LOWER(\'' . $this->db->escape($category) . '\') AND c.parent_id = \''.$parent_id.'\' LIMIT 1');

                    if(isset($result->num_rows) AND $result->num_rows > 0 ) {
                         $category_id = $result->row['category_id'];

                    } else {
                        if($parent_id == 0) {
                            $column = 1;
                            $top = 1;
                        } else {
                            $column = 0;
                            $top = 0;
                        }

                        $this->db->query('INSERT INTO `' . DB_PREFIX . 'category` SET parent_id = ' . $parent_id . ', `top` = ' . (int)$top . ', `column` = ' . $column . ', sort_order = 1, status = 1, date_modified =  NOW(), date_added = NOW()');

                        $category_id = $this->db->getLastId();

                        $this->db->query("INSERT INTO " . DB_PREFIX . "category_description SET category_id = '" . (int)$category_id . "', language_id = '" . (int)$this->setting['language_id'] . "', name = '" . $this->db->escape($category) . "'");

// MySQL Hierarchical Data Closure Table Pattern
                        $level = 0;

                        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$parent_id . "' ORDER BY `level` ASC");

                        foreach ($query->rows as $result) {
                            $this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET `category_id` = '" . (int)$category_id . "', `path_id` = '" . (int)$result['path_id'] . "', `level` = '" . (int)$level . "'");

                            $level++;
                        }

                        $this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET `category_id` = '" . (int)$category_id . "', `path_id` = '" . (int)$category_id . "', `level` = '" . (int)$level . "'");


                      $i=0;
                      if($parent_id == 0) {
                            $parent_id =  $category_id;
                         }
                            $this->db->query("INSERT INTO " . DB_PREFIX . "category_path SET level = '" . (int)$i . "', category_id = '" . (int)$category_id . "', path_id = '" . (int)$parent_id . "'");


                        if ((isset($data['keyword'])) and (!empty($data['keyword']))) {
                            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'category_id=" . (int)$category_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
                        }	else {
                            $this->load->model('tool/alias');
                            $name_to_alias = $category;
                            $name = $this->model_tool_alias->translitIt(str_replace(' ', '-', trim($this->db->escape($name_to_alias))), ENT_QUOTES, "UTF-8").'-'.$category_id;
                            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'category_id=" . (int)$category_id . "', keyword = '" . $name . "'");
                        }

                        // Category to Store
                        foreach ($this->setting['product_store'] as $store_id) {
                            $this->db->query("INSERT INTO " . DB_PREFIX . "category_to_store SET category_id = '" . (int)$category_id . "', store_id = '" . (int)$store_id . "'");
                        }
                    }

                    $parent_id = $category_id;
                    $categories_id[] = (int)$category_id;

                }


            return array_unique($categories_id);
        }



    public function GetManufacturerIdByname($name) {

        $sql = "SELECT c.manufacturer_id FROM " . DB_PREFIX . "manufacturer c WHERE c.name = '" . $name . "'";

        $query = $this->db->query($sql);

        if (isset($query->row['manufacturer_id'])){
            return $query->row['manufacturer_id'];
        } else {
            return false;
        }

    }


    public function addManufacturer($name) {

        $this->load->model('localisation/language');
        $language_info = $this->model_localisation_language->getLanguageByCode($this->config->get('config_language'));
        $front_language_id = $language_info['language_id'];


        $this->db->query("INSERT INTO " . DB_PREFIX . "manufacturer SET name = '" . $this->db->escape($name) . "'");

        $manufacturer_id = $this->db->getLastId();


            $this->db->query("INSERT INTO " . DB_PREFIX . "manufacturer_description SET manufacturer_id = '" . (int)$manufacturer_id . "', language_id = '" . (int)$front_language_id . "', name = '" . $name . "'");


          $this->db->query("INSERT INTO " . DB_PREFIX . "manufacturer_to_store SET manufacturer_id = '" . (int)$manufacturer_id . "', store_id = '0'");

        /*mod create alias*/

            $this->load->model('tool/alias');
            $name_to_alias = $name;
            $name = $this->model_tool_alias->translitIt(str_replace(' ', '-', trim($this->db->escape($name_to_alias))), ENT_QUOTES, "UTF-8").'-'.$manufacturer_id;
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'manufacturer_id=" . (int)$manufacturer_id . "', keyword = '" . $name . "'");


        $this->cache->delete('seo_pro');

        /*!mod create alias*/

        $this->cache->delete('manufacturer');

        return $manufacturer_id;
    }



    public function getParrentCategory($parent_id = 0, $parrent_categories =array()) {

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category WHERE category_id = '" . (int)$parent_id . "'");

        if (isset($query->row['parent_id']) AND !empty($query->row['parent_id'])) {
            return $query->row['parent_id'];
        }

    }


    public function getCategoryInP5s($p5s_category_id) {

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_to_p5s   WHERE 	p5s_id  = '" . (int)$p5s_category_id . "'");

         if (isset($query->row['category_id'])){
            return $query->row['category_id'];
        };
    }



    public function addCategory($data) {

        $this->log->write("addCategory \n".print_r($data,1), 'p5s_log.txt');

        $this->db->query("INSERT INTO " . DB_PREFIX . "category SET parent_id = '" . (int)$data['parent_id'] . "', `top` = '" . (isset($data['top']) ? (int)$data['top'] : 0) . "', `column` = '" . (int)$data['column'] . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "', date_modified = NOW(), date_added = NOW()");

        $category_id = $this->db->getLastId();

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "category SET image = '" . $this->db->escape($data['image']) . "' WHERE category_id = '" . (int)$category_id . "'");
        }

        foreach ($data['category_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "category_description SET category_id = '" . (int)$category_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_h1 = '" . $this->db->escape($value['meta_h1']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
        }

        // MySQL Hierarchical Data Closure Table Pattern
        $level = 0;

        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$data['parent_id'] . "' ORDER BY `level` ASC");

        foreach ($query->rows as $result) {
            $this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET `category_id` = '" . (int)$category_id . "', `path_id` = '" . (int)$result['path_id'] . "', `level` = '" . (int)$level . "'");

            $level++;
        }

        $this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET `category_id` = '" . (int)$category_id . "', `path_id` = '" . (int)$category_id . "', `level` = '" . (int)$level . "'");

        if (isset($data['category_filter'])) {
            foreach ($data['category_filter'] as $filter_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_filter SET category_id = '" . (int)$category_id . "', filter_id = '" . (int)$filter_id . "'");
            }
        }

        if (isset($data['category_store'])) {
            foreach ($data['category_store'] as $store_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_to_store SET category_id = '" . (int)$category_id . "', store_id = '" . (int)$store_id . "'");
            }
        }

        if (isset($data['p5s_id'])) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_to_p5s SET category_id = '" . (int)$category_id . "', p5s_id = '" . (int)$data['p5s_id'] . "'");
        }

        // Set which layout to use with this category
        if (isset($data['category_layout'])) {
            foreach ($data['category_layout'] as $store_id => $layout_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_to_layout SET category_id = '" . (int)$category_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout_id . "'");
            }
        }

        /*mod create alias*/
        if ((isset($data['keyword'])) and (!empty($data['keyword']))) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'category_id=" . (int)$category_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
        }	else {
            $this->load->model('tool/alias');
            $name_to_alias = $data['category_description'][$this->config->get('config_language_id')]['name'];
            $name = $this->model_tool_alias->translitIt(str_replace(' ', '-', trim($this->db->escape($name_to_alias))), ENT_QUOTES, "UTF-8").'-'.$category_id;
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'category_id=" . (int)$category_id . "', keyword = '" . $name . "'");
        }
        $this->cache->delete('seo_pro');
        /*!mod create alias*/

        $this->cache->delete('category');

        return $category_id;
    }

    public function editCategory($data, $category_id) {

        $this->log->write("editCategory \n".print_r($data,1), 'p5s_log.txt');

        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'category_id=" . (int)$category_id . "'");

        /*mod create alias*/
        if ((isset($data['keyword'])) and (!empty($data['keyword']))) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'category_id=" . (int)$category_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
        }	else {
            $this->load->model('tool/alias');
            $name_to_alias = $data['category_description'][$this->config->get('config_language_id')]['name'];
            $name = $this->model_tool_alias->translitIt(str_replace(' ', '-', trim($this->db->escape($name_to_alias))), ENT_QUOTES, "UTF-8").'-'.$category_id;
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'category_id=" . (int)$category_id . "', keyword = '" . $name . "'");
        }

        $this->cache->delete('seo_pro');
        /*!mod create alias*/

        $this->cache->delete('category');

        return $category_id;
    }

    public function DisabledAllProducts() {

        $this->db->query("UPDATE " . DB_PREFIX . "product SET status = '0' WHERE 1");

    }

    public function getAttributeIdByName($attribute_name) {
        $query = $this->db->query("SELECT ad.attribute_id FROM " . DB_PREFIX . "attribute_description ad WHERE ad.name = '" . $this->db->escape($attribute_name) . "' AND ad.language_id = '" . (int)$this->config->get('config_language_id') . "'");

        return $query->row['attribute_id'];
    }

    public function addAttribute($data) {

        $this->db->query("INSERT INTO " . DB_PREFIX . "attribute SET attribute_group_id = '" . (int)$data['attribute_group_id'] . "', sort_order = '" . (int)$data['sort_order'] . "'");

        $attribute_id = $this->db->getLastId();

        foreach ($data['attribute_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "attribute_description SET attribute_id = '" . (int)$attribute_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "'");
        }

        return $attribute_id;
    }

    public function addProduct($data) {

        $this->event->trigger('pre.admin.product.add', $data);

        //var_export($data); exit;

        $this->db->query("INSERT INTO " . DB_PREFIX . "product SET model = '" . $this->db->escape($data['model']) . "', sku = '" . $this->db->escape($data['sku']) . "', upc = '" . $this->db->escape($data['upc']) . "', ean = '" . $this->db->escape($data['ean']) . "', jan = '" . $this->db->escape($data['jan']) . "', isbn = '" . $this->db->escape($data['isbn']) . "', mpn = '" . $this->db->escape($data['mpn']) . "', location = '" . $this->db->escape($data['location']) . "', quantity = '" . (int)$data['quantity'] . "', for_whom = '" . (int)$data['for_whom'] . "', minimum = '" . (int)$data['minimum'] . "', subtract = '" . (int)$data['subtract'] . "', stock_status_id = '" . (int)$data['stock_status_id'] . "', date_available = '" . $this->db->escape($data['date_available']) . "', manufacturer_id = '" . (int)$data['manufacturer_id'] . "', shipping = '" . (int)$data['shipping'] . "', price = '" . (float)$data['price'] . "', points = '" . (int)$data['points'] . "', weight = '" . (float)$data['weight'] . "', weight_class_id = '" . (int)$data['weight_class_id'] . "', length = '" . (float)$data['length'] . "', width = '" . (float)$data['width'] . "', height = '" . (float)$data['height'] . "', length_class_id = '" . (int)$data['length_class_id'] . "', status = '" . (int)$data['status'] . "', tax_class_id = '" . (int)$data['tax_class_id'] . "', sort_order = '" . (int)$data['sort_order'] . "', date_added = NOW()");

        $product_id = $this->db->getLastId();

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "product SET image = '" . $this->db->escape($data['image']) . "' WHERE product_id = '" . (int)$product_id . "'");
        }

        foreach ($data['product_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "product_description SET product_id = '" . (int)$product_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', short_description = '" . $this->db->escape($value['short_description']) . "', tag = '" . $this->db->escape($value['tag']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_h1 = '" . $this->db->escape($value['meta_h1']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
        }

        if (isset($data['product_store'])) {
            foreach ($data['product_store'] as $store_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$store_id . "'");
            }
        }

        if (isset($data['product_attribute'])) {
            foreach ($data['product_attribute'] as $product_attribute) {
                if ($product_attribute['attribute_id']) {
                    foreach ($product_attribute['product_attribute_description'] as $language_id => $product_attribute_description) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "product_attribute SET product_id = '" . (int)$product_id . "', attribute_id = '" . (int)$product_attribute['attribute_id'] . "', language_id = '" . (int)$language_id . "', text = '" .  $this->db->escape($product_attribute_description['text']) . "'");
                    }
                }
            }
        }

        if (isset($data['product_option'])) {
            foreach ($data['product_option'] as $product_option) {
                if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') {
                    if (isset($product_option['product_option_value'])) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', required = '" . (int)$product_option['required'] . "'");

                        $product_option_id = $this->db->getLastId();

                        foreach ($product_option['product_option_value'] as $product_option_value) {
                            $this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = '" . (int)$product_option_id . "', product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', option_value_id = '" . (int)$product_option_value['option_value_id'] . "', quantity = '" . (int)$product_option_value['quantity'] . "', subtract = '" . (int)$product_option_value['subtract'] . "', price = '" . (float)$product_option_value['price'] . "', price_prefix = '" . $this->db->escape($product_option_value['price_prefix']) . "', points = '" . (int)$product_option_value['points'] . "', points_prefix = '" . $this->db->escape($product_option_value['points_prefix']) . "', weight = '" . (float)$product_option_value['weight'] . "', weight_prefix = '" . $this->db->escape($product_option_value['weight_prefix']) . "'");
                        }
                    }
                } else {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', value = '" . $this->db->escape($product_option['value']) . "', required = '" . (int)$product_option['required'] . "'");
                }
            }
        }

        if (isset($data['product_discount'])) {
            foreach ($data['product_discount'] as $product_discount) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_discount SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$product_discount['customer_group_id'] . "', quantity = '" . (int)$product_discount['quantity'] . "', priority = '" . (int)$product_discount['priority'] . "', price = '" . (float)$product_discount['price'] . "', date_start = '" . $this->db->escape($product_discount['date_start']) . "', date_end = '" . $this->db->escape($product_discount['date_end']) . "'");
            }
        }

        if (isset($data['product_special'])) {
            foreach ($data['product_special'] as $product_special) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$product_special['customer_group_id'] . "', priority = '" . (int)$product_special['priority'] . "', price = '" . (float)$product_special['price'] . "', date_start = '" . $this->db->escape($product_special['date_start']) . "', date_end = '" . $this->db->escape($product_special['date_end']) . "'");
            }
        }

        if (isset($data['product_image'])) {
            foreach ($data['product_image'] as $product_image) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . (int)$product_id . "', image = '" . $this->db->escape($product_image['image']) . "', sort_order = '" . (int)$product_image['sort_order'] . "'");
            }
        }

        if (isset($data['product_download'])) {
            foreach ($data['product_download'] as $download_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_download SET product_id = '" . (int)$product_id . "', download_id = '" . (int)$download_id . "'");
            }
        }

        if (isset($data['product_category'])) {
            foreach ($data['product_category'] as $category_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$category_id . "'");
            }
        }

        if(isset($data['main_category_id']) && $data['main_category_id'] > 0) {
            $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "' AND category_id = '" . (int)$data['main_category_id'] . "'");
            $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$data['main_category_id'] . "', main_category = 1");
        } elseif(isset($data['product_category'][0])) {
            $this->db->query("UPDATE " . DB_PREFIX . "product_to_category SET main_category = 1 WHERE product_id = '" . (int)$product_id . "' AND category_id = '" . (int)$data['product_category'][0] . "'");
        }

        if (isset($data['product_filter'])) {
            foreach ($data['product_filter'] as $filter_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_filter SET product_id = '" . (int)$product_id . "', filter_id = '" . (int)$filter_id . "'");
            }
        }

        if (isset($data['product_related'])) {
            foreach ($data['product_related'] as $related_id) {
                $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "' AND related_id = '" . (int)$related_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int)$product_id . "', related_id = '" . (int)$related_id . "'");
                $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$related_id . "' AND related_id = '" . (int)$product_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int)$related_id . "', related_id = '" . (int)$product_id . "'");
            }
        }

        if (isset($data['product_reward'])) {
            foreach ($data['product_reward'] as $customer_group_id => $product_reward) {
                if ((int)$product_reward['points'] > 0) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_reward SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$customer_group_id . "', points = '" . (int)$product_reward['points'] . "'");
                }
            }
        }

        if (isset($data['product_layout'])) {
            foreach ($data['product_layout'] as $store_id => $layout_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_layout SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout_id . "'");
            }
        }
        /*mod create alias*/
        if ((isset($data['keyword'])) and (!empty($data['keyword']))) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'product_id=" . (int)$product_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
        }	else {
            $this->load->model('tool/alias');
            $name_to_alias = $data['product_description'][$this->config->get('config_language_id')]['name'];
            $name = $this->model_tool_alias->translitIt(str_replace(' ', '-', trim($this->db->escape($name_to_alias))), ENT_QUOTES, "UTF-8").'-'.$product_id;
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'product_id=" . (int)$product_id . "', keyword = '" . $name . "'");
        }
        $this->cache->delete('seo_pro');

        /*!mod create alias*/


        if (isset($data['product_recurrings'])) {
            foreach ($data['product_recurrings'] as $recurring) {
                $this->db->query("INSERT INTO `" . DB_PREFIX . "product_recurring` SET `product_id` = " . (int)$product_id . ", customer_group_id = " . (int)$recurring['customer_group_id'] . ", `recurring_id` = " . (int)$recurring['recurring_id']);
            }
        }

        $this->cache->delete('product');

        $this->event->trigger('post.admin.product.add', $product_id);

        return $product_id;
    }

    public function editProduct($product_id, $data) {
        $this->event->trigger('pre.admin.product.edit', $data);


        //   var_export($data); exit;

        $this->db->query("UPDATE " . DB_PREFIX . "product SET model = '" . $this->db->escape($data['model']) . "', sku = '" . $this->db->escape($data['sku']) . "', upc = '" . $this->db->escape($data['upc']) . "', ean = '" . $this->db->escape($data['ean']) . "', jan = '" . $this->db->escape($data['jan']) . "', isbn = '" . $this->db->escape($data['isbn']) . "', mpn = '" . $this->db->escape($data['mpn']) . "', location = '" . $this->db->escape($data['location']) . "', quantity = '" . (int)$data['quantity'] . "', for_whom = '" . (int)$data['for_whom'] . "', minimum = '" . (int)$data['minimum'] . "', subtract = '" . (int)$data['subtract'] . "', stock_status_id = '" . (int)$data['stock_status_id'] . "', date_available = '" . $this->db->escape($data['date_available']) . "', manufacturer_id = '" . (int)$data['manufacturer_id'] . "', shipping = '" . (int)$data['shipping'] . "', price = '" . (float)$data['price'] . "', points = '" . (int)$data['points'] . "', weight = '" . (float)$data['weight'] . "', weight_class_id = '" . (int)$data['weight_class_id'] . "', length = '" . (float)$data['length'] . "', width = '" . (float)$data['width'] . "', height = '" . (float)$data['height'] . "', length_class_id = '" . (int)$data['length_class_id'] . "', status = '" . (int)$data['status'] . "', tax_class_id = '" . (int)$data['tax_class_id'] . "', sort_order = '" . (int)$data['sort_order'] . "', date_modified = NOW() WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "product SET image = '" . $this->db->escape($data['image']) . "' WHERE product_id = '" . (int)$product_id . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$product_id . "'");

        foreach ($data['product_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "product_description SET product_id = '" . (int)$product_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', short_description = '" . $this->db->escape($value['short_description']) . "', tag = '" . $this->db->escape($value['tag']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_h1 = '" . $this->db->escape($value['meta_h1']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_store'])) {
            foreach ($data['product_store'] as $store_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$store_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "'");

        if (!empty($data['product_attribute'])) {
            foreach ($data['product_attribute'] as $product_attribute) {
                if ($product_attribute['attribute_id']) {
                    foreach ($product_attribute['product_attribute_description'] as $language_id => $product_attribute_description) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "product_attribute SET product_id = '" . (int)$product_id . "', attribute_id = '" . (int)$product_attribute['attribute_id'] . "', language_id = '" . (int)$language_id . "', text = '" .  $this->db->escape($product_attribute_description['text']) . "'");
                    }
                }
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_option WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_option_value WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_option'])) {
            foreach ($data['product_option'] as $product_option) {
                if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') {
                    if (isset($product_option['product_option_value'])) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_option_id = '" . (int)$product_option['product_option_id'] . "', product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', required = '" . (int)$product_option['required'] . "'");

                        $product_option_id = $this->db->getLastId();

                        foreach ($product_option['product_option_value'] as $product_option_value) {
                            $this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_value_id = '" . (int)$product_option_value['product_option_value_id'] . "', product_option_id = '" . (int)$product_option_id . "', product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', option_value_id = '" . (int)$product_option_value['option_value_id'] . "', quantity = '" . (int)$product_option_value['quantity'] . "', subtract = '" . (int)$product_option_value['subtract'] . "', price = '" . (float)$product_option_value['price'] . "', price_prefix = '" . $this->db->escape($product_option_value['price_prefix']) . "', points = '" . (int)$product_option_value['points'] . "', points_prefix = '" . $this->db->escape($product_option_value['points_prefix']) . "', weight = '" . (float)$product_option_value['weight'] . "', weight_prefix = '" . $this->db->escape($product_option_value['weight_prefix']) . "'");
                        }
                    }
                } else {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_option_id = '" . (int)$product_option['product_option_id'] . "', product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', value = '" . $this->db->escape($product_option['value']) . "', required = '" . (int)$product_option['required'] . "'");
                }
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_discount'])) {
            foreach ($data['product_discount'] as $product_discount) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_discount SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$product_discount['customer_group_id'] . "', quantity = '" . (int)$product_discount['quantity'] . "', priority = '" . (int)$product_discount['priority'] . "', price = '" . (float)$product_discount['price'] . "', date_start = '" . $this->db->escape($product_discount['date_start']) . "', date_end = '" . $this->db->escape($product_discount['date_end']) . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_special'])) {
            foreach ($data['product_special'] as $product_special) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$product_special['customer_group_id'] . "', priority = '" . (int)$product_special['priority'] . "', price = '" . (float)$product_special['price'] . "', date_start = '" . $this->db->escape($product_special['date_start']) . "', date_end = '" . $this->db->escape($product_special['date_end']) . "'");
            }
        }
if ($this->setting['image']==1) {
    $this->db->query("DELETE FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "'");

    if (isset($data['product_image'])) {
        foreach ($data['product_image'] as $product_image) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . (int)$product_id . "', image = '" . $this->db->escape($product_image['image']) . "', sort_order = '" . (int)$product_image['sort_order'] . "'");
        }
    }
}
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_download WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_download'])) {
            foreach ($data['product_download'] as $download_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_download SET product_id = '" . (int)$product_id . "', download_id = '" . (int)$download_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_category'])) {
            foreach ($data['product_category'] as $category_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$category_id . "'");
            }
        }

        if(isset($data['main_category_id']) && $data['main_category_id'] > 0) {
            $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "' AND category_id = '" . (int)$data['main_category_id'] . "'");
            $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$data['main_category_id'] . "', main_category = 1");
        } elseif(isset($data['product_category'][0])) {
            $this->db->query("UPDATE " . DB_PREFIX . "product_to_category SET main_category = 1 WHERE product_id = '" . (int)$product_id . "' AND category_id = '" . (int)$data['product_category'][0] . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_filter WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_filter'])) {
            foreach ($data['product_filter'] as $filter_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_filter SET product_id = '" . (int)$product_id . "', filter_id = '" . (int)$filter_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE related_id = '" . (int)$product_id . "'");

        if (isset($data['product_related'])) {
            foreach ($data['product_related'] as $related_id) {
                $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "' AND related_id = '" . (int)$related_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int)$product_id . "', related_id = '" . (int)$related_id . "'");
                $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$related_id . "' AND related_id = '" . (int)$product_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int)$related_id . "', related_id = '" . (int)$product_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_reward'])) {
            foreach ($data['product_reward'] as $customer_group_id => $value) {
                if ((int)$value['points'] > 0) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_reward SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$customer_group_id . "', points = '" . (int)$value['points'] . "'");
                }
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_layout WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_layout'])) {
            foreach ($data['product_layout'] as $store_id => $layout_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_layout SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'product_id=" . (int)$product_id . "'");

        /*mod create alias*/
        if ((isset($data['keyword'])) and (!empty($data['keyword']))) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'product_id=" . (int)$product_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
        }	else {
            $this->load->model('tool/alias');
            $name_to_alias = $data['product_description'][$this->config->get('config_language_id')]['name'];
            $name = $this->model_tool_alias->translitIt(str_replace(' ', '-', trim($this->db->escape($name_to_alias))), ENT_QUOTES, "UTF-8").'-'.$product_id;
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'product_id=" . (int)$product_id . "', keyword = '" . $name . "'");
        }
        $this->cache->delete('seo_pro');

        /*!mod create alias*/

        $this->db->query("DELETE FROM `" . DB_PREFIX . "product_recurring` WHERE product_id = " . (int)$product_id);

        if (isset($data['product_recurring'])) {
            foreach ($data['product_recurring'] as $product_recurring) {
                $this->db->query("INSERT INTO `" . DB_PREFIX . "product_recurring` SET `product_id` = " . (int)$product_id . ", customer_group_id = " . (int)$product_recurring['customer_group_id'] . ", `recurring_id` = " . (int)$product_recurring['recurring_id']);
            }
        }

        $this->cache->delete('product');

        $this->event->trigger('post.admin.product.edit', $product_id);

        return true;
    }





}