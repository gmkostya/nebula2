<?php
class ModelModuleMainLanguageBanner extends Model
{
	public function getMainBanner()
	{

	    $def_language = $this->getLanguageByCode($this->config->get('config_language_default'));

        $main_language_banner = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "main_banner mb LEFT JOIN " . DB_PREFIX . "main_banner_description mbd ON (mb.id  = mbd.main_banner_id) WHERE mbd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY mb.sort_order ASC");



        foreach ($query->rows as $row) {
            $def_image = $this->db->query("SELECT image FROM " . DB_PREFIX . "main_banner_description WHERE main_banner_id = '".(int)$row['id']."' AND language_id='".(int)$def_language['language_id']."'");


            if(!$row['image']){
                $row['image'] = $def_image->row['image'];
            }
            if(!$row['image_mobile']){
                $row['image_mobile'] = $row['image'];
            }

            $main_language_banner[] = array(
                'main_banner_id' => $row['main_banner_id'],
                'title' => $row['title'],
                'link' => $row['link'],
                'image' => $row['image'],
                'image_mobile' => $row['image_mobile'],
            );
        }

        return $main_language_banner;
    }

    public function getLanguageByCode($code) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "language WHERE code = '" . $this->db->escape($code) . "'");

        return $query->row;
    }
}
