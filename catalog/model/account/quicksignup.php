<?php
class ModelAccountQuicksignup extends Model {
	public function addCustomer($data) {
		if(!empty($data['name'])){
			$customer_name = explode(' ',$data['name']);
			$data['firstname']='';
			$data['lastname'] = '';
			if(isset($customer_name[0])){
				$data['firstname'] = $customer_name[0];
			}
			if(isset($customer_name[1])) {
				$data['lastname'] .= $customer_name[1];
			}
			if(isset($customer_name[2])) {
				$data['lastname'] .= ' '.$customer_name[2];
			}
			if(isset($customer_name[3])) {
				$data['lastname'] .= ' '.$customer_name[3];
			}
		}
		
		if (isset($data['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($data['customer_group_id'], $this->config->get('config_customer_group_display'))) {
			$customer_group_id = $data['customer_group_id'];
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}

		if(!isset($data['telephone'])){
            $data['telephone'] = '';
        }

		if(!isset($data['newsletter'])){
            $data['newsletter'] = 0;
        }

		$this->load->model('account/customer_group');

		$customer_group_info = $this->model_account_customer_group->getCustomerGroup($customer_group_id);

		$this->db->query("INSERT INTO " . DB_PREFIX . "customer SET customer_group_id = '" . (int)$customer_group_id . "', store_id = '" . (int)$this->config->get('config_store_id') . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', salt = '" . $this->db->escape($salt = substr(md5(uniqid(rand(), true)), 0, 9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', status = '1', approved = '" . (int)!$customer_group_info['approval'] . "', newsletter = '" . (int)$data['newsletter'] . "', date_added = NOW()");

		$customer_id = $this->db->getLastId();

		$this->load->language('mail/customer');

		$subject = sprintf($this->language->get('text_subject'), $this->config->get('config_name'));

        $message  = html_entity_decode(sprintf($this->language->get('text_logo'), $this->config->get('config_url'), $this->config->get('config_url') . 'image/' . $this->config->get('config_logo'), $this->config->get('config_name')), ENT_QUOTES, 'UTF-8') . "<br/><br/>";

        $message .= sprintf($this->language->get('text_welcome'), $this->config->get('config_name')) . "<br/><br/>";

		if (!$customer_group_info['approval']) {
			$message .= $this->language->get('text_login') . "<br/>";
		} else {
			$message .= $this->language->get('text_approval') . "<br/>";
		}


		$message .= sprintf('<a href=%s>%s</a>',$this->url->link('common/home', '', 'SSL'),$this->url->link('common/home', '', 'SSL')). "<br/><br/>";
		$message .= $this->language->get('text_services') . "<br/><br/>";
		$message .= $this->language->get('text_thanks') . "<br/>";
		$message .= $this->config->get('config_name');

		$mail = new Mail();
		$mail->setTo($data['email']);
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender($this->config->get('config_name'));
		$mail->setSubject($subject);
		$mail->setHtml(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
		$mail->send();

		// Send to main admin email if new account email is enabled
		if ($this->config->get('config_account_mail')) {
            $message  = html_entity_decode(sprintf($this->language->get('text_logo'), $this->config->get('config_url'), $this->config->get('config_url') . 'image/' . $this->config->get('config_logo'), $this->config->get('config_name')), ENT_QUOTES, 'UTF-8') . "<br/><br/>";
            $message .= $this->language->get('text_signup') . "<br/><br/>";
			$message .= $this->language->get('text_website') . ' ' . $this->config->get('config_name') . "<br/>";
			$message .= $this->language->get('text_firstname') . ' ' . $data['firstname'] . "<br/>";
			
			if(!empty($data['lastname'])) {
				$message .= $this->language->get('text_lastname') . ' ' . $data['lastname'] . "<br/>";
			}
			
			$message .= $this->language->get('text_customer_group') . ' ' . $customer_group_info['name'] . "<br/>";
			$message .= $this->language->get('text_email') . ' '  .  $data['email'] . "<br/>";
//			$message .= $this->language->get('text_telephone') . ' ' . $data['telephone'] . "<br/>";

			$mail->setTo($this->config->get('config_email'));
			$mail->setSubject(html_entity_decode($this->language->get('text_new_customer'), ENT_QUOTES, 'UTF-8'));
			$mail->setHtml(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
			$mail->send();

			// Send to additional alert emails if new account email is enabled
			$emails = explode(',', $this->config->get('config_mail_alert'));

			foreach ($emails as $email) {
				if (utf8_strlen($email) > 0 && preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $email)) {
					$mail->setTo($email);
					$mail->send();
				}
			}
		}

		return $customer_id;
	}

}