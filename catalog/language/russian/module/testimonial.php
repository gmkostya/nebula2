<?php
// Text
$_['heading_title']	           = 'Отзывы';
$_['tab_shop_review']	       = 'Отзывы о магазине';
$_['tab_review']	           = 'Отзывы о товарах';
$_['text_heading']	           = 'Поделись своим опытом... А мы раскажем о своем...';

$_['text_write']               = 'Оставьте свой отзыв';
$_['text_login']               = 'Please <a href="%s">login</a> or <a href="%s">register</a> to review';
$_['text_no_reviews']          = 'На данный момент нет отзывов.';
$_['text_note']                = '<span class="text-danger">Note:</span> HTML is not translated!';
$_['text_success']             = 'Благодарим вас за отзыв. Он был представлен администратору для утверждения.';
$_['text_title']               = 'Это было прекрасно...';
$_['text_subtitle']            = 'Спасибо за отзыв';


$_['text_mail_subject']        = 'You have a new testimonial (%s).';
$_['text_mail_waiting']	       = 'You have a new testimonial waiting.';
$_['text_mail_author']	       = 'Author: %s';
$_['text_mail_rating']	       = 'Rating: %s';
$_['text_mail_text']	       = 'Text:';

// Entry
$_['entry_name']               = 'Ваше имя';
$_['entry_review']             = 'Расскажи нам о своих впечатлениях';
$_['entry_email']              = 'Email';
$_['entry_phone']              = 'Телефон';
$_['entry_rating']             = 'Рейтинг';
$_['entry_good']               = 'Good';
$_['entry_bad']                = 'Bad';

// Button
$_['button_continue']          = 'Continue';

// Error
$_['error_name']               = 'Внимание: имя должно быть от 3 до 25 символов!';
$_['error_text']               = 'Внимание: сообщение должно составлять от 25 до 3000 символов!';
$_['error_email']              = 'Неверно введен email!';
$_['error_rating']             = 'Внимание: Выберите оценку рейтинга!';
$_['error_captcha']            = 'Warning: Verification code does not match the image!';

