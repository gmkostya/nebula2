<?php
// Heading 
$_['heading_title'] 		 = 'Подписка на рассылку';

// Text
$_['text_intro']	 		 = 'Не пропусти самое интересное';
$_['text_description']	 	 = 'Our promise to you: You will only receive the emails that you permitted upon subscription. Your email address will never be shared with any 3rd parties and you will receive only the type of content for which you signed up.';
$_['text_subscribed']   	 = 'Подписка оформлена!';
$_['text_title']             = 'Это было прекрасно...';
$_['text_unsubscribed']   	 = 'Вы уже подписаны!';
$_['text_subject'] 			 = 'Подписка';
$_['text_message'] 			 = 'Email: %s';
$_['text_success'] 			 = 'Мы благодарны за Ваш выбор.<br>Делайте свою жизнь разнообразной вместе с нами';

//Fields
$_['entry_email'] 			 = 'Твой e-mail';

//Buttons
$_['text_button'] 			 = 'Подписаться';

//Error
$_['error_invalid'] 		 = 'Некорректный e-mail!';
