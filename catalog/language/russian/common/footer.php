<?php
// Text
$_['text_information']  = 'Информация';
$_['text_service']      = 'Служба поддержки';
$_['text_extra']        = 'Дополнительно';
$_['text_contact']      = 'Связаться с нами';
$_['text_return']       = 'Возврат товара';
$_['text_sitemap']      = 'Карта сайта';
$_['text_manufacturer'] = 'Производители';
$_['text_voucher']      = 'Подарочные сертификаты';
$_['text_affiliate']    = 'Партнёры';
$_['text_special']      = 'Товары со скидкой';
$_['text_account']      = 'МОЙ Nebulatoy';
$_['text_edit']         = 'Мои данные';
$_['text_order']        = 'История покупок';
$_['text_wishlist']     = 'Мой вишлист';
$_['text_newsletter']   = 'Рассылка новостей';
$_['text_shop']   = 'Магазин';
$_['text_exit']   = 'Выход';
//$_['text_powered']      = 'Работает на <a target="_blank" href="http://myopencart.com/">ocStore</a><br /> %s &copy; %s';
$_['text_powered']      = '© Nebulatoys 2012-2019, Все права защищены';

$_['entry_name']      	= 'Имя';
$_['entry_phone']       = 'Номер мобильного телефона';
$_['text_call']      	= 'Заказать звонок';
$_['text_send']      	= 'Отправить';
$_['text_loading']      = 'Обработка';
$_['text_news']          = 'Новости';
$_['text_gallery']       = 'Галерея';
$_['text_contact']       = 'Контакты';
$_['text_review']        = 'Отзывы';
