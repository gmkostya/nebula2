<?php
// Text
$_['text_signin_register']    = 'Sign In/Register';
$_['text_login']   			  = 'Sign In';
$_['text_register']    		  = 'Register';

$_['text_new_customer']       = 'New Customer';
$_['text_returning']          = 'Returning Customer';
$_['text_returning_customer'] = 'I am a returning customer';
$_['text_details']            = 'Your Personal Details';
$_['entry_email']             = 'E-mail';
$_['entry_name']              = 'Имя';
$_['entry_password']          = 'Пароль';
$_['entry_password2']         = 'Давай еще разок';
$_['entry_telephone']         = 'Telephone';
$_['text_forgotten']          = 'Forgotten Password';
$_['text_agree']              = 'I agree to the <a href="%s" class="agree"><b>%s</b></a>';



//Button
$_['button_login']            = 'Войти';
$_['button_register']         = 'Зарегестрироваться';

//Error
$_['error_name']           = 'Имя должно быть от 1 до 32 символов!';
$_['error_email']          = 'Email не является действительным!';
$_['error_telephone']      = 'Телефон должен быть от 3 до 32 символов!';
$_['error_password']       = 'Пароль должен быть от 4 до 20 символов!';
$_['error_password2']      = 'Повторите пароль!';
$_['error_password3']      = 'Пароли должны совпадать!';
$_['error_exists']         = 'Адрес электронной почты уже зарегистрирован!';
$_['error_agree']          = 'Вы должны согласиться на% s!';
$_['error_warning']        = 'Пожалуйста, внимательно проверьте форму на наличие ошибок!';
$_['error_approved']       = 'Ваша учетная запись требует одобрения, прежде чем вы сможете войти';
$_['error_login']          = 'Нет совпадения для адреса<br>электронной почты и / или пароля.';