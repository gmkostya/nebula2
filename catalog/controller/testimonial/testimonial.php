<?php
class ControllerTestimonialTestimonial extends Controller
{
    private $error = array();
    private $moduleName 			= 'testimonial';
    private $moduleModel 			= 'model_module_testimonial';
    private $moduleModelPath 		= 'module/testimonial';
    private $modulePath 		    = 'testimonial/testimonial';
    private $moduleVersion 			= '1.4.2';

    public function index() {
        
        $lang_ar = $this->load->language($this->moduleModelPath);

        foreach($lang_ar as $key => $item){
            $data[$key] = $item;
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['heading_title'] = $this->language->get('heading_title');
        $data['tab_shop_review'] = $this->language->get('tab_shop_review');
        $data['tab_review'] = $this->language->get('tab_review');
        $data['text_heading'] = $this->language->get('text_heading');
        $data['button_send'] = $this->language->get('button_send');

        $this->document->setTitle($data['heading_title']);

//        $this->document->addScript('catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js');
//        $this->document->addStyle('catalog/view/javascript/jquery/magnific/magnific-popup.css');
        
        $data['text_login'] = sprintf($this->language->get('text_login'), $this->url->link('account/login', '', 'SSL'), $this->url->link('account/register', '', 'SSL'));

        $this->load->model($this->moduleModelPath);

        $data['review_status'] = $this->config->get('config_review_status');

        if ($this->config->get('config_review_guest') || $this->customer->isLogged()) {
            $data['review_guest'] = true;
        } else {
            $data['review_guest'] = false;
        }

        if ($this->customer->isLogged()) {
            $data['customer_name'] = $this->customer->getFirstName() . '&nbsp;' . $this->customer->getLastName();
        } else {
            $data['customer_name'] = '';
        }

        $data['breadcrumbs'][] = array(
            'text' => $data['heading_title'],
            'href' => $this->url->link($this->modulePath)
        );

        $data['review'] = 'index.php?route=' . $this->modulePath . '/review';
        $data['write'] = 'index.php?route=' . $this->modulePath . '/write';
        $data['rev_products'] = 'index.php?route=' . $this->modulePath . '/rev_products';
        $data['text_notifications'] = sprintf($this->language->get('text_notifications'),$this->url->link('index.php?route=information/information', 'information_id=5', 'SSL'));
//        $data['review_prod'] = 'index.php?route=' . $this->modulePath . '/review_prod';


        if(substr(VERSION, 0, 7) > '2.1.0.1'){
            if ($this->config->get($this->config->get('config_captcha') . '_status') && in_array('testimonial', (array)$this->config->get('config_captcha_page'))) {
                $data['captcha'] = $this->load->controller('captcha/' . $this->config->get('config_captcha'));
            } else {
                $data['captcha'] = '';
            }
        }else{
            if ($this->config->get('config_google_captcha_status')) {
                $this->document->addScript('https://www.google.com/recaptcha/api.js');
                $data['site_key'] = $this->config->get('config_google_captcha_public');
            } else {
                $data['site_key'] = '';
            }
        }

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        if(substr(VERSION, 0, 7) > '2.1.0.2'){
            $this->response->setOutput($this->load->view($this->modulePath, $data));
        }else{
            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/' . $this->modulePath . '.tpl')) {
                $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/' . $this->modulePath . '.tpl', $data));
            } else {
                $this->response->setOutput($this->load->view('default/template/' . $this->modulePath . '.tpl', $data));
            }
        }
    }

    public function rev_products() {
        $lang_ar = $this->load->language($this->moduleModelPath);

        foreach($lang_ar as $key => $item){
            $data[$key] = $item;
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['heading_title'] = $this->language->get('heading_title');
        $data['tab_shop_review'] = $this->language->get('tab_shop_review');
        $data['tab_review'] = $this->language->get('tab_review');
        $data['text_heading'] = $this->language->get('text_heading');
        $data['button_send'] = $this->language->get('button_send');

        $this->document->setTitle($data['heading_title']);

//        $this->document->addScript('catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js');
//        $this->document->addStyle('catalog/view/javascript/jquery/magnific/magnific-popup.css');

        $data['text_login'] = sprintf($this->language->get('text_login'), $this->url->link('account/login', '', 'SSL'), $this->url->link('account/register', '', 'SSL'));

        $this->load->model($this->moduleModelPath);

        $data['review_status'] = $this->config->get('config_review_status');

        if ($this->config->get('config_review_guest') || $this->customer->isLogged()) {
            $data['review_guest'] = true;
        } else {
            $data['review_guest'] = false;
        }

        if ($this->customer->isLogged()) {
            $data['customer_name'] = $this->customer->getFirstName() . '&nbsp;' . $this->customer->getLastName();
        } else {
            $data['customer_name'] = '';
        }

        $data['breadcrumbs'][] = array(
            'text' => $data['heading_title'],
            'href' => $this->url->link($this->modulePath)
        );

//        $data['review'] = 'index.php?route=' . $this->modulePath . '/review';
//        $data['write'] = 'index.php?route=' . $this->modulePath . '/write';
        $data['review_prod'] = 'index.php?route=' . $this->modulePath . '/review_prod';
        $data['rev_shop'] = 'index.php?route=' . $this->modulePath;


        /*if(substr(VERSION, 0, 7) > '2.1.0.1'){
            if ($this->config->get($this->config->get('config_captcha') . '_status') && in_array('testimonial', (array)$this->config->get('config_captcha_page'))) {
                $data['captcha'] = $this->load->controller('captcha/' . $this->config->get('config_captcha'));
            } else {
                $data['captcha'] = '';
            }
        }else{
            if ($this->config->get('config_google_captcha_status')) {
                $this->document->addScript('https://www.google.com/recaptcha/api.js');
                $data['site_key'] = $this->config->get('config_google_captcha_public');
            } else {
                $data['site_key'] = '';
            }
        }*/

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');
        
        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/' . $this->modulePath . '_products.tpl')) {
            $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/' . $this->modulePath . '_products.tpl', $data));
        } else {
            $this->response->setOutput($this->load->view('default/template/' . $this->modulePath . '_products.tpl', $data));
        }
    }

    public function review() {
        $this->load->language($this->moduleModelPath);
        $this->load->model($this->moduleModelPath);

        $data['text_no_reviews'] = $this->language->get('text_no_reviews');

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $limit = 10;

        $data['reviews'] = array();

        $review_total = $this->{$this->moduleModel}->getTotalReviews();

        $results = $this->{$this->moduleModel}->getReviews(($page - 1) * $limit, $limit);

        foreach ($results as $result) {
            $data['reviews'][] = array(
                'author'     => $result['author'],
                'text'       => nl2br($result['text']),
                'rating'     => (int)$result['rating'],
                //'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added']))
                'date_added' => date('d-m-Y', strtotime($result['date_added']))
            );
        }

        $pagination = new Pagination();

        $pagination->total = $review_total;
        $pagination->page = $page;
        $pagination->limit = $limit;
        $pagination->text_prev = $this->language->get('text_prev');
        $pagination->text_next = $this->language->get('text_next');
        $pagination->url = 'index.php?route=' . $this->modulePath . '/review&page={page}';

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($review_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($review_total - $limit)) ? $review_total : ((($page - 1) * $limit) + $limit), $review_total, ceil($review_total / $limit));

        if(substr(VERSION, 0, 7) > '2.1.0.2'){
            $this->response->setOutput($this->load->view($this->modulePath . '_list', $data));
        }else{
            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/' . $this->modulePath . '_list.tpl')) {
                $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/' . $this->modulePath . '_list.tpl', $data));
            } else {
                $this->response->setOutput($this->load->view('default/template/' . $this->modulePath . '_list.tpl', $data));
            }
        }
    }

    public function write() {
        
        $this->load->language($this->moduleModelPath);

        if ($this->request->server['REQUEST_METHOD'] == 'POST') {
            
            if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 25)) {
                $json['error']['name'] = $this->language->get('error_name');
            }

            /*if ((utf8_strlen($this->request->post['phone']) != 19)) {
                $json['error']['phone'] = $this->language->get('error_phone');
            }*/

            if ((utf8_strlen($this->request->post['text']) < 25) || (utf8_strlen($this->request->post['text']) > 3000)) {
                $json['error']['text'] = $this->language->get('error_text');
            }

            /*if (!filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
                $json['error']['email'] = $this->language->get('error_email');
            }*/

            if (empty($this->request->post['rating']) || $this->request->post['rating'] < 0 || $this->request->post['rating'] > 5) {
                $json['error']['rating'] = $this->language->get('error_rating');
            }

            if ($this->config->get('config_google_captcha_status')) {
                $recaptcha = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode($this->config->get('config_google_captcha_secret')) . '&response=' . $this->request->post['g-recaptcha-response'] . '&remoteip=' . $this->request->server['REMOTE_ADDR']);

                $recaptcha = json_decode($recaptcha, true);

                if (!$recaptcha['success']) {
                    $json['error'] = $this->language->get('error_captcha');
                }
            }

            if ($this->config->get($this->config->get('config_captcha') . '_status') && in_array('testimonial', (array)$this->config->get('config_captcha_page'))) {
                $captcha = $this->load->controller('captcha/' . $this->config->get('config_captcha') . '/validate');

                if ($captcha) {
                    $json['error'] = $captcha;
                }
            }

            if (!isset($json['error'])) {
                $this->load->model($this->moduleModelPath);
                $json['success'] = $this->language->get('text_success');
                $json['title'] = $this->language->get('text_title');
                $json['subtitle'] = $this->language->get('text_subtitle');

                $this->{$this->moduleModel}->addReview($this->request->post);
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
    public function review_prod() {
        $this->load->language('product/product');

        $this->load->model('catalog/review');
        $this->load->model('tool/image');

        $data['text_no_reviews'] = $this->language->get('text_no_reviews');

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $limit = 10;

        $data['reviews'] = array();

        $review_total = $this->model_catalog_review->getTotalReviewsByProduct();


        $results = $this->model_catalog_review->getReviewsByProduct(($page - 1) * $limit, $limit);
//        wss_log($results);
//        var_dump($results);
        foreach ($results as $result) {
            if ($result['image']) {
                $image = $this->model_tool_image->resize($result['image'], 122, 122);
            } else {
                $image = $this->model_tool_image->resize('no_image.png', 122, 122);
            }

            $data['reviews'][] = array(
                'author'     => $result['author'],
                'text'       => nl2br($result['text']),
                'prod_name'  => nl2br($result['name']),
                'href'       => $this->url->link('product/product', 'product_id=' . $result['product_id']),
                'rating'     => (int)$result['rating'],
                'image'      => $image,
                //'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added']))
                'date_added' => date('d-m-Y', strtotime($result['date_added']))
            );
        }

        $pagination = new Pagination();
        $pagination->total = $review_total;
        $pagination->page = $page;
        $pagination->limit = $limit;
        $pagination->text_prev = $this->language->get('text_prev');
        $pagination->text_next = $this->language->get('text_next');
        $pagination->url = 'index.php?route=' . $this->modulePath . '/review_prod&page={page}';

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($review_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($review_total - $limit)) ? $review_total : ((($page - 1) * $limit) + $limit), $review_total, ceil($review_total / $limit));

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/testimonial/review_all_products.tpl')) {
            $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/testimonial/review_all_products.tpl', $data));
        } else {
            $this->response->setOutput($this->load->view('default/template/testimonial/review_all_products.tpl', $data));
        }
    }
}