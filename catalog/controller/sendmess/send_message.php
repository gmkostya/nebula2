<?php
class ControllerSendmessSendMessage extends Controller
{
    private $error = array();

    public function send()
    {
        $json = array();
        
        $this->load->language('sendmess/sendmess');

        $data['text_name']          =  $this->language->get('text_name');
        $data['text_email']         =  $this->language->get('text_email');
        $data['text_message']       =  $this->language->get('text_message');
        $data['text_phone']         =  $this->language->get('text_phone');
        $data['text_type']          =  $this->language->get('text_type');
        $data['text_manufacturer']  =  $this->language->get('text_manufacturer');
        $data['text_model']         =  $this->language->get('text_model');

        $data['error_name']         =  $this->language->get('error_name');
        $data['error_email']        =  $this->language->get('error_email');
        $data['error_phone']        =  $this->language->get('error_phone');
        $data['error_message']      =  $this->language->get('error_message');
        $data['success']            =  $this->language->get('success');

        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            if (isset($this->request->post['goal'])){
                if ($this->request->post['goal'] == 'send_message'):
                    if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 25)) {
                        $json['error'] = true;
                        $json['error_name'] = $data['error_name'];
                    }
                    if ((utf8_strlen($this->request->post['email']) < 5)) {
                        $json['error'] = true;
                        $json['error_email'] = $data['error_email'];
                    }
                    if ((utf8_strlen($this->request->post['message']) < 10)) {
                        $json['error'] = true;
                        $json['error_message'] = $data['error_message'];
                    }
                    if (!isset($json['error'])):        
                        $mail = new Mail();
                        $mail->protocol = $this->config->get('config_mail_protocol');
                        $mail->parameter = $this->config->get('config_mail_parameter');
                        $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
                        $mail->smtp_username = $this->config->get('config_mail_smtp_username');
                        $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
                        $mail->smtp_port = $this->config->get('config_mail_smtp_port');
                        $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
                        $mail->setTo($this->config->get('config_email'));
                        $mail->setFrom($this->config->get('config_name'));
                        $mail->setSender(html_entity_decode($this->request->post['name'], ENT_QUOTES, 'UTF-8'));
                        $mail->setSubject(html_entity_decode(sprintf($this->language->get('email_subject_message'), $this->request->post['name']), ENT_QUOTES, 'UTF-8'));
                        $html = '<table style="width: 100%; background-color: #f1f1f1;">
                                    <tbody>
                                        <tr>
                                            <td style="width: 30%; background-color: #f3f3f3;">'.$data['text_name'].'</td><td>'.$this->request->post['name'].'</td>
                                        </tr>
                                        <tr>
                                            <td style="width: 30%; background-color: #f3f3f3;">'.$data['text_email'].'</td><td>'.$this->request->post['email'].'</td>
                                        </tr>
                                        <tr>
                                            <td style="width: 30%; background-color: #f3f3f3;">'.$data['text_message'].'</td><td>'.$this->request->post['message'].'</td>
                                        </tr>
                                    </tbody>
                                </table>';
                        $mail->setHtml($html);
                        $mail->send();
                        $json['success'] = $data['success'];
                    endif;
                endif;
                if ($this->request->post['goal'] == 'callback_request'):
                    if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 25)) {
                        $json['error'] = true;
                        $json['error_name'] = $data['error_name'];
                    }
                    if ((utf8_strlen($this->request->post['phone']) < 5)) {
                        $json['error'] = true;
                        $json['error_phone'] = $data['error_phone'];
                    }
                    if (!isset($json['error'])):
                        $mail = new Mail();
                        $mail->protocol = $this->config->get('config_mail_protocol');
                        $mail->parameter = $this->config->get('config_mail_parameter');
                        $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
                        $mail->smtp_username = $this->config->get('config_mail_smtp_username');
                        $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
                        $mail->smtp_port = $this->config->get('config_mail_smtp_port');
                        $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
                        $mail->setTo($this->config->get('config_email'));
                        $mail->setFrom($this->config->get('config_email'));
                        $mail->setSender(html_entity_decode($this->request->post['phone'], ENT_QUOTES, 'UTF-8'));
                        $mail->setSubject(html_entity_decode(sprintf($this->language->get('email_subject_callback'), ''), ENT_QUOTES, 'UTF-8'));
                        $html = '<table style="width: 100%; background-color: #f1f1f1;">
                                    <tbody>
                                        <tr>
                                            <td colspan="2"><a href="'.$this->config->get('config_url').'"><img src="'.$this->config->get('config_url') . 'image/' . $this->config->get('config_logo').'" alt="'.$this->config->get('config_name').'" /></a></td>
                                        </tr>
                                        <tr>
                                            <td style="width: 30%; background-color: #f3f3f3;">'.$data['text_name'].'</td><td>'.$this->request->post['name'].'</td>
                                        </tr>
                                        <tr>
                                            <td style="width: 30%; background-color: #f3f3f3;">'.$data['text_phone'].'</td><td>'.$this->request->post['phone'].'</td>
                                        </tr>
                                    </tbody>
                                </table>';
                        $mail->setHtml($html);
                        $mail->send();
                        $json['success'] = $data['success'];
                    endif;
                endif;
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
}