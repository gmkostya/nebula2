<?php
class ControllerModuleFeatured extends Controller {
	public function index($setting) {
		$this->load->language('module/featured');

		$data['heading_title'] = $this->language->get('heading_title');
		$data['title'] = $setting['title'];
		$data['sub_title'] = $setting['sub_title'];

		$data['text_tax'] = $this->language->get('text_tax');

		$data['button_cart'] = $this->language->get('button_cart');
        $data['button_oneclick'] = $this->language->get('button_oneclick');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

        $data['url_catalog'] =$this->url->link('product/category', 'path=14');
		$data['products'] = array();

		if (!$setting['limit']) {
			$setting['limit'] = 4;
		}

		if (!empty($setting['product'])) {
			$products = array_slice($setting['product'], 0, (int)$setting['limit']);

            $data['products'] = $this->load->controller('product/product_partial/each', [
                'results'   => $products,
                'width'     => $setting['width'],
                'height'    => $setting['height'],
                'url'       => $this->url->link('product/product', 'product_id=%s')
            ]);
		}

		if ($data['products']) {
            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/featured.tpl')) {
				return $this->load->view($this->config->get('config_template') . '/template/module/featured.tpl', $data);
			} else {
				return $this->load->view('default/template/module/featured.tpl', $data);
			}
		}
	}
}