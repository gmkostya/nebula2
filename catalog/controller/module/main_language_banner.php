<?php
class ControllerModuleMainLanguageBanner extends Controller {
	public function index() {


		$this->load->model('module/main_language_banner');

        $this->load->model('tool/image');

        $this->document->addStyle('catalog/view/javascript/jquery/slick/slick.css');
        $this->document->addStyle('catalog/view/javascript/jquery/slick/slick-theme.css');
        $this->document->addScript('catalog/view/javascript/jquery/slick/slick.js');

        $data['banners'] = array();

        $results = $this->model_module_main_language_banner->getMainBanner();

        foreach ($results as $result) {
            if (is_file(DIR_IMAGE . $result['image'])) {

                $data['banners'][] = array(
                    'main_banner_id' => $result['main_banner_id'],
                    'title' => $result['title'],
                    'link'  => $result['link'],
                    'image' => '/image/'.$result['image'],
                    'image_mobile' => '/image/'.$result['image_mobile']
                );
            }
        }


		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/main_language_banner.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/main_language_banner.tpl', $data);
		} else {
			return $this->load->view('default/template/module/main_language_banner.tpl', $data);
		}
	}
}