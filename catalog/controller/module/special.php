<?php
class ControllerModuleSpecial extends Controller {
	public function index($setting) {
		$this->load->language('module/special');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_tax'] = $this->language->get('text_tax');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_oneclick'] = $this->language->get('button_oneclick');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');
		$data['title'] = $setting['name'];
		$data['subtitle'] = $setting['subtitle'];
		wss_log($setting);

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		$data['products'] = array();

		$filter_data = array(
			'sort'  => 'p.product_id',
			'order' => 'ASC',
			'start' => 0,
			'limit' => $setting['limit']
		);

		$results = $this->model_catalog_product->getProductSpecials($filter_data);

		if ($results) {

            $data['products'] = $this->load->controller('product/product_partial', [
                'results'   => $results,
                'width'     => $setting['width'],
                'height'    => $setting['height'],
                'url'       => $this->url->link('product/product', 'product_id=%s')
            ]);

            if(isset($setting['module_id'])){
                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/special-category.tpl')) {
                    return $this->load->view($this->config->get('config_template') . '/template/module/special-category.tpl', $data);
                } else {
                    return $this->load->view('default/template/module/special-category.tpl', $data);
                }
            }else{
                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/special.tpl')) {
                    return $this->load->view($this->config->get('config_template') . '/template/module/special.tpl', $data);
                } else {
                    return $this->load->view('default/template/module/special.tpl', $data);
                }
            }
		}
	}
}