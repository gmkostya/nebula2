<?php
class ControllerModuleVisitedproduct extends Controller {
	public function index($setting) {
		$this->load->language('module/visitedproduct');

		$data['heading_title'] = $setting['name'];
        $data['button_cart'] = $this->language->get('button_cart');
        $data['button_oneclick'] = $this->language->get('button_oneclick');
        $data['button_wishlist'] = $this->language->get('button_wishlist');
        $data['button_compare'] = $this->language->get('button_compare');

		$this->load->model('catalog/category');

		$this->load->model('catalog/product');
		
		$this->load->model('tool/image');
		
		if(isset($this->session->data['products_id'])){
            $this->session->data['products_id'] = array_slice($this->session->data['products_id'], -$setting['limit']);
        }
        $data['products'] = array();
		if (isset($this->session->data['products_id'])) {
            $products = array_slice($this->session->data['products_id'], 0, (int)$setting['limit']);

            $data['products'] = $this->load->controller('product/product_partial/each', [
                'results'   => $products,
                'width'     => $setting['width'],
                'height'    => $setting['height'],
                'url'       => $this->url->link('product/product', 'product_id=%s')
            ]);
        }
        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/visitedproduct.tpl')) {
            return $this->load->view($this->config->get('config_template') . '/template/module/visitedproduct.tpl', $data);
        } else {
            return $this->load->view('default/template/module/visitedproduct.tpl', $data);
        }
	}
}