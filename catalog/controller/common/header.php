<?php
class ControllerCommonHeader extends Controller {
	public function index() {
		// Analytics
		$this->load->model('extension/extension');

		$data['analytics'] = array();

		$analytics = $this->model_extension_extension->getExtensions('analytics');

		foreach ($analytics as $analytic) {
			if ($this->config->get($analytic['code'] . '_status')) {
				$data['analytics'][] = $this->load->controller('analytics/' . $analytic['code']);
			}
		}

		if ($this->request->server['HTTPS']) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}

		if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
			$this->document->addLink($server . 'image/' . $this->config->get('config_icon'), 'icon');
		}
        $this->document->removeScripts('catalog/view/javascript/jquery/ajaxupload.js');
        $this->document->removeScripts('catalog/view/javascript/jquery/datetimepicker/moment.js');
        $this->document->removeScripts('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
        $this->document->removeScripts('catalog/view/javascript/jquery/jquery.maskedinput.min.js');
        $this->document->removeScripts('catalog/view/javascript/easyTooltip.js');


		$data['title'] = $this->document->getTitle();

		$data['base'] = $server;
		$data['description'] = $this->document->getDescription();
		$data['keywords'] = $this->document->getKeywords();
		$data['links'] = $this->document->getLinks();
		$data['styles'] = $this->document->getStyles();
		$data['scripts'] = $this->document->getScripts();
        /**
         * задаємо мета robots
         * можна в необхідному контролері додати $this->document->addMetaRobots
         * також через vqmod добавляється $data['robots'] для кастомних сторінок, можна добавити через адмінку в модулі "Seo кастомных страниц"
         */
        $data['meta'] = $this->document->getMetaRobots();
		$data['lang'] = $this->language->get('code');
		$data['direction'] = $this->language->get('direction');

        $data['alter_lang'] = $this->getAlterLanguageLinks($this->document->getLinks(),$data['lang'],$this->config->get('config_language_default'));
		$data['name'] = $this->config->get('config_name');
		$data['gtm'] = trim($this->config->get('config_gtm'));

		if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
			$data['logo'] = $server . 'image/' . $this->config->get('config_logo');
		} else {
			$data['logo'] = '';
		}

		$this->load->language('common/header');
		$data['og_url'] = ((isset($this->request->server['HTTPS']) && !empty($this->request->server['HTTPS'])) ? HTTPS_SERVER : HTTP_SERVER) . substr($this->request->server['REQUEST_URI'], 1, (strlen($this->request->server['REQUEST_URI'])-1));
		$data['og_image'] = $this->document->getOgImage();

		$data['text_home'] = $this->language->get('text_home');
		$data['is_logged'] = $this->customer->isLogged();

		// Wishlist
		if ($this->customer->isLogged()) {
			$this->load->model('account/wishlist');

			$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
            $data['total_wishlist'] = $this->model_account_wishlist->getTotalWishlist();
		} else {
			$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
            $data['total_wishlist'] = isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0;
		}
        $data['total_compare'] = isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0;

		$data['text_shopping_cart'] = $this->language->get('text_shopping_cart');
		$data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', 'SSL'), $this->customer->getFirstName(), $this->url->link('account/logout', '', 'SSL'));

		$data['text_account'] = $this->language->get('text_account');
		$data['text_register'] = $this->language->get('text_register');
		$data['text_login'] = $this->language->get('text_login');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_transaction'] = $this->language->get('text_transaction');
		$data['text_download'] = $this->language->get('text_download');
		$data['text_logout'] = $this->language->get('text_logout');
		$data['text_checkout'] = $this->language->get('text_checkout');
		$data['text_page'] = $this->language->get('text_page');
		$data['text_category'] = $this->language->get('text_category');
		$data['text_all'] = $this->language->get('text_all');
        $data['text_news'] = $this->language->get('text_news');
        $data['text_review'] = $this->language->get('text_review');
        $data['text_gallery'] = $this->language->get('text_gallery');
        $data['text_contact'] = $this->language->get('text_contact');

        $data['news'] =$this->url->link('news/ncategory', 'ncat=1', '', 'SSL');
		$data['home'] = $this->url->link('common/home');
        $data['review'] = $this->url->link('testimonial/testimonial', '' , 'SSL');
		$data['wishlist'] = $this->url->link('account/wishlist', '', 'SSL');
		$data['compare'] = $this->url->link('product/compare', '', 'SSL');
		$data['logged'] = $this->customer->isLogged();
		$data['account'] = $this->url->link('account/account', '', 'SSL');
        $data['account_edit'] = $this->url->link('account/edit', '', 'SSL');
		$data['register'] = $this->url->link('account/register', '', 'SSL');
		$data['login'] = $this->url->link('account/login', '', 'SSL');
		$data['order'] = $this->url->link('account/order', '', 'SSL');
		$data['transaction'] = $this->url->link('account/transaction', '', 'SSL');
		$data['download'] = $this->url->link('account/download', '', 'SSL');
		$data['logout'] = $this->url->link('account/logout', '', 'SSL');
		$data['shopping_cart'] = $this->url->link('checkout/cart');
		$data['checkout'] = $this->url->link('checkout/checkout', '', 'SSL');
		$data['contact'] = $this->url->link('information/contact');
		$data['gallery'] = $this->url->link('information/gallery');
        $data['config_country'] = $this->config->get('config_country');
        $data['config_city'] = $this->config->get('config_city');
        $data['config_address'] = $this->config->get('config_address');
        $data['config_postcode'] = $this->config->get('config_postcode');
		$data['telephone'] = $this->config->get('config_telephone');
        $data['email'] = $this->config->get('config_email');
        $data['social'] = array();
        if($this->config->get('config_fb')){
            $data['social']['face'] = $this->config->get('config_fb');
        }
        if($this->config->get('config_vk')){
            $data['social']['vk'] = $this->config->get('config_vk');
        }
        if($this->config->get('config_twitter')){
            $data['social']['twit'] = $this->config->get('config_twitter');
        }
        if($this->config->get('config_instagram')){
            $data['social']['insta'] = $this->config->get('config_instagram');
        }
        $data['social_count'] = count($data['social'])-1;

		$status = true;

		if (isset($this->request->server['HTTP_USER_AGENT'])) {
			$robots = explode("\n", str_replace(array("\r\n", "\r"), "\n", trim($this->config->get('config_robots'))));

			foreach ($robots as $robot) {
				if ($robot && strpos($this->request->server['HTTP_USER_AGENT'], trim($robot)) !== false) {
					$status = false;

					break;
				}
			}
		}

		// Menu
        $this->load->model('catalog/information');

        $data['informations'] = array();

        foreach ($this->model_catalog_information->getInformations() as $result) {
            if ($result['bottom']) {
                $data['informations'][$result['information_id']] = array(
                    'title' => $result['title'],
                    'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
                );
            }
        }

		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$data['categories'] = array();

		$categories = $this->model_catalog_category->getCategories(0);

		foreach ($categories as $category) {
			if ($category['top']) {
				// Level 2
                $children_data = array();

                $children = $this->model_catalog_category->getCategories($category['category_id']);

                foreach ($children as $key => $child) {
                    $children_data[] = array(
                        'name'  => $child['name'],
                        'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
                    );
                }

				// Level 1
				$data['categories'][] = array(
					'name'     => $category['name'],
					'children' => $children_data,
					//'column'   => $category['column'] ? $category['column'] : 1,
					'href'     => $this->url->link('product/category', 'path=' . $category['category_id'])
				);
			}
		}

		$data['language'] = $this->load->controller('common/language');
		$data['currency'] = $this->load->controller('common/currency');
		$data['search'] = $this->load->controller('common/search');
		$data['cart'] = $this->load->controller('common/cart');

		// For page specific css
		if (isset($this->request->get['route'])) {
			if (isset($this->request->get['product_id'])) {
				$class = '-' . $this->request->get['product_id'].' product-page';
			} elseif (isset($this->request->get['path'])) {
				$class = '-' . $this->request->get['path'].' category-page';
			} elseif (isset($this->request->get['manufacturer_id'])) {
				$class = '-' . $this->request->get['manufacturer_id'].' manufacturer-page';
			} else {
				$class = '';
			}

			$data['class'] = str_replace('/', '-', $this->request->get['route']) . $class;
		} else {
			$data['class'] = 'common-home';
		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/header.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/common/header.tpl', $data);
		} else {
			return $this->load->view('default/template/common/header.tpl', $data);
		}
	}

	public function home(){
        // Analytics
        $this->load->model('extension/extension');

        $data['analytics'] = array();

        $analytics = $this->model_extension_extension->getExtensions('analytics');

        foreach ($analytics as $analytic) {
            if ($this->config->get($analytic['code'] . '_status')) {
                $data['analytics'][] = $this->load->controller('analytics/' . $analytic['code']);
            }
        }

        if ($this->request->server['HTTPS']) {
            $server = $this->config->get('config_ssl');
        } else {
            $server = $this->config->get('config_url');
        }

        /*if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
            $this->document->addLink($server . 'image/' . $this->config->get('config_icon'), 'icon');
        }*/

        $data['title'] = $this->document->getTitle();

        $data['base'] = $server;
        $data['description'] = $this->document->getDescription();
        $data['keywords'] = $this->document->getKeywords();
        $data['links'] = $this->document->getLinks();
        $data['styles'] = $this->document->getStyles();
        $data['scripts'] = $this->document->getScripts();
        /**
         * задаємо мета robots
         * можна в необхідному контролері додати $this->document->addMetaRobots
         * також через vqmod добавляється $data['robots'] для кастомних сторінок, можна добавити через адмінку в модулі "Seo кастомных страниц"
         */
        $data['meta'] = $this->document->getMetaRobots();
        $data['lang'] = $this->language->get('code');
        $data['direction'] = $this->language->get('direction');

        $data['alter_lang'] = $this->getAlterLanguageLinks($this->document->getLinks(),$data['lang'],$this->config->get('config_language_default'));
        $data['name'] = $this->config->get('config_name');
        $data['gtm'] = trim($this->config->get('config_gtm'));

        if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
            $data['logo'] = $server . 'image/' . $this->config->get('config_logo');
        } else {
            $data['logo'] = '';
        }

        $this->load->language('common/header');
        $data['og_url'] = ((isset($this->request->server['HTTPS']) && !empty($this->request->server['HTTPS'])) ? HTTPS_SERVER : HTTP_SERVER) . substr($this->request->server['REQUEST_URI'], 1, (strlen($this->request->server['REQUEST_URI'])-1));
        $data['og_image'] = $this->document->getOgImage();

        $data['text_home'] = $this->language->get('text_home');
        $data['is_logged'] = $this->customer->isLogged();

        // Wishlist
        if ($this->customer->isLogged()) {
            $this->load->model('account/wishlist');

            $data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
            $data['total_wishlist'] = $this->model_account_wishlist->getTotalWishlist();
        } else {
            $data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
            $data['total_wishlist'] = isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0;
        }
        $data['total_compare'] = isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0;

        $data['text_shopping_cart'] = $this->language->get('text_shopping_cart');
        $data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', 'SSL'), $this->customer->getFirstName(), $this->url->link('account/logout', '', 'SSL'));

        $data['text_account'] = $this->language->get('text_account');
        $data['text_register'] = $this->language->get('text_register');
        $data['text_login'] = $this->language->get('text_login');
        $data['text_order'] = $this->language->get('text_order');
        $data['text_transaction'] = $this->language->get('text_transaction');
        $data['text_download'] = $this->language->get('text_download');
        $data['text_logout'] = $this->language->get('text_logout');
        $data['text_checkout'] = $this->language->get('text_checkout');
        $data['text_page'] = $this->language->get('text_page');
        $data['text_category'] = $this->language->get('text_category');
        $data['text_all'] = $this->language->get('text_all');
        $data['text_news'] = $this->language->get('text_news');
        $data['text_review'] = $this->language->get('text_review');
        $data['text_gallery'] = $this->language->get('text_gallery');
        $data['text_contact'] = $this->language->get('text_contact');

        $data['news'] =$this->url->link('news/ncategory', 'ncat=1', '', 'SSL');
        $data['home'] = $this->url->link('common/home');
        $data['review'] = $this->url->link('testimonial/testimonial', '' , 'SSL');
        $data['wishlist'] = $this->url->link('account/wishlist', '', 'SSL');
        $data['compare'] = $this->url->link('product/compare', '', 'SSL');
        $data['logged'] = $this->customer->isLogged();
        $data['account'] = $this->url->link('account/account', '', 'SSL');
        $data['account_edit'] = $this->url->link('account/edit', '', 'SSL');
        $data['register'] = $this->url->link('account/register', '', 'SSL');
        $data['login'] = $this->url->link('account/login', '', 'SSL');
        $data['order'] = $this->url->link('account/order', '', 'SSL');
        $data['transaction'] = $this->url->link('account/transaction', '', 'SSL');
        $data['download'] = $this->url->link('account/download', '', 'SSL');
        $data['logout'] = $this->url->link('account/logout', '', 'SSL');
        $data['shopping_cart'] = $this->url->link('checkout/cart');
        $data['checkout'] = $this->url->link('checkout/checkout', '', 'SSL');
        $data['contact'] = $this->url->link('information/contact');
        $data['gallery'] = $this->url->link('information/gallery');
        $data['config_country'] = $this->config->get('config_country');
        $data['config_city'] = $this->config->get('config_city');
        $data['config_address'] = $this->config->get('config_address');
        $data['config_postcode'] = $this->config->get('config_postcode');
        $data['telephone'] = $this->config->get('config_telephone');
        $data['email'] = $this->config->get('config_email');
        $data['social'] = array();
        if($this->config->get('config_fb')){
            $data['social']['face'] = $this->config->get('config_fb');
        }
        if($this->config->get('config_vk')){
            $data['social']['vk'] = $this->config->get('config_vk');
        }
        if($this->config->get('config_twitter')){
            $data['social']['twit'] = $this->config->get('config_twitter');
        }
        if($this->config->get('config_instagram')){
            $data['social']['insta'] = $this->config->get('config_instagram');
        }
        $data['social_count'] = count($data['social'])-1;

        $status = true;

        if (isset($this->request->server['HTTP_USER_AGENT'])) {
            $robots = explode("\n", str_replace(array("\r\n", "\r"), "\n", trim($this->config->get('config_robots'))));

            foreach ($robots as $robot) {
                if ($robot && strpos($this->request->server['HTTP_USER_AGENT'], trim($robot)) !== false) {
                    $status = false;

                    break;
                }
            }
        }

        // Menu
        $this->load->model('catalog/information');

        $data['informations'] = array();

        foreach ($this->model_catalog_information->getInformations() as $result) {
            if ($result['bottom']) {
                $data['informations'][$result['information_id']] = array(
                    'title' => $result['title'],
                    'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
                );
            }
        }

        $this->load->model('catalog/category');

        $this->load->model('catalog/product');

        $data['categories'] = array();

        $categories = $this->model_catalog_category->getCategories(0);

        foreach ($categories as $category) {
            if ($category['top']) {
                // Level 2
                $children_data = array();

                $children = $this->model_catalog_category->getCategories($category['category_id']);

                foreach ($children as $key => $child) {
                    $children_data[] = array(
                        'name'  => $child['name'],
                        'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
                    );
                }

                // Level 1
                $data['categories'][] = array(
                    'name'     => $category['name'],
                    'children' => $children_data,
                    //'column'   => $category['column'] ? $category['column'] : 1,
                    'href'     => $this->url->link('product/category', 'path=' . $category['category_id'])
                );
            }
        }

        $data['text_register'] = $this->language->get('text_register');
        $data['text_login'] = $this->language->get('text_login');

        $data['language'] = $this->load->controller('common/language');
        $data['currency'] = $this->load->controller('common/currency');
        $data['search'] = $this->load->controller('common/search');
        $data['cart'] = $this->load->controller('common/cart');

        // For page specific css
        if (isset($this->request->get['route'])) {
            if (isset($this->request->get['product_id'])) {
                $class = '-' . $this->request->get['product_id'].' product-page';
            } elseif (isset($this->request->get['path'])) {
                $class = '-' . $this->request->get['path'].' category-page';
            } elseif (isset($this->request->get['manufacturer_id'])) {
                $class = '-' . $this->request->get['manufacturer_id'].' manufacturer-page';
            } else {
                $class = '';
            }

            $data['class'] = str_replace('/', '-', $this->request->get['route']) . $class;
        } else {
            $data['class'] = 'common-home';
        }

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/header-home.tpl')) {
            return $this->load->view($this->config->get('config_template') . '/template/common/header-home.tpl', $data);
        } else {
            return $this->load->view('default/template/common/header-home.tpl', $data);
        }
    }

    protected function getAlterLanguageLinks($links,$lang_cur,$lang_def) {
        $result = array();
        if ($this->config->get('config_seo_url')) {
            foreach($links as $link) {
                if($link['rel']=='canonical') {
                    $url=$link['href'];
                    $schema = parse_url($url,PHP_URL_SCHEME);
                    $server = strtolower($schema)=='https' ? HTTPS_SERVER : HTTP_SERVER;
                    $cur_lang = substr($url, strlen($server),2);
                    if($lang_cur == $cur_lang){
                        $query = substr($url, strlen($server)+2);
                    }else{
                        $cur_lang = $lang_def;
                        $query = '/'.str_replace($server,'',$url);
                    }
                    $this->load->model('localisation/language');
                    $languages = $this->model_localisation_language->getLanguages();
                    $active_langs = array();
                    foreach($languages as $lang) {
                        if($lang['status']) {
                            $active_langs[]=$lang['code'];
                        }
                    }
                    if(in_array($cur_lang, $active_langs)) {
                        foreach($active_langs as $lang) {
                            if($lang_def != $lang){
                                $result[$lang] = $server.$lang.($query ? $query : '');
                            }else{
                                $pos = strpos($query, '/');
                                if($pos == 0){
                                    $result[$lang] = $server.($query ?  substr_replace($query, '', $pos, 1) : '');
                                    //$result[$lang] = $server.($query ? str_replace('/','',$query) : '');
                                }

                            }
                        }
                    }
                }
            }
        }
        return $result;
    }
}
