<?php
class ControllerCommonFooter extends Controller {
	public function index() {
		$this->load->language('common/footer');

        $this->document->removeScripts('catalog/view/javascript/jquery/ajaxupload.js');
        $this->document->removeScripts('catalog/view/javascript/jquery/datetimepicker/moment.js');
        $this->document->removeScripts('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
        $this->document->removeScripts('catalog/view/javascript/jquery/jquery.maskedinput.min.js');
        $this->document->removeScripts('catalog/view/javascript/easyTooltip.js');
        $data['quicksignup'] = $this->load->controller('common/quicksignup');
        $v='0.4';
        $this->document->addStyle('/catalog/view/theme/default/stylesheet/wss.css');
        $this->document->addScript('/catalog/view/javascript/oc/custom_forms.js?v='.$v,'footer');
        $this->document->addScript('/catalog/view/javascript/common.js?v='.$v,'footer');
		$data['scripts'] = $this->document->getScripts();
		$data['scripts2'] = $this->document->getScripts('footer');
        $data['links'] = $this->document->getLinks();
        $data['styles'] = $this->document->getStyles('footer');

		$data['text_information'] = $this->language->get('text_information');
		$data['text_service'] = $this->language->get('text_service');
		$data['text_extra'] = $this->language->get('text_extra');
		$data['text_contact'] = $this->language->get('text_contact');
		$data['text_return'] = $this->language->get('text_return');
		$data['text_sitemap'] = $this->language->get('text_sitemap');
		$data['text_manufacturer'] = $this->language->get('text_manufacturer');
		$data['text_voucher'] = $this->language->get('text_voucher');
		$data['text_affiliate'] = $this->language->get('text_affiliate');
		$data['text_special'] = $this->language->get('text_special');
		$data['text_account'] = $this->language->get('text_account');
		$data['text_order'] = $this->language->get('text_order');
        $data['text_edit'] = $this->language->get('text_edit');
		$data['text_wishlist'] = $this->language->get('text_wishlist');
		$data['text_newsletter'] = $this->language->get('text_newsletter');
        $data['text_news'] = $this->language->get('text_news');
        $data['text_review'] = $this->language->get('text_review');
        $data['text_gallery'] = $this->language->get('text_gallery');
        $data['text_contact'] = $this->language->get('text_contact');
        $data['text_shop'] = $this->language->get('text_shop');
        $data['text_exit'] = $this->language->get('text_exit');

		$data['text_call'] = $this->language->get('text_call');
		$data['text_name'] = $this->language->get('text_name');
		$data['text_phone'] = $this->language->get('text_phone');
		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_phone'] = $this->language->get('entry_phone');
		$data['button_continue'] = $this->language->get('button_continue');
		$data['text_loading'] = $this->language->get('text_loading');
        $data['form_callback'] = $this->load->controller('oc/custom_forms',array('form' => 'callback'));

		$this->load->model('catalog/information');

		$data['informations'] = array();

		foreach ($this->model_catalog_information->getInformations() as $result) {
			if ($result['bottom']) {
				$data['informations'][$result['information_id']] = array(
					'title' => $result['title'],
					'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
				);
			}
		}

        $this->load->model('catalog/category');

        $data['categories'] = array();

        $categories = $this->model_catalog_category->getCategories(0);

        foreach ($categories as $category) {
            if ($category['top']) {
                // Level 2
                $children_data = array();

                $children = $this->model_catalog_category->getCategories($category['category_id']);

                foreach ($children as $key => $child) {
                    $children_data[] = array(
                        'name'  => $child['name'],
                        'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
                    );
                }

                // Level 1
                $data['categories'][] = array(
                    'name'     => $category['name'],
                    'children' => $children_data,
                    //'column'   => $category['column'] ? $category['column'] : 1,
                    'href'     => $this->url->link('product/category', 'path=' . $category['category_id'])
                );
            }
        }


        $data['url_shop'] =$this->url->link('product/category', 'path=14');
		$data['contact'] = $this->url->link('information/contact');
        $data['gallery'] = $this->url->link('information/gallery');
        $data['review'] = $this->url->link('testimonial/testimonial', '' , 'SSL');
        $data['account_edit'] = $this->url->link('account/edit', '', 'SSL');
		$data['return'] = $this->url->link('account/return/add', '', 'SSL');
		$data['sitemap'] = $this->url->link('information/sitemap');
		$data['manufacturer'] = $this->url->link('product/manufacturer');
		$data['voucher'] = $this->url->link('account/voucher', '', 'SSL');
		$data['affiliate'] = $this->url->link('affiliate/account', '', 'SSL');
		$data['special'] = $this->url->link('product/special');
		$data['account'] = $this->url->link('account/account', '', 'SSL');
		$data['order'] = $this->url->link('account/order', '', 'SSL');
        $data['logout'] = $this->url->link('account/logout', '', 'SSL');
		$data['wishlist'] = $this->url->link('account/wishlist', '', 'SSL');
		$data['newsletter'] = $this->url->link('account/newsletter', '', 'SSL');
		$data['news'] =$this->url->link('news/ncategory', 'ncat=1', '', 'SSL');
        $data['is_logged'] = $this->customer->isLogged();

        $data['config_city'] = $this->config->get('config_city');
        $data['config_address'] = $this->config->get('config_address');
        $data['telephone'] = $this->config->get('config_telephone');
        $data['email'] = $this->config->get('config_email');
        $data['social'] = array();
        if($this->config->get('config_fb')){
            $data['social']['face'] = $this->config->get('config_fb');
        }
        if($this->config->get('config_vk')){
            $data['social']['vk'] = $this->config->get('config_vk');
        }
        if($this->config->get('config_twitter')){
            $data['social']['twit'] = $this->config->get('config_twitter');
        }
        if($this->config->get('config_instagram')){
            $data['social']['insta'] = $this->config->get('config_instagram');
        }
        $data['social_count'] = count($data['social']);

		$data['powered'] = sprintf($this->language->get('text_powered'), $this->config->get('config_name'), date('Y', time()));


        $data['user_name'] = $this->customer->getFirstName();
        $data['user_phone'] = $this->customer->getTelephone();

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/footer.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/common/footer.tpl', $data);
		} else {
			return $this->load->view('default/template/common/footer.tpl', $data);
		}
	}

    public function home() {
        $this->load->language('common/footer');

        $data['quicksignup'] = $this->load->controller('common/quicksignup');
        $this->document->addStyle('/catalog/view/theme/default/stylesheet/wss.css');
        $this->document->addScript('/catalog/view/javascript/oc/custom_forms.js','footer');
        $data['scripts'] = $this->document->getScripts('footer');
        $data['scripts2'] = $this->document->getScripts('footer2');
        $data['links'] = $this->document->getLinks();
        $data['styles'] = $this->document->getStyles('footer');

        $data['text_information'] = $this->language->get('text_information');
        $data['text_service'] = $this->language->get('text_service');
        $data['text_extra'] = $this->language->get('text_extra');
        $data['text_contact'] = $this->language->get('text_contact');
        $data['text_return'] = $this->language->get('text_return');
        $data['text_sitemap'] = $this->language->get('text_sitemap');
        $data['text_manufacturer'] = $this->language->get('text_manufacturer');
        $data['text_voucher'] = $this->language->get('text_voucher');
        $data['text_affiliate'] = $this->language->get('text_affiliate');
        $data['text_special'] = $this->language->get('text_special');
        $data['text_account'] = $this->language->get('text_account');
        $data['text_edit'] = $this->language->get('text_edit');
        $data['text_order'] = $this->language->get('text_order');
        $data['text_wishlist'] = $this->language->get('text_wishlist');
        $data['text_newsletter'] = $this->language->get('text_newsletter');
        $data['text_news'] = $this->language->get('text_news');
        $data['text_review'] = $this->language->get('text_review');
        $data['text_gallery'] = $this->language->get('text_gallery');
        $data['text_contact'] = $this->language->get('text_contact');
        $data['text_shop'] = $this->language->get('text_shop');
        $data['text_exit'] = $this->language->get('text_exit');

        $data['text_call'] = $this->language->get('text_call');
        $data['text_name'] = $this->language->get('text_name');
        $data['text_phone'] = $this->language->get('text_phone');
        $data['entry_name'] = $this->language->get('entry_name');
        $data['entry_phone'] = $this->language->get('entry_phone');
        $data['button_continue'] = $this->language->get('button_continue');
        $data['text_loading'] = $this->language->get('text_loading');

        $data['form_callback'] = $this->load->controller('oc/custom_forms',array('form' => 'callback'));
        $this->load->model('catalog/information');

        $data['informations'] = array();

        foreach ($this->model_catalog_information->getInformations() as $result) {
            if ($result['bottom']) {
                $data['informations'][$result['information_id']] = array(
                    'title' => $result['title'],
                    'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
                );
            }
        }

        $this->load->model('catalog/category');

        $data['categories'] = array();

        $categories = $this->model_catalog_category->getCategories(0);

        foreach ($categories as $category) {
            if ($category['top']) {
                // Level 2
                $children_data = array();

                $children = $this->model_catalog_category->getCategories($category['category_id']);

                foreach ($children as $key => $child) {
                    $children_data[] = array(
                        'name'  => $child['name'],
                        'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
                    );
                }

                // Level 1
                $data['categories'][] = array(
                    'name'     => $category['name'],
                    'children' => $children_data,
                    //'column'   => $category['column'] ? $category['column'] : 1,
                    'href'     => $this->url->link('product/category', 'path=' . $category['category_id'])
                );
            }
        }


        $data['url_shop'] =$this->url->link('product/category', 'path=14');
        $data['contact'] = $this->url->link('information/contact');
        $data['gallery'] = $this->url->link('information/gallery');
        $data['review'] = $this->url->link('testimonial/testimonial', '' , 'SSL');
        $data['account_edit'] = $this->url->link('account/edit', '', 'SSL');
        $data['return'] = $this->url->link('account/return/add', '', 'SSL');
        $data['sitemap'] = $this->url->link('information/sitemap');
        $data['manufacturer'] = $this->url->link('product/manufacturer');
        $data['voucher'] = $this->url->link('account/voucher', '', 'SSL');
        $data['affiliate'] = $this->url->link('affiliate/account', '', 'SSL');
        $data['special'] = $this->url->link('product/special');
        $data['account'] = $this->url->link('account/account', '', 'SSL');
        $data['order'] = $this->url->link('account/order', '', 'SSL');
        $data['logout'] = $this->url->link('account/logout', '', 'SSL');
        $data['wishlist'] = $this->url->link('account/wishlist', '', 'SSL');
        $data['newsletter'] = $this->url->link('account/newsletter', '', 'SSL');
        $data['news'] =$this->url->link('news/ncategory', 'ncat=1', '', 'SSL');
        $data['is_logged'] = $this->customer->isLogged();

        $data['config_city'] = $this->config->get('config_city');
        $data['config_address'] = $this->config->get('config_address');
        $data['telephone'] = $this->config->get('config_telephone');
        $data['email'] = $this->config->get('config_email');
        $data['social'] = array();
        if($this->config->get('config_fb')){
            $data['social']['face'] = $this->config->get('config_fb');
        }
        if($this->config->get('config_vk')){
            $data['social']['vk'] = $this->config->get('config_vk');
        }
        if($this->config->get('config_twitter')){
            $data['social']['twit'] = $this->config->get('config_twitter');
        }
        if($this->config->get('config_instagram')){
            $data['social']['insta'] = $this->config->get('config_instagram');
        }
        $data['social_count'] = count($data['social']);

        $data['powered'] = sprintf($this->language->get('text_powered'), $this->config->get('config_name'), date('Y', time()));


        $data['user_name'] = $this->customer->getFirstName();
        $data['user_phone'] = $this->customer->getTelephone();

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/footer-home.tpl')) {
            return $this->load->view($this->config->get('config_template') . '/template/common/footer-home.tpl', $data);
        } else {
            return $this->load->view('default/template/common/footer-home.tpl', $data);
        }
    }
}
