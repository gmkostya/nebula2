<?php
class ControllerProductProductPartial extends Controller {
    public function index( $setting ){

        $this->load->model('account/wishlist');
        $products = array();
        foreach ($setting['results'] as $result) {

            if ($result['image']) {
                $image = $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']);
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
            }

            if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
            } else {
                $price = false;
            }

            if ((float)$result['special']) {
                $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
            } else {
                $special = false;
            }

            if ($this->config->get('config_tax')) {
                $tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
            } else {
                $tax = false;
            }

            if ($this->config->get('config_review_status')) {
                $rating = (int)$result['rating'];
            } else {
                $rating = false;
            }

            if ($this->customer->isLogged()) {
                $wishlists = $this->model_account_wishlist->getWishlistID();
            } else {
                $wishlists =  isset($this->session->data['wishlist']) ? $this->session->data['wishlist'] : array();
            }
            //if (isset($this->session->data['wishlist']) && in_array($result['product_id'], $this->session->data['wishlist'])) {
            if (isset($wishlists) && in_array($result['product_id'], $wishlists)) {
                $wishlist_status = true;
            } else {
                $wishlist_status = false;
            }

            if (isset($this->session->data['compare']) && in_array($result['product_id'], $this->session->data['compare'])) {
                $compare_status = true;
            } else {
                $compare_status = false;
            }

            $products[] = array(
                'product_id'  => $result['product_id'],
                'thumb'       => $image,
                'name'        => $result['name'],
                'model'        => $result['model'],
                'for_whom'    => $result['for_whom'],
                'short_description' => $result['short_description'],
                'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
                'price'       => $price,
                'special'     => $special,
                'date_end'    => date('Y-m-d', strtotime($result['date_end'])),
                'date_start'  => date('Y-m-d', strtotime($result['date_start'])),
                'tax'         => $tax,
                'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
                'rating'      => $result['rating'],
                'wishlist_status' => $wishlist_status,
                'compare_status' => $compare_status,
                'availability' => $result['quantity'] ? true : false,
               // 'href'        => sprintf($setting['url'], $result['product_id'])
                'href'        => $this->url->link(sprintf($setting['url'], $result['product_id']))
            );
        }

        return $products;
    }
    public function each( $setting ){

        $this->load->model('account/wishlist');
        $products = array();
        foreach ($setting['results'] as $product_id) {
            $product_info = $this->model_catalog_product->getProduct($product_id);

            if ($product_info) {
                if ($product_info['image']) {
                    $image = $this->model_tool_image->resize($product_info['image'], $setting['width'], $setting['height']);
                } else {
                    $image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
                }

                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                    $price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $price = false;
                }

                if ((float)$product_info['special']) {
                    $special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $special = false;
                }

                if ($this->config->get('config_tax')) {
                    $tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price']);
                } else {
                    $tax = false;
                }

                if ($this->config->get('config_review_status')) {
                    $rating = $product_info['rating'];
                } else {
                    $rating = false;
                }

                if ($this->customer->isLogged()) {
                    $wishlists = $this->model_account_wishlist->getWishlistID();
                } else {
                    $wishlists =  isset($this->session->data['wishlist']) ? $this->session->data['wishlist'] : array();
                }
                //if (isset($this->session->data['wishlist']) && in_array($result['product_id'], $this->session->data['wishlist'])) {
                if (isset($wishlists) && in_array($product_info['product_id'], $wishlists)) {
                    $wishlist_status = true;
                } else {
                    $wishlist_status = false;
                }

                if (isset($this->session->data['compare']) && in_array($product_info['product_id'], $this->session->data['compare'])) {
                    $compare_status = true;
                } else {
                    $compare_status = false;
                }

                $products[] = array(
                    'product_id'  => $product_info['product_id'],
                    'thumb'       => $image,
                    'name'        => $product_info['name'],
                    'model'        => $product_info['model'],
                    'for_whom'    => $product_info['for_whom'],
                    'short_description' => $product_info['short_description'],
                    'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
                    'price'       => $price,
                    'special'     => $special,
                    'date_end'    => date('Y-m-d', strtotime($product_info['date_end'])),
                    'date_start'  => date('Y-m-d', strtotime($product_info['date_start'])),
                    'tax'         => $tax,
                    'minimum'     => $product_info['minimum'] > 0 ? $product_info['minimum'] : 1,
                    'rating'      => $rating,
                    'wishlist_status' => $wishlist_status,
                    'compare_status' => $compare_status,
                    'availability' => $product_info['quantity'] ? true : false,
                    'href'        => $this->url->link(sprintf($setting['url'], $product_info['product_id']))
                );
            }
        }

        return $products;
    }
}
