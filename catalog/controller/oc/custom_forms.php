<?php

class ControllerOcCustomForms extends Controller {

    private $_helper  = 'oc';
    private $_type	  = 'custom_forms';
    private $_module  = 'oc_custom_forms';
    private $error = array();

    public function index($setting) {

        $wsh = new WSH($this->_helper,$this->_type,$this->_helper);

//        $data['element'] = wsh_catalog('forms',$setting['form'],$this->config->get('config_language_id'));
        $data['element'] = $wsh->wsh_get_group_language($setting['form'],$this->config->get('config_language_id'));
        /*echo '<pre>';
        print_r($data['element']);
        echo '</pre>';*/
        if($setting['form'] == 'contacts'){
            $data['captcha'] = $this->load->controller('captcha/' . $this->config->get('config_captcha'), $this->error);
        }
        $data['action'] = $this->url->link($this->_helper.'/'.$this->_type.'/action_' . $setting['form'], '', 'SSL');
        $data['faq'] =  $this->url->link('module/faq', '', 'SSL');
        $this->load->language('common/header');
        $data['text_notifications'] = sprintf($this->language->get('text_notifications'),$this->url->link('index.php?route=information/information', 'information_id=5', 'SSL'));
        if (method_exists($this, 'a'.$setting['form'])) {
            $data['values'] = $this->{'a'.$setting['form']}($data);
        }
        /*$this->load->model('tool/image');
        $data['element']['image']['thumbnail'] = $this->model_tool_image->resize($data['element']['image']['value'], 152, 152, 'onesize');*/

        $view = '';
        if(isset($setting['view'])){
            $view = '_' . $setting['view'];
        }
        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/'.$this->_helper.'/'.$this->_type.'/'.$setting['form'].$view.'.tpl')) {
            return $this->load->view($this->config->get('config_template') . '/template/'.$this->_helper.'/'.$this->_type.'/'.$setting['form'].$view.'.tpl', $data);
        } else {
            return $this->load->view('default/template/wsh/forms/base.tpl', $data);
        }
    }
    
    public function sendData($data, $from, $sender, $fields, $form_name = '') {
        /*begin templates email*/
        $vars = array();

        $text = '';
        /*echo '<pre>';
        var_dump($fields);
        exit();*/
        /*if(isset($data['values'])){
            $labels = wsh_catalog('forms','calculate',$this->config->get('config_language_id'));
            $values = json_decode($data['values'],true);
            foreach ($labels as $key => $item) {
                if ((isset($item['mail_send']) && $item['mail_send']) && isset($values[$key])) {
                    $text .= $item['label'] . ": " . $values[$key] . "\n\n";
                    $vars['fields'][$key] = array(
                        'label' => $item['label'],
                        'value' => $data[$key]
                    );
                }
            }
        }*/
        $text .= html_entity_decode(sprintf('<a href="%s"><img  src="%s" alt="%s" width="100"></a><br/><br/>',$this->config->get('config_url'), $this->config->get('config_url') . 'image/' . $this->config->get('config_logo'), $this->config->get('config_name')), ENT_QUOTES, 'UTF-8');
        foreach ($fields as $key => $item) {
            if ((isset($item['mail_send']) && $item['mail_send']) && isset($data[$key])) {
                if(!isset($data['values'])){
                    $text .= $item['label'] . ": " . $data[$key] . "<br/><br/>";
                    $vars['fields'][$key] = array(
                        'label' => $item['label'],
                        'value' => $data[$key]
                    );
                }
            }
        }
        $subject = 'Сообщения с формы - ' .$form_name;

        $vars['form_name'] = $form_name;

        //$template = $this->ocstore->getTemplateEmail('forms', $vars);
        //$text = $template['text'];
        //$html = $template['html'];
        //$subject = $template['subject'];
        /*end templates email*/
        
        $mail = new Mail();
        $mail->protocol = $this->config->get('config_mail_protocol');
        $mail->parameter = $this->config->get('config_mail_parameter');
        $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
        $mail->smtp_username = $this->config->get('config_mail_smtp_username');
        $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
        $mail->smtp_port = $this->config->get('config_mail_smtp_port');
        $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

        $mail->setTo($this->config->get('config_email'));
        $mail->setFrom($this->config->get('config_email'));
        $mail->setSender($sender);
        $mail->setSubject($subject);
//        $mail->setText(strip_tags(html_entity_decode($text, ENT_QUOTES, 'UTF-8')));
        $mail->setHtml($text);
        $mail->send();

        //$mail->setFrom($from);
        //$mail->setSender($sender);
        //$mail->setSubject($subject);
        //$mail->setText(strip_tags(html_entity_decode($this->request->post['enquiry'], ENT_QUOTES, 'UTF-8')));

        //$mail->setHtml(html_entity_decode($html, ENT_QUOTES, 'UTF-8'));
        //$mail->setText(strip_tags(html_entity_decode($text, ENT_QUOTES, 'UTF-8')));

        //$mail->send();
        
        // Send to additional alert emails
        $emails = explode(',', $this->config->get('config_alert_emails'));

        foreach ($emails as $email) {
            if ($email && preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $email)) {
                $mail->setTo($email);
                $mail->send();
            }
        }
        
    }

    public function action_callback(){
        $json = array('status' => 'error');
        $wsh = new WSH($this->_helper,$this->_type,$this->_helper);
        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            $field = $wsh->wsh_get_group_language('callback',$this->config->get('config_language_id'));


            $errors = $this->validate_callback($field);
            if ($errors === true) {

                $json['status'] = 'success';
                $json['title'] = $field['form_text']['title_successful'];
                $json['subtitle'] = $field['form_text']['subtitle_successful'];
                $json['message'] = $field['form_text']['text_successful'];


                $data = $this->request->post;
                $from = $this->config->get('config_email');
                $sender = $this->config->get('config_name');
                $this->sendData($data, $from, $sender, $field, $field['form_text']['title']);
            } else {
                $json['errors'] = $errors;
                $json['title'] = $field['form_text']['title_error'];
            }
        }
        $this->response->setOutput(json_encode($json));
    }

    public function acallback($data){

        return $data;
    }

    protected function validate_callback($data){

        if ((utf8_strlen($this->request->post['name']) > 0)) {
            $this->error['name'] = 'error';
        }

        if (isset($this->request->post['confirm']) && (utf8_strlen($this->request->post['confirm']) == '1')) {
            $this->error['confirm'] = 'error';
        }

        if($data['first_name']['required']){
            if ((utf8_strlen($this->request->post['first_name']) < 3) || (utf8_strlen($this->request->post['first_name']) > 32)) {
                $this->error['first_name'] = $data['first_name']['error'];
            }
        }

        if($data['phone']['required']){
            if (utf8_strlen($this->request->post['phone']) != 18) {
                $this->error['phone'] = $data['phone']['error'];
            }
        }

        if($data['message']['required']){
            if ((utf8_strlen($this->request->post['message']) < 3)) {
                $this->error['message'] = $data['message']['error'];
            }
        }

        if (!$this->error) {
            return true;
        } else {
            return $this->error;
        }
    }

    public function action_contacts(){
        $json = array('status' => 'error');
        $wsh = new WSH($this->_helper,$this->_type,$this->_helper);
        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            $field = $wsh->wsh_get_group_language('contacts',$this->config->get('config_language_id'));


            $errors = $this->validate_contacts($field);
            if ($errors === true) {

                $json['status'] = 'success';
                $json['title'] = $field['form_text']['title_successful'];
                $json['subtitle'] = $field['form_text']['subtitle_successful'];
                $json['message'] = $field['form_text']['text_successful'];


                $data = $this->request->post;
                $from = $this->config->get('config_email');
                $sender = $this->config->get('config_name');
                $this->sendData($data, $from, $sender, $field, $field['form_text']['title']);
            } else {
                $json['errors'] = $errors;
                $json['title'] = $field['form_text']['title_error'];
            }
        }
        $this->response->setOutput(json_encode($json));
    }

    public function acontacts($data){

        return $data;
    }

    protected function validate_contacts($data){

        if ((utf8_strlen($this->request->post['name']) > 0)) {
            $this->error['name'] = 'error';
        }

        if (isset($this->request->post['confirm']) && (utf8_strlen($this->request->post['confirm']) == '1')) {
            $this->error['confirm'] = 'error';
        }

        if($data['first_name']['required']){
            if ((utf8_strlen($this->request->post['first_name']) < 3) || (utf8_strlen($this->request->post['first_name']) > 32)) {
                $this->error['first_name'] = $data['first_name']['error'];
            }
        }

        if($data['phone']['required']){
            if (utf8_strlen($this->request->post['phone']) != 18) {
                $this->error['phone'] = $data['phone']['error'];
            }
        }

        if($data['email']['required']){
            if (!wss_validate($this->request->post['email'])) {
                $this->error['email'] = $data['email']['error'];
            }
        }

        if($data['message']['required']){
            if ((utf8_strlen($this->request->post['message']) < 3)) {
                $this->error['message'] = $data['message']['error'];
            }
        }

        if(1){
            $captcha = $this->load->controller('captcha/' . $this->config->get('config_captcha') . '/validate');

            if ($captcha) {
                $this->error['captcha'] = $captcha;
            }
        }

        if (!$this->error) {
            return true;
        } else {
            return $this->error;
        }
    }

}

?>
