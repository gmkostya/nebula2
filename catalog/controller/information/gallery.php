<?php
class ControllerInformationGallery extends Controller {
	public function index() {
		$this->load->language('information/gallery');

        $this->load->model('tool/image');
        $this->load->model('design/gallery');

//        $this->document->addStyle('https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.css');
//        $this->document->addScript('https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js');

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        //id баннера кторый будет использыватся для галереи
        if($this->config->get('gallery_banner_id')){
            $banner_id = $this->config->get('gallery_banner_id');
        }else {
            $banner_id = 9;
        }

        if($this->config->get('gallery_limit')){
            $limit = $this->config->get('gallery_limit');
        }else{
            $limit = 9;
        }

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

        $this->document->setTitle($this->language->get('heading_title'));
        $this->document->setDescription($this->language->get('heading_title'));
        $this->document->setKeywords($this->language->get('heading_title'));

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('information/gallery', '' , 'SSL')
        );

        $data['heading_title'] = $this->language->get('heading_title');
        $data['button_continue'] = $this->language->get('button_continue');
        $data['text_empty'] = $this->language->get('text_empty');
        $data['continue'] = $this->url->link('common/home');

        $filter_data = array(
            'banner_id'      => $banner_id,
            'start'          => ($page - 1) * $limit,
            'limit'          => $limit
        );

        $photo_total = $this->model_design_gallery->getCountPhotoFromBanner($filter_data);
        $results = $this->model_design_gallery->getPhotoFromBanner($filter_data);

        $data['images'] = array();
        foreach ($results as $k => $result){


            if($result['image']){
                $image = $this->model_tool_image->resize($result['image'], 360, 360);
            }else{
                $image = $this->model_tool_image->resize('no_image.png', 360, 360);
            }

            if($result['image']){
                $thumb = $this->model_tool_image->resize($result['image'], 750, 750);
            }else{
                $thumb = $this->model_tool_image->resize('no_image.png', 750, 750);
            }

            $data['images'][] = array(
                'title' => $result['title'],
                'alt' => $result['alt'],
                'video' => $result['video'],
                'image' => $image,
                'thumb' => $thumb,
            );
        }

        $data['results'] = sprintf($this->language->get('text_pagination'), ($photo_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($photo_total - $limit)) ? $photo_total : ((($page - 1) * $limit) + $limit), $photo_total, ceil($photo_total / $limit));


        $pagination = new Pagination();
        $pagination->total = $photo_total;
        $pagination->page = $page;
        $pagination->limit = $limit;
        $pagination->text_next = '<i class="icon-arrow-right"></i>';
        $pagination->text_prev = '<i class="icon-arrow-left"></i>';
        $pagination->url = $this->url->link('information/gallery', 'page={page}', 'SSL');

        $data['pagination'] = $pagination->render();

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/gallery.tpl')) {
            $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/information/gallery.tpl', $data));
        } else {
            $this->response->setOutput($this->load->view('default/template/information/gallery.tpl', $data));
        }

	}
}