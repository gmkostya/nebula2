<div class="buttons">
  <div class="pull-right">
    <input type="button" value="<?php echo $button_confirm; ?>" id="button-confirm" class="btn btn-default" data-loading-text="<?php echo $text_loading; ?>" />
  </div>
</div>
<script type="text/javascript"><!--
$('#button-confirm').on('click', function() {
	$.ajax({
		type: 'get',
		url: 'index.php?route=payment/cod/confirm',
		cache: false,
		beforeSend: function() {
			$('#button-confirm').button('loading');
		},
		complete: function() {
			$('#button-confirm').button('reset');
		},
		success: function() {
			location = '<?php echo $continue; ?>';
		}
	});
});
if ($('#simplecheckout_form_0 [name=notifications]:checked')) {
    $('#simplecheckout_button_confirm').prop('disabled', true);
    $('#button-confirm').prop('disabled', true);
} else {
    $('#simplecheckout_button_confirm').prop('disabled', false);
    $('#button-confirm').prop('disabled', false);

}
$('#simplecheckout_form_0 [name=notifications]').on('change', function () {
    if (this.checked) {
        $('#simplecheckout_button_confirm').prop('disabled', false);
        $('#button-confirm').prop('disabled', false);
    } else {
        $('#simplecheckout_button_confirm').prop('disabled', true);
        $('#button-confirm').prop('disabled', true);
    }
});
//--></script>
