<?php
/**
 * @var $product
 * @var $button_cart
 * @var $button_oneclick
 * @var $button_wishlist
 * @var $button_compare
 */
?>
<div class="product-layout__compare">
    <div class="product-layout non-hover product-layout-com">
        <a href="<?php echo $product['href']; ?>" class="product-layout__pct">
            <img src="<?php echo $product['thumb']; ?>" alt="<?php echo ($product['model']) ? $product['name'] . '(' . $product['model'] . ')' : $product['name'] ?>" title="<?php echo ($product['model']) ? $product['name'] . ', артикул: ' . $product['model'] : $product['name']; ?>">
        </a>
        <a href="<?php echo $product['href']; ?>" class="product-layout__name" title="<?php echo $product['name']; ?>"><?php echo $product['name']; ?></a>
        <div class="product-layout__wishlist-compare">
            <a href="javascript:void(0);" title="<?php echo $button_wishlist; ?>" <?= $product['wishlist_status'] ? 'class="wishlist-'.$product['product_id'].' active"' : 'class="wishlist-'.$product['product_id'].'" onclick="wishlist.add('. $product['product_id'] . ');"' ?>><i class="icon-wishlist"></i></a>
        </div>
        <a href="<?php echo $product['remove']; ?>" class="product-layout__remove"><i class="icon-close"></i></a>
    </div>
    <div class="product-layout__atributes">
        <?php if ($product['price']) { ?>
            <?php if (!$product['special']) { ?>
                <div><?php echo $product['price']; ?></div>
            <?php } else { ?>
                <div><?php echo $product['special']; ?></div>
            <?php } ?>
        <?php } ?>
        <?php /*<div><?php echo $product['category_name']; ?></div>*/?>
        <div><?php if ($product['manufacturer']) { echo $product['manufacturer']; } else { echo '-'; } ?></div>
        <div><?php if ($product['colour']) { echo $product['colour']; } else { echo '-'; } ?></div>
    </div>
    <div class="product-layout__compare__buy flex-container btn-center">
        <?php if($product['availability']){ ?>
        <a href="javascript:void(0);" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="btn btn-default btn-medium"><span><?php echo $button_cart; ?></span></a>
        <?php } ?>
    </div>
</div>