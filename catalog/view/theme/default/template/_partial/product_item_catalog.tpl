<?php
/**
 * @var $product
 * @var $button_cart
 * @var $button_oneclick
 * @var $button_wishlist
 * @var $button_compare
 */
?>
<div class="product-layout non-hover">
    <a href="<?php echo $product['href']; ?>" class="product-layout__pct">
        <img src="<?php echo $product['thumb']; ?>" alt="<?php echo ($product['model']) ? $product['name'] . '(' . $product['model'] . ')' : $product['name'] ?>" title="<?php echo ($product['model']) ? $product['name'] . ', артикул: ' . $product['model'] : $product['name']; ?>">
    </a>
    <a href="<?php echo $product['href']; ?>" class="product-layout__name" title="<?php echo $product['name']; ?>"><?php echo $product['name']; ?></a>
    <div class="product-layout__desc"><?php echo $product['description']; ?></div>
    <?php if ($product['price']) { ?>
        <?php if (!$product['special']) { ?>
            <div class="product-layout__price"><?php echo $product['price']; ?></div>
        <?php } else { ?>
            <div class="product-layout__price-old"><?php echo $product['price']; ?></div>
            <div class="product-layout__price"><?php echo $product['special']; ?></div>
        <?php } ?>
    <?php } ?>
    <div class="product-layout__actions flex-container flex-center flex-top">
        <?php if($product['availability']){ ?>
        <a href="javascript:void(0);" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="btn btn-default"><span><?php echo $button_cart; ?></span></a>
        <a href="javascript:void(0);" onclick="get_popup_purchase('<?php echo $product['product_id']; ?>');" class="more-link"><?php echo $button_oneclick; ?></a>
        <?php } ?>
    </div>
    <div class="product-layout__wishlist-compare">
        <a href="javascript:void(0);" title="<?php echo $button_wishlist; ?>" <?= $product['wishlist_status'] ? 'class="wishlist-'.$product['product_id'].' active"' : 'class="wishlist-'.$product['product_id'].'" onclick="wishlist.add('. $product['product_id'] . ');"' ?>><i class="icon-wishlist"></i></a>
        <a href="javascript:void(0);" title="<?php echo $button_compare; ?>" <?= $product['compare_status'] ? 'class="compare-'.$product['product_id'].' active"' : 'class="compare-'.$product['product_id'].'" onclick="compare.add(' . $product['product_id'] . ');"' ?>><i class="icon-libra"></i></a>
    </div>
    <?php if($product['for_whom']){ ?>
        <div class="product-layout__gender">
            <?php if($product['for_whom']==1){ ?>
                <i class="icon-women"></i>
            <?php } ?>
            <?php if($product['for_whom']==2){ ?>
                <i class="icon-man"></i>
            <?php } ?>
        </div>
    <?php } ?>
</div>