<?php
/**
 * @var $product
 * @var $button_cart
 * @var $button_oneclick
 * @var $button_wishlist
 * @var $button_compare
 */
$now = date('Y-m-d');
?>
<div class="product-layout non-hover non-hover-t">
    <?php if(($product['date_start']<=$now) && ($product['date_end']>$now)){ ?>
    <div class="product-layout__timer">
        <label>до конца акции</label>
        <div class="product-layout__counter" data-deadline="<?= $product['date_end']?>">
            <span class="days"></span> д : <span class="hours"></span> ч : <span class="minutes"></span> м : <span class="seconds"></span> с
        </div>
    </div>
    <?php } ?>
    <a href="<?php echo $product['href']; ?>" class="product-layout__pct">
        <img src="<?php echo $product['thumb']; ?>" alt="<?php echo ($product['model']) ? $product['name'] . '(' . $product['model'] . ')' : $product['name'] ?>" title="<?php echo ($product['model']) ? $product['name'] . ', артикул: ' . $product['model'] : $product['name']; ?>">
    </a>
    <a href="<?php echo $product['href']; ?>" class="product-layout__name" title="<?php echo $product['name']; ?>"><?php echo $product['name']; ?></a>
    <div class="product-layout__desc"><?php echo $product['description']; ?></div>
    <?php if ($product['price']) { ?>
        <?php if (!$product['special']) { ?>
            <div class="product-layout__price"><?php echo $product['price']; ?></div>
        <?php } else { ?>
            <div class="product-layout__old-price"><?php echo $product['price']; ?></div>
            <div class="product-layout__price"><?php echo $product['special']; ?></div>
        <?php } ?>
    <?php } ?>
    <?php /*
    <div class="product-layout__actions flex-container flex-between flex-top">
        <a href="javascript:void(0);" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="btn btn-default"><span><?php echo $button_cart; ?></span></a>
        <a href="javascript:void(0);" onclick="get_popup_purchase('<?php echo $product['product_id']; ?>');" class="btn btn-transparent"><?php echo $button_oneclick; ?></a>
    </div>
    <div class="product-layout__wishlist-compare">
        <a href="javascript:void(0);" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="icon-wishlist"></i></a>
        <a href="javascript:void(0);" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="icon-libra"></i></a>
    </div>*/?>
    <?php if($product['for_whom']){ ?>
        <div class="product-layout__gender">
            <?php if($product['for_whom']==1){ ?>
                <i class="icon-women"></i>
            <?php } ?>
            <?php if($product['for_whom']==2){ ?>
                <i class="icon-man"></i>
            <?php } ?>
        </div>
    <?php } ?>
</div>