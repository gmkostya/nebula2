<?php
/**
 * @var $product
 * @var $button_cart
 * @var $button_oneclick
 * @var $button_wishlist
 * @var $button_compare
 */
$now = date('Y-m-d');
?>
<div class="product-layout">
    <?php if(($product['date_start']<=$now) && ($product['date_end']>$now)){ ?>
    <div class="product-layout__timer">
        <label>до конца акции</label>
        <div class="product-layout__counter" data-deadline="<?= $product['date_end']?>">
            <span class="days"></span> д : <span class="hours"></span> ч : <span class="minutes"></span> м : <span class="seconds"></span> с
        </div>
    </div>
    <?php } ?>
    <a href="<?php echo $product['href']; ?>" class="product-layout__pct">
        <img src="<?php echo $product['thumb']; ?>" alt="<?php echo ($product['model']) ? $product['name'] . '(' . $product['model'] . ')' : $product['name'] ?>" title="<?php echo ($product['model']) ? $product['name'] . ', артикул: ' . $product['model'] : $product['name']; ?>">
    </a>
    <a href="<?php echo $product['href']; ?>" class="product-layout__name" title="<?php echo $product['name']; ?>"><?php echo $product['name']; ?></a>
    <div class="product-layout__desc"><?php echo $product['description']; ?></div>
    <?php if ($product['price']) { ?>
        <?php if (!$product['special']) { ?>
            <div class="product-layout__price"><?php echo $product['price']; ?></div>
        <?php } else { ?>
            <div class="product-layout__old-price"><?php echo $product['price']; ?></div>
            <div class="product-layout__price"><?php echo $product['special']; ?></div>
        <?php } ?>
    <?php } ?>
    <div class="product-layout__actions flex-container flex-between flex-top">
        <?php if($product['availability']){ ?>
        <a href="javascript:void(0);" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="btn btn-default"><span><?php echo $button_cart; ?></span></a>
        <a href="javascript:void(0);" onclick="get_popup_purchase('<?php echo $product['product_id']; ?>');" class="btn btn-transparent"><?php echo $button_oneclick; ?></a>
        <?php } ?>
    </div>
    <div class="product-layout__wishlist-compare">
        <a href="javascript:void(0);" title="<?php echo $button_wishlist; ?>" <?= $product['wishlist_status'] ? 'class="wishlist-'.$product['product_id'].' active"' : 'class="wishlist-'.$product['product_id'].'" onclick="wishlist.add('. $product['product_id'] . ');"' ?>><i class="icon-wishlist"></i></a>
        <a href="javascript:void(0);" title="<?php echo $button_compare; ?>" <?= $product['compare_status'] ? 'class="compare-'.$product['product_id'].' active"' : 'class="compare-'.$product['product_id'].'" onclick="compare.add(' . $product['product_id'] . ');"' ?>><i class="icon-libra"></i></a>
    </div>
    <?php if($product['for_whom']){ ?>
        <div class="product-layout__gender">
            <?php if($product['for_whom']==1){ ?>
                <i class="icon-women"></i>
            <?php } ?>
            <?php if($product['for_whom']==2){ ?>
                <i class="icon-man"></i>
            <?php } ?>
        </div>
    <?php } ?>
</div>
<?php /*
<div class="product-layout col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="product-thumb transition">
        <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
        <div class="caption">
            <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
            <p><?php echo $product['description']; ?></p>
            <?php if ($product['rating']) { ?>
                <div class="rating">
                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                        <?php if ($product['rating'] < $i) { ?>
                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                        <?php } else { ?>
                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                        <?php } ?>
                    <?php } ?>
                </div>
            <?php } ?>
            <?php if ($product['price']) { ?>
                <p class="price">
                    <?php if (!$product['special']) { ?>
                        <?php echo $product['price']; ?>
                    <?php } else { ?>
                        <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                    <?php } ?>
                    <?php if ($product['tax']) { ?>
                        <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                    <?php } ?>
                </p>
            <?php } ?>
        </div>
        <div class="button-group">
            <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button>
            <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart"></i></button>
            <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-exchange"></i></button>
        </div>
    </div>
</div>*/?>