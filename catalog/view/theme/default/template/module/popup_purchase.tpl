<div class="modal fade" id="popup-purchase-wrapper" tabindex="-1" role="dialog" aria-labelledby="popupPurchaseWrapper" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                <?php if ($thumb) { ?>
                <div class="modal-callback__pct">
                    <img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" />
                </div>
                <?php } ?>
                <div class="modal-callback__form">
                    <div class="section__heading">
                        <div class="section__title"><?php echo $heading_title; ?></div>
                        <div class="section__subtitle"><?php echo $product_name; ?></div>
                    </div>
                    <form method="post" enctype="multipart/form-data" id="purchase-form">
                        <input name="product_id" value="<?php echo $product_id; ?>" style="display: none;" type="hidden" />
                        <?php if ($popup_purchase_data['firstname']) { ?>
                            <div class="input-wrap">
                                <input type="text" name="firstname" id="purchase_firstname" value="<?php echo $firstname;?>" />
                                <label for="purchase_firstname"><i class="icon-log-in"></i><span><?php echo $enter_firstname; ?></span></label>
                            </div>
                        <?php } ?>
                        <?php if ($popup_purchase_data['telephone']) { ?>
                            <div class="input-wrap">
                                <input type="tel" name="telephone" id="purchase_telephone" value="<?php echo $telephone;?>" />
                                <label for="purchase_telephone"><i class="icon-call-back"></i><span><?php echo $enter_telephone; ?></span></label>
                            </div>
                        <?php } ?>
                        <?php if ($popup_purchase_data['email']) { ?>
                            <div class="input-wrap">
                                <input type="text" name="email" id="purchase_email" value="<?php echo $email;?>" />
                                <label for="purchase_email"><i class="icon-mail"></i><span><?php echo $enter_email; ?></span></label>
                            </div>
                        <?php } ?>
                        <div class="check-wrap">
                            <input type="checkbox" name="notifications" id="purchase_notifications">
                            <label for="purchase_notifications"><?= $text_notifications ?></label>
                        </div>
                        <input type="hidden" name="quantity" value="<?php echo $minimum; ?>" />
                        <button id="popup-checkout-button" class="btn btn-default btn-center"><span><?php echo $button_checkout; ?></span></button>
                    </form>
                    <?php if (!$stock_warning) { ?>
                    <?php /*
                    <script src="catalog/view/javascript/jquery/datetimepicker/moment.js" type="text/javascript"></script>
                    <script src="catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
                    <link href="catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
                    <script type="text/javascript"><!--
                    $('.date').datetimepicker({
                    pickTime: false,
                    });

                    $('.datetime').datetimepicker({
                    pickDate: true,
                    pickTime: true
                    });

                    $('.time').datetimepicker({
                    pickDate: false,
                    });

                    $(document).on('click', 'button[id^=\'button-upload\']', function() {
                    var node = this;

                    $('#form-upload').remove();

                    $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

                    $('#form-upload input[name=\'file\']').trigger('click');

                    if (typeof timer != 'undefined') {
                      clearInterval(timer);
                    }

                    timer = setInterval(function() {
                    if ($('#form-upload input[name=\'file\']').val() != '') {
                      clearInterval(timer);

                      $.ajax({
                        url: 'index.php?route=tool/upload',
                        type: 'post',
                        dataType: 'json',
                        data: new FormData($('#form-upload')[0]),
                        cache: false,
                        contentType: false,
                        processData: false,
                        beforeSend: function() {
                          $(node).button('loading');
                        },
                        complete: function() {
                          $(node).button('reset');
                        },
                        success: function(json) {
                          $('.text-danger').remove();

                          if (json['error']) {
                            $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
                          }

                          if (json['success']) {
                            alert(json['success']);

                            $(node).parent().find('input').attr('value', json['code']);
                          }
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                          alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                        }
                      });
                    }
                    }, 500);
                    });
                    //--></script>*/?>
                    <script type="text/javascript"><!--
                    window.onload = function() {
                        // console.log('222');
                        function masked(element, status) {
                            if (status == true) {
                                $('<div/>')
                                    .attr({'class': 'masked'})
                                    .prependTo(element);
                                $('<div class="masked_loading" />').insertAfter($('.masked'));
                            } else {
                                $('.masked').remove();
                                $('.masked_loading').remove();
                            }
                        }

                        <?php if ($popup_purchase_data['quantity']) { ?>
                        function validate(input) {
                            input.value = input.value.replace(/[^\d,]/g, '');
                        }
                        <?php } ?>

                        <?php if ($popup_purchase_data['quantity']) { ?>
                        function update_prices(product_id) {
                            masked('#popup-purchase-wrapper', true);
                            var input_val = $('#purchase-form').find('input[name=quantity]').val();
                            var quantity = parseInt(input_val);

                            <?php if ($minimum > 1) { ?>
                            if (quantity < <?php echo $minimum; ?>) {
                                quantity = $('#purchase-form').find('input[name=quantity]').val(<?php echo $minimum; ?>);
                                masked('#popup-purchase-wrapper', false);
                                return;
                            }
                            <?php } else { ?>
                            if (quantity == 0) {
                                quantity = $('#purchase-form').find('input[name=quantity]').val(1);
                                masked('#popup-purchase-wrapper', false);
                                return;
                            }
                            <?php } ?>

                            $.ajax({
                                url: 'index.php?route=module/popup_purchase/update_prices&product_id=' + product_id + '&quantity=' + quantity,
                                type: 'post',
                                dataType: 'json',
                                data: $('#purchase-form').serialize(),
                                success: function (json) {
                                    $('#main-price').html(json['price']);
                                    $('#special-price').html(json['special']);
                                    $('#main-tax').html(json['tax']);
                                    masked('#popup-purchase-wrapper', false);
                                }
                            });
                        }
                        <?php } ?>

                        $('select[name=\'recurring_id\'], input[name="quantity"]').change(function () {
                            $.ajax({
                                url: 'index.php?route=product/product/getRecurringDescription',
                                type: 'post',
                                data: $('#purchase-form input[name=\'product_id\'], #purchase-form input[name=\'quantity\'], #purchase-form select[name=\'recurring_id\']'),
                                dataType: 'json',
                                beforeSend: function () {
                                    $('#recurring-description').html('');
                                },
                                success: function (json) {
                                    $('.alert, .text-danger').remove();

                                    if (json['success']) {
                                        $('#recurring-description').html(json['success']);
                                    }
                                }
                            });
                        });
                    };
                    //--></script>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
