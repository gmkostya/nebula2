<?php echo $header; ?>
<main class="profile">
    <div class="page-heading" style="background-image: url('/catalog/view/theme/default/html/build/img/checkout-main-bg.jpg');">
        <div class="breadcrumbs container">
            <?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>
        </div>
        <div class="container">
            <h1 class="page-heading__title"><?php echo $heading_title; ?></h1>
        </div>
    </div>
    <div class="profile-wrap container">
        <?php echo $column_right; ?>
        <?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
            <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
            <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
            <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div class="profile-body"><?php echo $content_top; ?>

            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                <input type="hidden" name="social_id" value="<?php echo $social_id; ?>" />
                    <legend>Уточните Ваши контактные данные...</legend>
                    <div class="input-wrap required">
                        <input type="text" name="firstname" value="<?php echo $firstname; ?>"  id="input-firstname" />
                        <label for="input-firstname"><i class="icon-log-in"></i><span><?php echo $entry_firstname; ?> </span></label>
                            <?php if ($error_firstname) { ?>
                                <label class="error" for="input-firstname"><i class="icon-log-in"></i><span><?php echo $error_firstname; ?></span></label>
                            <?php } ?>
                    </div>
                
                    <div class="input-wrap required">
                        <input type="text" name="lastname" value="<?php echo $lastname; ?>" id="input-lastname"  />
                        <label  for="input-lastname"><i class="icon-log-in"></i><span><?php echo $entry_lastname; ?></span></label>
                            <?php if ($error_lastname) { ?>
                        <label  class="error"  for="input-lastname"><i class="icon-log-in"></i><span><?php echo $error_lastname; ?></span></div>
                            <?php } ?>
                    </div>
                    <div class="input-wrap required">
                        <input type="email" name="email" value="<?php echo $email; ?>" id="input-email" />

                        <label  for="input-email"><i class="icon-mail"></i><span><?php echo $entry_email; ?></span></label>
                            <?php if ($error_email) { ?>
                                <label   class="error"   for="input-mail"><i class="icon-pass"></i><span><?php echo $entry_email; ?></span></label>
                            <?php } ?>
                    </div>
        <div class="input-wrap required">
                        <input type="tel" name="telephone" value="<?php echo $telephone; ?>" id="input-telephone" />

                        <label  for="input-telephone"><i class="icon-call-back"></i><span><?php echo $entry_telephone; ?></span></label>
                            <?php if ($error_telephone) { ?>
                                <label   class="error"   for="icon-call-back"><i class="icon-pass"></i><span><?php echo $entry_telephone; ?></span></label>
                            <?php } ?>
                    </div>

        <button type="submit" class="btn btn-default"><span><?php echo $button_continue; ?></span></button>
            </form>
            
            
            
            <?php echo $content_bottom; ?>
        </div>
    </div>
</main>
<?php echo $footer; ?>
