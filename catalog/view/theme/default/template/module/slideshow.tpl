<?php
/**
 * @var $banners
 */
?>
<?php if($banners[0]['image']){ ?>
<div class="main-banner">
    <img src="<?= ($banners[0]['image']) ? $banners[0]['image'] : '' ?>"<?= ($banners[0]['alt']) ? ' alt="'.$banners[0]['alt'].'"' : '' ?><?= ($banners[0]['title']) ? ' title="'.$banners[0]['title'].'"' : '' ?>>
    <div class="next-slide"><i class="icon-arrow-down"></i></div>
</div>
<?php } ?>