<?php if($products){ ?>
    <div class="product__archive">
        <div class="product__archive__title">Ранее просмотренные</div>
        <div class="carousel-wrap">
            <?php $count = count($products); ?>
            <?php if($count>4){ ?>
                <div class="carousel-nav">
                    <i class="icon-arrow-left"></i>
                    <i class="icon-arrow-right"></i>
                </div>
            <?php } ?>
            <div class="products-carousel products-carousel-similar">
                <?php foreach ($products as $product) { ?>
                    <?php $this->partial('product_item_catalog', array('product' => $product, 'button_cart' => $button_cart, 'button_oneclick' => $button_oneclick,  'button_wishlist' => $button_wishlist, 'button_compare' => $button_compare));?>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>