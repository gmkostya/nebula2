<div class="reviews__tabs">
    <?php if (!$logged) { ?>
        <div class="reviews__tab"><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></div>
        <div class="reviews__tab"><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></div>
        <div class="reviews__tab"><a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a></div>
    <?php } ?>
    <?php /* <div class="reviews__tab"><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></div>*/?>
    <?php if ($logged) { ?>
        <div class="reviews__tab"><a href="<?php echo $edit; ?>"><?php echo $text_edit; ?></a></div>
        <div class="reviews__tab"><a href="<?php echo $password; ?>"><?php echo $text_password; ?></a></div>
    <?php } ?>
    <div class="reviews__tab"><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></div>
    <?php if ($logged) { ?>
        <div class="reviews__tab"><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></div>
    <?php } ?>
</div>