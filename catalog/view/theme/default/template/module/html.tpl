<div class="main-about">
    <?php if($heading_title) { ?>
    <div class="section__heading">
        <h1 class="section__title"><?php echo $heading_title; ?></h1>
    </div>
    <?php } ?>
    <?php echo $html; ?>
</div>