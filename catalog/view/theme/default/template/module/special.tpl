<?php
/**
 * @var $title
 * @var $subtitle
 * @var $products
 * @var $button_cart
 * @var $button_oneclick
 * @var $button_wishlist
 * @var $button_compare
 */
?>
<div class="section">
    <div class="section__heading">
        <div class="section__title"><?= $title ?></div>
        <div class="section__subtitle"><?= $subtitle ?></div>
    </div>
    <div class="container">
        <div class="carousel-wrap">
            <?php $count = count($products); ?>
            <?php if($count>3){ ?>
                <div class="carousel-nav">
                    <i class="icon-arrow-left"></i>
                    <i class="icon-arrow-right"></i>
                </div>
            <?php } ?>
            <div class="products-carousel">
                <?php foreach ($products as $product) { ?>
                    <?php $this->partial('product_item_special', array('product' => $product, 'button_cart' => $button_cart, 'button_oneclick' => $button_oneclick, 'button_wishlist' => $button_wishlist, 'button_compare' => $button_compare));?>
                <?php } ?>
            </div>
        </div>
    </div>
</div>