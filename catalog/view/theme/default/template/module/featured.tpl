<div class="section">
    <div class="section__heading">
        <div class="section__title"><?= $title ?></div>
        <div class="section__subtitle"><?= $sub_title ?></div>
    </div>
    <div class="container">
        <div class="carousel-wrap">
            <?php $count = count($products); ?>
            <?php if($count>6){ ?>
            <div class="carousel-nav">
                <i class="icon-arrow-left"></i>
                <i class="icon-arrow-right"></i>
            </div>
            <?php } ?>
            <div class="catalog-carousel">
                <?php foreach ($products as $product) { ?>
                    <?php $this->partial('product_item', array('product' => $product, 'button_cart' => $button_cart, 'button_oneclick' => $button_oneclick, 'button_wishlist' => $button_wishlist, 'button_compare' => $button_compare));?>
                <?php } ?>
            </div>
        </div>
        <a href="<?= $url_catalog ?>" class="btn btn-default btn-center"><span><?= $title ?></span></a>
    </div>
</div>