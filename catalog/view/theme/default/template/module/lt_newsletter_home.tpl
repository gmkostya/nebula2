<div class="section section-subscribe section-dark">
    <div class="container">
        <div class="section__heading">
            <div class="section__title"><?= $heading_title ?></div>
            <div class="section__subtitle"><?= $text_intro ?></div>
        </div>
        <form class="subscribe" id="lt_newsletter_form">
            <div class="input-wrap">
                <input type="email" id="lt_newsletter_email" name="lt_newsletter_email">
                <label for="lt_newsletter_email"><i class="icon-mail"></i><span><?= $entry_email; ?></span></label>
            </div>
            <button type="submit" class="btn btn-default"><span><?= $text_button; ?></span></button>
        </form>
    </div>
</div>