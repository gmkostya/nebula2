<div class="section__heading">
    <div class="section__title"><?= $heading_title; ?></div>
    <div class="section__subtitle"><?= $text_headlines ?></div>
</div>
<div class="carousel-wrap">
    <?php $count = count($article); ?>
    <?php if($count>3){ ?>
    <div class="carousel-nav">
        <i class="icon-arrow-left"></i>
        <i class="icon-arrow-right"></i>
    </div>
    <?php } ?>
    <div class="news-carousel">
        <?php foreach ($article as $articles) { ?>
            <div class="news-layout">
                <?php if ($articles['thumb']) { ?>
                    <a href="<?php echo $articles['href']; ?>" class="news-layout__pct">
                        <img src="<?php echo $articles['thumb']; ?>" title="<?php echo strip_tags($articles['name']); ?>" alt="<?php echo strip_tags($articles['name']); ?>">
                    </a>
                <?php } ?>
                <a href="<?php echo $articles['href']; ?>" class="news-layout__title"><?php echo html_entity_decode($articles['name']); ?></a>
                <div class="news-layout__hover">
                    <a href="<?php echo $articles['href']; ?>" class="news-layout__title"><?php echo html_entity_decode($articles['name']); ?> </a>
                    <?php if ($articles['description']) { ?>
                        <div class="news-layout__desc"><?php echo $articles['description']; ?></div>
                    <?php } ?>
                    <a href="<?php echo $articles['href']; ?>" class="more-link"><?php echo $button_more; ?></a>
                </div>
            </div>
        <?php } ?>
    </div>
</div>