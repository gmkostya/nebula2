<?php
/**
 * @var $title
 * @var $subtitle
 * @var $products
 * @var $button_cart
 * @var $button_oneclick
 * @var $button_wishlist
 * @var $button_compare
 */
?>
<div class="products-carousel products-carousel-single">
    <?php foreach ($products as $product) { ?>
        <?php $this->partial('product_item_special2', array('product' => $product, 'button_cart' => $button_cart, 'button_oneclick' => $button_oneclick, 'button_wishlist' => $button_wishlist, 'button_compare' => $button_compare));?>
    <?php } ?>
</div>