<div class="input-captcha__label"><?php echo $entry_captcha; ?></div>
<div class="captcha"><img src="index.php?route=captcha/basic_captcha/captcha" alt="captcha"></div>
<i class="icon-refresh" id="basic_captcha-refresh"></i>
<div class="input-wrap">
    <input type="text" name="captcha" id="contacts-captcha"/>
    <?php if ($error_captcha) { ?>
        <label for="contacts-captcha" class="error"><?php echo $error_captcha; ?></label>
    <?php } ?>
</div>
<script>
// window.onload = function() {
    $('#basic_captcha-refresh').click(function () {
        console.log('click');
        $(this).prev().find('img').attr('src', 'index.php?route=captcha/basic_captcha/captcha');
    });
// }
</script>