<?php if ($reviews) { ?>
    <div class="reviews__list">
        <?php foreach ($reviews as $review) { ?>
            <div class="product-reviews__item">
                <a href="<?= $review['href'] ?>" class="product-reviews__pct">
                    <img src="<?= $review['image'] ?>" alt="<?= $review['prod_name'] ?>">
                </a>
                <div class="product-reviews__body">
                    <div class="product-reviews__meta flex-container flex-left flex-bottom">
                        <div class="product-reviews__author"><?php echo $review['author']; ?></div>
                        <div class="product-reviews__date"><?php echo $review['date_added'] ?></div>
                    </div>
                    <div class="product-reviews__rating flex-container flex-left flex-top">
                        <?php for($i=0;$i<$review['rating'];$i++){ ?>
                            <i class="icon-star-full"></i>
                        <?php } ?>
                    </div>
                    <div class="product-reviews__text"><?php echo $review['text']; ?></div>
                </div>
            </div>
        <?php } ?>
    </div>
    <div class="pagination">
        <?php echo $pagination; ?>
    </div>
<?php } else { ?>
    <div class="reviews__list">
        <p><?php echo $text_no_reviews; ?></p>
    </div>
<?php } ?>