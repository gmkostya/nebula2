<?php echo $header; ?>
<main class="reviews">
    <div class="page-heading" style="background-image: url('/catalog/view/theme/default/html/build/img/review-heading-bg.jpg');">
        <div class="breadcrumbs container">
            <?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>
        </div>
        <div class="container">
            <h1 class="page-heading__title"><?= $heading_title ?></h1>
            <div class="page-heading__subtitle"><?= $text_heading ?></div>
        </div>
    </div>
    <div class="reviews__wrap container flex-container flex-between flex-stretch">
        <div class="reviews__body">
            <div class="reviews__tabs">
                <div class="reviews__tab active"><a href="javascript:void(0)"><?= $tab_shop_review ?></a></div>
                <div class="reviews__tab"><a href="<?= $rev_products ?>"><?= $tab_review ?></a></div>
            </div>
            <div id="shop_reviews">

            </div>
        </div>
        <div class="reviews__side">
            <div class="catalog-filter__title">Поделись своим опытом</div>
            <form id="form-review">
                <div class="input-wrap">
                    <input type="text" name="name" value="" id="input-name"">
                    <label for="input-name"><i class="icon-log-in"></i><span><?php echo $entry_name; ?></span></label>
                </div>
                <div class="input-rating">
                    <div class="input-rating__radio">
                        <input type="radio" name="rating" value="1" id="rating-1">
                        <label for="rating-1"></label>
                    </div>
                    <div class="input-rating__radio">
                        <input type="radio" name="rating" value="2" id="rating-2">
                        <label for="rating-2"></label>
                    </div>
                    <div class="input-rating__radio">
                        <input type="radio" name="rating" value="3" id="rating-3">
                        <label for="rating-3"></label>
                    </div>
                    <div class="input-rating__radio">
                        <input type="radio" name="rating" value="4" id="rating-4">
                        <label for="rating-4"></label>
                    </div>
                    <div class="input-rating__radio">
                        <input type="radio" name="rating" value="5" id="rating-5">
                        <label for="rating-5"></label>
                    </div>
                </div>
                <div class="input-wrap">
                    <textarea name="text" id="text_review" placeholder="<?php echo $entry_review; ?>"></textarea>
                </div>
                <div class="check-wrap">
                    <input type="checkbox" name="notifications" id="notifications">
                    <label for="notifications"><?= $text_notifications ?></label>
                </div>
                <button id="button-review" class="btn btn-default btn-center"><span>Поделиться опытом</span></button>
            </form>
        </div>
    </div>
</main>
<?php echo $content_bottom; ?>
<script type="text/javascript">
// window.onload = function() {
    $('#shop_reviews').delegate('.pagination a', 'click', function (e) {
        e.preventDefault();
        $('#shop_reviews').load(this.href);
    });

    $('#shop_reviews').load('<?php echo html_entity_decode($review); ?>');

    <?php /*$('#productReviews').delegate('.pagination a', 'click', function(e) {
        e.preventDefault();
        console.log(this.href);

        $('#productReviews').fadeOut('slow');

        $('#productReviews').load(this.href);

        $('#productReviews').fadeIn('slow');
    });

    $('#productReviews').load('<?php echo html_entity_decode($review_prod); ?>');*/?>

    $('#form-review input, #form-review textarea').on('focus', function() {
        $(this).parent('.input-wrap').find('label.error').remove();
    });
    if($('#form-review [name=notifications]:checked')){
        $('#form-review button').prop('disabled',true);
    }else{
        $('#form-review button').prop('disabled', false);
    }
    $('#form-review [name=notifications]').on('change', function() {
        if(this.checked){
            $('#form-review button').prop('disabled', false);
        }else{
            $('#form-review button').prop('disabled',true);
        }
    });

    $('#button-review').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            url: '<?php echo html_entity_decode($write); ?>',
            type: 'post',
            dataType: 'json',
            data: $("#form-review").serialize(),
            beforeSend: function () {
                if ($("textarea").is("#g-recaptcha-response")) {
                    grecaptcha.reset();
                }
                $('#button-review').button('loading');
            },
            complete: function () {
                $('#button-review').button('reset');
            },
            success: function (json) {
                // $('#form-review *.error').removeClass('error');
                $('#form-review .text-danger').remove();
                $("#form-review label.error").remove();

                // if (json['error']) {
                //     $('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
                // }

                if (json['error']) {
                    $.each(json['error'], function (key, value) {

                        if(key == 'name'){
                            $('#form-review input[name=' + key + ']').parent().append('<label for="input-name" class="error"><i class="icon-log-in"></i><span>' + value + '</span></label>');
                        }

                        if(key == 'text'){
                            $('#form-review textarea[name=' + key + ']').parent().append('<label for="text_review" class="error"><span>' + value + '</span></label>');
                        }

                        /*if (key != 'rating') {
                            // $('#form-review input[name=' + key + '], #form-review textarea[name=' + key + ']').addClass('error');
                            $('#form-review input[name=' + key + '], #form-review textarea[name=' + key + ']').parent().append('<label for="reg-input-name" class="error"><i class="icon-log-in"></i><span>'+json['error']['error_name']+'</span></label>');
                                .after('<span class="text-danger">' + value + '</span>');

                        }*/
                        if (key == 'rating') {
                            $('.input-rating').addClass('error');
                            $('.input-rating').append('<span class="text-danger">' + value + '</span>');
                        }
                    });
                    //$('#form-review h2').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'][Object.keys(json['error'])[0]] + '</div>');
                }
                if (json['success']) {
                    //$('#form-review h2').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

                    /*if($('.control_container').outerWidth() == 1) {
                        $('body, html').animate({scrollTop: $('.alert').offset().top - 50}, 150);
                    }*/

                    $('#form-review').find("input").removeClass('filled');
                    $('input[name=\'name\']').val('');
                    // $('input[name=\'phone\']').val('');
                    $('textarea[name=\'text\']').val('');
                    $('input[name=\'rating\']:checked').prop('checked', false);

                    $('#thankYou .page-heading-simple__subtitle').html(json['subtitle']);
                    $('#thankYou .page-heading-simple__title').html(json['title']);
                    $('#thankYou .modal-body__text').html(json['success']);
                    $('#thankYou').modal('show');
                    setTimeout(function () {
                        $('#thankYou').modal('hide');
                    }, 5000);

                    /*$('#modal_default .modal-title').html(json['title']);
                    $('#modal_default .modal-body').html('<div class="text-center">' + json['success'] + '</div>');
                    $('#modal_default').modal('show');

                    setTimeout(function () {
                        $('#modal_default').modal('hide');
                    }, 4000);*/
                }
            }
        });
    });
// };
</script>
<?php echo $footer; ?>
