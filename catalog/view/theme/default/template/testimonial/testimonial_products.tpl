<?php echo $header; ?>
<main class="reviews">
    <div class="page-heading" style="background-image: url('/catalog/view/theme/default/html/build/img/review-heading-bg.jpg');">
        <div class="breadcrumbs container">
            <?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>
        </div>
        <div class="container">
            <h1 class="page-heading__title"><?= $heading_title ?></h1>
            <div class="page-heading__subtitle"><?= $text_heading ?></div>
        </div>
    </div>
    <div class="reviews__wrap container flex-container flex-between flex-stretch">
        <div class="reviews__body full-width">
            <div class="reviews__tabs">
                <div class="reviews__tab"><a href="<?= $rev_shop ?>"><?= $tab_shop_review ?></a></div>
                <div class="reviews__tab active"><a href="javascript:void(0)"><?= $tab_review ?></a></div>
            </div>
            <div id="products_reviews">

            </div>
        </div>
    </div>
</main>
<?php echo $content_bottom; ?>
<script type="text/javascript"><!--
// window.onload = function() {
    $('#products_reviews').delegate('.pagination a', 'click', function (e) {
        e.preventDefault();
        $('#products_reviews').load(this.href);
    });

    $('#products_reviews').load('<?php echo html_entity_decode($review_prod); ?>');

    <?php /*$('#productReviews').delegate('.pagination a', 'click', function(e) {
        e.preventDefault();
        console.log(this.href);

        $('#productReviews').fadeOut('slow');

        $('#productReviews').load(this.href);

        $('#productReviews').fadeIn('slow');
    });

    $('#productReviews').load('<?php echo html_entity_decode($review_prod); ?>');

    $('#button-review').on('click', function () {
        $.ajax({
            url: '<?php echo html_entity_decode($write); ?>',
            type: 'post',
            dataType: 'json',
            data: $("#form-review").serialize(),
            beforeSend: function () {
                if ($("textarea").is("#g-recaptcha-response")) {
                    grecaptcha.reset();
                }
                $('#button-review').button('loading');
            },
            complete: function () {
                $('#button-review').button('reset');
            },
            success: function (json) {
                $('#form-review *.error').removeClass('error');
                $('#form-review .text-danger').remove();

                // if (json['error']) {
                //     $('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
                // }

                if (json['error']) {
                    $.each(json['error'], function (key, value) {

                        if (key != 'rating') {
                            $('#form-review input[name=' + key + '], #form-review textarea[name=' + key + ']').addClass('error');
                            $('#form-review input[name=' + key + '], #form-review textarea[name=' + key + ']').after('<span class="text-danger">' + value + '</span>');
                        }
                        if (key == 'rating') {
                            $('.rating-wrap').addClass('error');
                            $('.rating-wrap').after('<span class="text-danger">' + value + '</span>');
                        }
                    });
                    //$('#form-review h2').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'][Object.keys(json['error'])[0]] + '</div>');
                }
                if (json['success']) {
                    //$('#form-review h2').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

                    /*if($('.control_container').outerWidth() == 1) {
                        $('body, html').animate({scrollTop: $('.alert').offset().top - 50}, 150);
                    }*

                    $('input[name=\'name\']').val('');
                    // $('input[name=\'phone\']').val('');
                    $('textarea[name=\'text\']').val('');
                    $('input[name=\'rating\']:checked').prop('checked', false);

                    $('#modal_default .modal-title').html(json['title']);
                    $('#modal_default .modal-body').html('<div class="text-center">' + json['success'] + '</div>');
                    $('#modal_default').modal('show');

                    setTimeout(function () {
                        $('#modal_default').modal('hide');
                    }, 4000);
                }
            }
        });
    });*/?>
// };
//--></script>
<?php echo $footer; ?>
