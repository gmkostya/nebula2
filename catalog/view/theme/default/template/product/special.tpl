<?php echo $header; ?>
    <main class="catalog">
        <div class="page-heading" style="background-image: url('/catalog/view/theme/default/html/build/img/catalog-heading-bg.jpg');">
            <div class="breadcrumbs container">
                <?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>
            </div>
            <div class="container">
                <h1 class="page-heading__title"><?php echo $heading_title; ?></h1>
                <div class="page-heading__subtitle">Большой выбор интересных товаров для ярких впечетлений</div>
            </div>
        </div>
        <div class="catalog-wrapper container">
            <div class="flex-container flex-between flex-top">
                <div class="catalog-filter">
                    <?php echo $column_left; ?>
                </div>
                <div id="content" class="catalog-container">
                    <?php if ($products) { ?>
                        <div class="catalog-actions flex-container flex-between flex-top">
                            <div class="catalog-sorting flex-container flex-left flex-top">
                                <div class="catalog-sorting__label">Сортировать по:</div>
                                <select id="input-sort" class="form-control" onchange="location = this.value;">
                                    <option value="http://nebulatoys-back/shop/?sort=p.sort_order&amp;order=ASC" selected="selected">По умолчанию</option>
                                    <option value="http://nebulatoys-back/shop/?sort=pd.name&amp;order=ASC">По Имени (A - Я)</option>
                                    <option value="http://nebulatoys-back/shop/?sort=pd.name&amp;order=DESC">По Имени (Я - A)</option>
                                    <option value="http://nebulatoys-back/shop/?sort=p.price&amp;order=ASC">По Цене (возрастанию)</option>
                                    <option value="http://nebulatoys-back/shop/?sort=p.price&amp;order=DESC">По Цене (убыванию)</option>
                                    <option value="http://nebulatoys-back/shop/?sort=rating&amp;order=DESC">По Рейтингу (убыванию)</option>
                                    <option value="http://nebulatoys-back/shop/?sort=rating&amp;order=ASC">По Рейтингу (возрастанию)</option>
                                    <option value="http://nebulatoys-back/shop/?sort=p.model&amp;order=ASC">По Модели (A - Я)</option>
                                    <option value="http://nebulatoys-back/shop/?sort=p.model&amp;order=DESC">По Модели (Я - A)</option>
                                </select>
                            </div>
                            <div class="catalog-view">
                                <span data-view="x2"><i class="icon-x"></i></span>
                                <span data-view="x3"><i class="icon-x1"></i></span>
                                <span data-view="x4"><i class="icon-x2"></i></span>
                            </div>
                        </div>
                        <div class="catalog-body">
                            <?php foreach ($products as $product) { ?>
                                <?php $this->partial('product_item_catalog', array('product' => $product, 'button_cart' => $button_cart, 'button_oneclick' => $button_oneclick,  'button_wishlist' => $button_wishlist, 'button_compare' => $button_compare));?>
                            <?php } ?>
                            <?php echo $pagination; ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <?php echo $content_bottom; ?>
    </main>
<?php /*
<div class="container">
    <?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <?php if ($products) { ?>
      <p><a href="<?php echo $compare; ?>" id="compare-total"><?php echo $text_compare; ?></a></p>
      <div class="row">
        <div class="col-sm-3">
          <div class="btn-group hidden-xs">
            <button type="button" id="list-view" class="btn btn-default" data-toggle="tooltip" title="<?php echo $button_list; ?>"><i class="fa fa-th-list"></i></button>
            <button type="button" id="grid-view" class="btn btn-default" data-toggle="tooltip" title="<?php echo $button_grid; ?>"><i class="fa fa-th"></i></button>
          </div>
        </div>
        <div class="col-sm-1 col-sm-offset-2 text-right">
          <label class="control-label" for="input-sort"><?php echo $text_sort; ?></label>
        </div>
        <div class="col-sm-3 text-right">
          <select id="input-sort" class="form-control col-sm-3" onchange="location = this.value;">
            <?php foreach ($sorts as $sorts) { ?>
            <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
            <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
            <?php } ?>
            <?php } ?>
          </select>
        </div>
        <div class="col-sm-1 text-right">
          <label class="control-label" for="input-limit"><?php echo $text_limit; ?></label>
        </div>
        <div class="col-sm-2 text-right">
          <select id="input-limit" class="form-control" onchange="location = this.value;">
            <?php foreach ($limits as $limits) { ?>
            <?php if ($limits['value'] == $limit) { ?>
            <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
            <?php } ?>
            <?php } ?>
          </select>
        </div>
      </div>
      <br />
      <div class="row">
        <?php foreach ($products as $product) { ?>
            <?php $this->partial('product_item', array('product' => $product, 'button_cart' => $button_cart, 'text_tax' => $text_tax, 'button_wishlist' => $button_wishlist, 'button_compare' => $button_compare));?>

        <?php } ?>
      </div>
      <div class="row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>*/?>
<?php echo $footer; ?>