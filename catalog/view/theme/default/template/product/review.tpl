<?php if ($reviews) { ?>
<div class="product-reviews__scroll">
    <div>
        <?php foreach ($reviews as $review) { ?>
            <div class="product-reviews__item">
                <div class="product-reviews__meta flex-container flex-left flex-bottom">
                    <div class="product-reviews__author"><?php echo $review['author']; ?></div>
                    <div class="product-reviews__date"><?php echo $review['date_added']; ?></div>
                </div>
                <div class="product-reviews__rating flex-container flex-left flex-top">
                    <?php for($i=0;$i<$review['rating'];$i++){ ?>
                        <i class="icon-star-full"></i>
                    <?php } ?>
                </div>
                <div class="product-reviews__text"><?php echo $review['text']; ?></div>
            </div>
        <?php } ?>
    </div>  
</div>
<?php } else { ?>
<p><?php echo $text_no_reviews; ?></p>
<?php } ?>
<div class="product-reviews__share text-center">
    <a href="javascript:void(0)" data-toggle="modal" data-target="#review__modal" class="btn btn-default btn-medium"><span>Поделиться опытом</span></a>
</div>
<?php if($reviews){ ?>
<script>
    new IScroll( '.product-reviews__scroll', {
        bounce: false,
        scrollbars: true,
        mouseWheel: true,
        interactiveScrollbars: true,
        mouseWheelSpeed: 120
    });
</script>
<?php } ?>