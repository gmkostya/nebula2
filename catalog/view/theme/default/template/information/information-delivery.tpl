<?php echo $header; ?>
<main class="delivery">
    <div class="breadcrumbs container">
        <?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>
    </div>
    <div class="container">
        <div class="page-heading-simple">
            <h1 class="page-heading-simple__title"><?php echo $heading_title; ?></h1>
            <?php /*<div class="page-heading-simple__subtitle">Самое свежее и интересное</div>*/?>
        </div>
        <div class="delivery__items flex-container flex-center">
            <div class="delivery__item">
                <div class="delivery__title"> Мы упаковываем только в закрытую анонимную упаковку!</div>
                <div class="delivery__desc"><img src="/image/catalog/courier.png" alt="Мы упаковываем только в закрытую анонимную упаковку!"> </div>


            </div>

        </div>
        <div class="delivery__items flex-container flex-between flex-top">
            <div class="delivery__item">
                <div class="delivery__ico flex-container flex-center flex-middle"><i class="icon-delivery-1"></i></div>
                <div class="delivery__title"><?= $text_delivery_title1 ?></div>
                <div class="delivery__subtitle"><?= $text_delivery_subtitle1 ?></div>
                <div class="delivery__desc"><?= $text_delivery_desc1 ?></div>
            </div>
            <div class="delivery__item">
                <div class="delivery__ico flex-container flex-center flex-middle"><i class="icon-delivery-2"></i></div>
                <div class="delivery__title"><?= $text_delivery_title2 ?></div>
                <div class="delivery__subtitle"><?= $text_delivery_subtitle2 ?></div>
                <div class="delivery__desc"><?= $text_delivery_desc2 ?></div>
            </div>
            <div class="delivery__item">
                <div class="delivery__ico flex-container flex-center flex-middle"><i class="icon-delivery-3"></i></div>
                <div class="delivery__title"><?= $text_delivery_title3 ?></div>
                <div class="delivery__subtitle"><?= $text_delivery_subtitle3 ?></div>
                <div class="delivery__desc"><?= $text_delivery_desc3 ?></div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="delivery__note">
            <div class="delivery__note-wrap flex-container flex-between flex-middle">
                <div class="delivery__note-icon"><i class="icon-info-delivery"></i></div>
                <div class="delivery__note-desc"><?= $text_note_desc ?></div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="delivery__items flex-container flex-center flex-top">
            <div class="delivery__item">
                <div class="delivery__ico flex-container flex-center flex-middle"><i class="icon-puy-1"></i></div>
                <div class="delivery__title"><?= $text_delivery_title4 ?></div>
                <div class="delivery__desc"><?= $text_delivery_desc4 ?></div>
            </div>
            <div class="delivery__item">
                <div class="delivery__ico flex-container flex-center flex-middle"><i class="icon-puy-2"></i></div>
                <div class="delivery__title"><?= $text_delivery_title5 ?></div>
                <div class="delivery__desc"><?= $text_delivery_desc5 ?></div>
            </div>
        </div>
    </div>
</main>
<?php echo $column_right; ?>
<?php echo $content_bottom; ?>
<?php echo $footer; ?>
