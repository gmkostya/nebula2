<?php echo $header; ?>
<main class="gallery">
    <div class="breadcrumbs container">
        <?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>
    </div>
    <div class="container">
        <div class="page-heading-simple">
            <h1 class="page-heading-simple__title"><?= $heading_title ?></h1>
            <div class="page-heading-simple__subtitle">Есть на что посмотреть</div>
        </div>
        <?php if($images){ ?>
            <div class="gallery-grid">
                <?php foreach ($images as $image){ ?>
                    <a href="<?= ($image['video']) ? $image['video'] : $image['thumb'] ?>" data-fancybox="gallery" class="gallery-grid__item<?= ($image['video']) ? ' video' : ''?>">
                        <img src="<?= ($image['video']) ? $image['thumb'] : $image['image'] ?>" title="<?= $image['title'] ?>" alt="<?= ($image['alt']) ? $image['alt'] : $image['title'] ?>">
                    </a>
                <?php } ?>
            </div>
            <?php echo $pagination; ?>
        <?php }else{ ?>
            <p><?= $text_empty; ?></p>
        <?php } ?>
    </div>
</main>
<?php echo $content_bottom; ?>
<?php echo $footer; ?>