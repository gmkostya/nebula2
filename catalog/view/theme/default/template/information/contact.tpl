<?php echo $header; ?>
<main class="contacts">
    <div class="page-heading" style="background-image: url('/catalog/view/theme/default/html/build/img/contacts-heading-bg.jpg');">
        <div class="breadcrumbs container">
            <?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>
        </div>
        <div class="container">
            <h1 class="page-heading__title"><?php echo $heading_title; ?></h1>
            <div class="page-heading__subtitle"><?= $text_title ?></div>

            <div class="contacts__items flex-container flex-between flex-middle">
                <div class="contacts__item flex-container flex-left flex-middle">
                    <i class="icon-phone-kontaktu"></i>
                    <a href="tel:<?php echo str_replace(array(' ','(',')','-'),'',$telephone);?>"><?php echo $telephone ?></a>
                </div>
                <div class="contacts__item flex-container flex-left flex-middle">
                    <i class="icon-mail-kontaktu"></i>
                    <a href="mailto:<?php echo $config_email; ?>"><?php echo $config_email; ?></a>
                </div>
                <div class="contacts__item flex-container flex-left flex-middle">
                    <i class="icon-addres-kontaktu"></i>
                    <span>
                        <?= html_entity_decode($open) ?>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <?php echo $column_left; ?>
    <?php echo $content_top; ?>
    <?php echo $form_contacts ?>
    <div id="map" class="map"></div>
    <?php echo $column_right; ?>
    <?php echo $content_bottom; ?>
</main>
<script>
    function initMap() {
        <?php $ll = explode(',',$geocode) ?>
        // var center = {lat: 55.704437, lng: 37.641593};
        var center = {lat: <?= $ll[0] ?>, lng: <?= $ll[1] ?>};
        var map = new google.maps.Map(document.getElementById('map'), {
            center: center,
            zoom: 11,
            styles: [
                {
                    "featureType": "water",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#d3d3d3"
                        }
                    ]
                },
                {
                    "featureType": "transit",
                    "stylers": [
                        {
                            "color": "#808080"
                        },
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "color": "#b3b3b3"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#ffffff"
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "color": "#ffffff"
                        },
                        {
                            "weight": 1.8
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "color": "#d7d7d7"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "color": "#ebebeb"
                        }
                    ]
                },
                {
                    "featureType": "administrative",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#a7a7a7"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#ffffff"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#ffffff"
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "color": "#efefef"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#696969"
                        }
                    ]
                },
                {
                    "featureType": "administrative",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "color": "#737373"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "labels",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "color": "#d6d6d6"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {},
                {
                    "featureType": "poi",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#dadada"
                        }
                    ]
                },
                {
                    "featureType": "landscape.man_made",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "color": "#cccccc"
                        },
                        {
                            "visibility": "on"
                        }
                    ]
                },
            ]
        });
        var marker = new google.maps.Marker({
            position:  center,
            map: map
        });
    }
</script>
<?php echo $footer; ?>
