<?php echo $header; ?>
<main class="news-single">
    <div class="breadcrumbs container">
        <?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>
    </div>
    <div class="container">
        <h1 class="news-single__title"><?php echo $heading_title; ?></h1>
        <?php echo $description; ?>
    </div>
    <?php //echo $content_top; ?>
    <?php //echo $content_bottom; ?>
    <?php //echo $column_right; ?>
</main>
<?php echo $footer; ?>