<?php echo $header; ?>
<main class="about section-dark">
    <div class="breadcrumbs container">
        <?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>
    </div>
    <div class="container">
        <div class="page-heading-simple">
            <h1 class="page-heading-simple__title"><?php echo $heading_title; ?></h1>
            <div class="page-heading-simple__subtitle"><?= $subtitle_about ?></div>
        </div>
        <div class="about__desc">
            <?= $description ?>
        </div>
        <a href="<?= $url_shop ?>" class="btn btn-default"><span><?= $in_catalog ?></span></a>
    </div>
</main>
<?php echo $column_right; ?>
<?php echo $content_bottom; ?>
<?php echo $footer; ?>