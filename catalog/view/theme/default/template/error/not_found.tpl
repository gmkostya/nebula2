<?php echo $header; ?>
<main class="not-found">
    <div class="not-found__content">
        <div class="not-found__pct">
            <img src="/catalog/view/theme/default/html/build/img/not-found.png">
        </div>
        <div class="not-found__title"><?= $text_subtitle ?></div>
        <div class="not-found__desc">
            <?= $text_error ?>
        </div>
        <div class="text-center">
            <a href="<?php echo $continue; ?>" class="btn btn-default"><span>на главную</span></a>
        </div>
    </div>
    <?php echo $column_left; ?>
    <?php echo $content_top; ?>
    <?php echo $content_bottom; ?>
    <?php echo $column_right; ?>
</main>
<?php /*
<div class="container">
          <ul  class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
            <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
              <?php if($i+1<count($breadcrumbs)) { ?>
              <a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
                <span itemprop="name"><?php echo $breadcrumb['text']; ?></span></a>
              <?php } else { ?><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
              <?php } ?>
            </li>
            <?php } ?>
          </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <p><?php echo $text_error; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>*/?>
<?php echo $footer; ?>