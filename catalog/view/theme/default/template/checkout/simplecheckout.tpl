<?php if (!$ajax && !$popup && !$as_module) { ?>
<?php
$simple_page = 'simplecheckout';
$heading_title .= $display_weight ? '&nbsp;(<span id="weight">'. $weight . '</span>)' : '';
include $simple_header;
?>
    <div class="page-heading" style="background-image: url('/catalog/view/theme/default/html/build/img/checkout-main-bg.jpg');">
      <div class="breadcrumbs container">
        <?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>
      </div>
      <div class="container">
          <h1 class="page-heading__title"><?php echo $heading_title; ?></h1>
          <div class="page-heading__subtitle">Еще немножко потерпи...</div>
      </div>
  </div>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="profile-wrap container">
    <?php echo $column_right; ?>
    <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div class="profile-body"><?php echo $content_top; ?>
<style>
    <?php if ($left_column_width) { ?>
        .simplecheckout-left-column {
            width: <?php echo $left_column_width ?>%;
        }
        @media only screen and (max-width:768px) {
            .simplecheckout-left-column {
                width: 100%;
            }
        }
    <?php } ?>
    <?php if ($right_column_width) { ?>
        .simplecheckout-right-column {
            width: <?php echo $right_column_width ?>%;
        }
        @media only screen and (max-width:768px) {
            .simplecheckout-right-column {
                width: 100%;
            }
        }
    <?php } ?>
    <?php if ($customer_with_payment_address) { ?>
        #simplecheckout_customer {
            margin-bottom: 0;
        }
        #simplecheckout_customer .simplecheckout-block-content {
            border-bottom-width: 0;
            padding-bottom: 0;
        }
        #simplecheckout_payment_address div.checkout-heading {
            display: none;
        }
        #simplecheckout_payment_address .simplecheckout-block-content {
            border-top-width: 0;
            padding-top: 0;
        }
    <?php } ?>
    <?php if ($customer_with_shipping_address) { ?>
        #simplecheckout_customer {
            margin-bottom: 0;
        }
        #simplecheckout_customer .simplecheckout-block-content {
            border-bottom-width: 0;
            padding-bottom: 0;
        }
        #simplecheckout_shipping_address div.checkout-heading {
            display: none;
        }
        #simplecheckout_shipping_address .simplecheckout-block-content {
            border-top-width: 0;
            padding-top: 0;
        }
    <?php } ?>
</style>
<div class="simple-content">
<?php } ?>
    <?php if (!$ajax || ($ajax && $popup)) { ?>
    <script type="text/javascript" async>
    // window.onload = function() {
    (function($) {
    <?php if (!$popup && !$ajax) { ?>
        $(function(){
    <?php } ?>
            if (typeof Simplecheckout === "function") {
                var simplecheckout_<?php echo $group ?> = new Simplecheckout({
                    mainRoute: "checkout/simplecheckout",
                    additionalParams: "<?php echo $additional_params ?>",
                    additionalPath: "<?php echo $additional_path ?>",
                    mainUrl: "<?php echo $action; ?>",
                    mainContainer: "#simplecheckout_form_<?php echo $group ?>",
                    currentTheme: "<?php echo $current_theme ?>",
                    loginBoxBefore: "<?php echo $login_type == 'flat' ? '#simplecheckout_customer .simplecheckout-block-content:first' : '' ?>",
                    displayProceedText: <?php echo $display_proceed_text ? 1 : 0 ?>,
                    scrollToError: <?php echo $scroll_to_error ? 1 : 0 ?>,
                    scrollToPaymentForm: <?php echo $scroll_to_payment_form ? 1 : 0 ?>,
                    useAutocomplete: <?php echo $use_autocomplete ? 1 : 0 ?>,
                    useGoogleApi: <?php echo $use_google_api ? 1 : 0 ?>,
                    popup: <?php echo ($popup || $as_module) ? 1 : 0 ?>,
                    agreementCheckboxStep: <?php echo $agreement_checkbox_step != '' ? $agreement_checkbox_step : '\'\''; ?>,
                    enableAutoReloaingOfPaymentFrom: <?php echo $enable_reloading_of_payment_form ? 1 : 0 ?>,
                    javascriptCallback: function() {$('#cart').load('index.php?route=common/cart/info #cart > *');var cc = $('#cart-total').text(); if(cc==0){$('#cart').removeClass('active');}<?php //echo $javascript_callback ?>}
                });

                simplecheckout_<?php echo $group ?>.init();

                $(document).ajaxComplete(function(e, xhr, settings) {
                    if (settings.url.indexOf("route=module/cart&remove") > 0 || (settings.url.indexOf("route=module/cart") > 0 && settings.type == "POST") || settings.url.indexOf("route=checkout/cart/add") > 0 || settings.url.indexOf("route=checkout/cart/remove") > 0) {
                        window.resetSimpleQuantity = true;
                        simplecheckout_<?php echo $group ?>.reloadAll();
                    }
                });

                $(document).ajaxSend(function(e, xhr, settings) {
                    if (settings.url.indexOf("checkout/simplecheckout&group") > 0 && typeof window.resetSimpleQuantity !== "undefined" && window.resetSimpleQuantity) {
                        settings.data = settings.data.replace(/quantity.+?&/g,"")
                        window.resetSimpleQuantity = false;
                    }
                });
                if ($('#simplecheckout_form_0 [name=notifications]:checked')) {
                    $('#simplecheckout_form_0 #simplecheckout_button_confirm').attr('disabled', 'disabled');
                    $('#simplecheckout_form_0 #button-confirm').attr('disabled', 'disabled');
                    console.log('qweqweqweqwe');
                } else {
                    $('#simplecheckout_form_0 #simplecheckout_button_confirm').removeAttr('disabled');
                    $('#simplecheckout_form_0 #button-confirm').removeAttr('disabled');
                    console.log('asdsdasd');
                }
                $('#simplecheckout_form_0 [name=notifications]').on('change', function () {
                    if (this.checked) {
                        $('#simplecheckout_form_0 #simplecheckout_button_confirm').prop('disabled', false);
                        $('#simplecheckout_form_0 #button-confirm').prop('disabled', false);
                    } else {
                        $('#simplecheckout_form_0 #simplecheckout_button_confirm').prop('disabled', true);
                        $('#simplecheckout_form_0 #button-confirm').prop('disabled', true);
                    }
                });
            }
    <?php if (!$popup && !$ajax) { ?>
        });
    <?php } ?>
    })(jQuery || $);
    // };
    </script>
    <?php } ?>
    <?php $buttons = '';
    $buttons .= '<div id="simplecheckout_bottom" style="width:100%;height:1px;clear:both;"></div>';
    $buttons .= '<div class="simplecheckout-proceed-payment" id="simplecheckout_proceed_payment" style="display:none;">' . $text_proceed_payment . '</div>';
    if ($display_agreement_checkbox) {
        $buttons .= '<div class="simplecheckout-warning-block" id="agreement_warning"'; if ($display_error && $has_error) { $buttons .= 'data-error="true" '; } else { $buttons .= 'style="display:none;"'; } $buttons .= '>' . $error_warning_agreement . '</div>';
    }
    $buttons .= '<div class="simplecheckout-block check-wrap">';
    $buttons .=    '<input type="checkbox" name="notifications" id="simplecheckout_notifications">';
    $buttons .=    '<label for="simplecheckout_notifications">' . $text_notifications . '</label>';
    $buttons .= '</div>';
    if (isset($block_order) && !$block_order) {
        $buttons .= ' <div class="simplecheckout-button-block buttons" id="buttons">';
        $buttons .= '    <div class="">';
            if ($display_agreement_checkbox) {
                $buttons .= '<span id="agreement_checkbox"><label><input type="checkbox" name="agreement" value="1"'; if ($agreement == 1) { $buttons .= 'checked="checked"'; } $buttons .= ' />' . $text_agreement . '</label>&nbsp;</span>';
            }
            if ($steps_count > 1) {
                $buttons .= '<a class="button btn-default button_oc btn" data-onclick="nextStep" id="simplecheckout_button_next"><span>' . $button_next .'</span></a>';
            }
            $buttons .= '<button type="button" class="button btn-default button_oc btn" data-onclick="createOrder" id="simplecheckout_button_confirm"><span>' . $button_order . '</span></button>';
        $buttons .= '</div>';
        $buttons .= '<div class="simplecheckout-button-left">';
            if ($display_back_button) {
                $buttons .= '<a class="button btn-default button_oc btn" data-onclick="backHistory" id="simplecheckout_button_back"><span>' . $button_back . '</span></a>';
            }
            if ($steps_count > 1) {
                $buttons .= '<a class="button btn-default button_oc btn" data-onclick="previousStep" id="simplecheckout_button_prev"><span>' . $button_prev . '</span></a>';
            }
        $buttons .= '    </div>';
        $buttons .= '</div>';
    }?>
    <div id="simplecheckout_form_<?php echo $group ?>" <?php echo $display_error && $has_error ? 'data-error="true"' : '' ?> <?php echo $logged ? 'data-logged="true"' : '' ?>>
        <div class="simplecheckout">
            <?php if (!$cart_empty) { ?>
                <?php if ($steps_count > 1) { ?>
                    <div id="simplecheckout_step_menu">
                    <?php for ($i=1;$i<=$steps_count;$i++) { ?><span class="simple-step" data-onclick="gotoStep" data-step="<?php echo $i; ?>"><?php echo $step_names[$i-1] ?></span><?php if ($i < $steps_count) { ?><span class="simple-step-delimiter" data-step="<?php echo $i+1; ?>"><img src="<?php echo $additional_path ?>catalog/view/image/next_gray.png"></span><?php } ?><?php } ?>
                    </div>
                <?php } ?>
                <?php
                    $replace = array(
                        '{three_column}'     => '<div class="simplecheckout-three-column">',
                        '{/three_column}'    => '</div>',
                        '{left_column}'      => '<div class="simplecheckout-left-column">',
                        '{/left_column}'     => '</div>',
                        '{right_column}'     => '<div class="simplecheckout-right-column">',
                        '{/right_column}'    => '</div>',
                        '{step}'             => '<div class="simplecheckout-step">',
                        '{/step}'            => '</div>',
                        '{customer}'         => $simple_blocks['customer'],
                        '{payment_address}'  => $simple_blocks['payment_address'],
                        '{shipping_address}' => $simple_blocks['shipping_address'],
                        '{cart}'             => $simple_blocks['cart'],
                        '{shipping}'         => $simple_blocks['shipping'],
                        '{payment}'          => $simple_blocks['payment'],
                        '{agreement}'        => $simple_blocks['agreement'],
                        '{help}'             => $simple_blocks['help'],
                        '{summary}'          => $simple_blocks['summary'],
                        '{comment}'          => $simple_blocks['comment'],
                        '{buttons}'          => $buttons,
                        '{payment_form}'     => '<div class="simplecheckout-block" id="simplecheckout_payment_form">'.$simple_blocks['payment_form'].'</div>'
                    );

                    $find = array(
                        '{three_column}',
                        '{/three_column}',
                        '{left_column}',
                        '{/left_column}',
                        '{right_column}',
                        '{/right_column}',
                        '{step}',
                        '{/step}',
                        '{customer}',
                        '{payment_address}',
                        '{shipping_address}',
                        '{cart}',
                        '{shipping}',
                        '{payment}',
                        '{agreement}',
                        '{help}',
                        '{summary}',
                        '{comment}',
                        '{buttons}',
                        '{payment_form}'
                    );

                    foreach ($simple_blocks as $key => $value) {
                        $key_clear = $key;
                        $key = '{'.$key.'}';
                        if (!array_key_exists($key, $replace)) {
                            $find[] = $key;
                            $replace[$key] = '<div class="simplecheckout-block" id="'.$key_clear.'">'.$value.'</div>';
                        }
                    }

                    echo trim(str_replace($find, $replace, $simple_template));
                ?>
                <?php /*
                <div id="simplecheckout_bottom" style="width:100%;height:1px;clear:both;"></div>
                <div class="simplecheckout-proceed-payment" id="simplecheckout_proceed_payment" style="display:none;"><?php echo $text_proceed_payment ?></div>
                <?php if ($display_agreement_checkbox) { ?>
                    <div class="simplecheckout-warning-block" id="agreement_warning" <?php if ($display_error && $has_error) { ?>data-error="true"<?php } else { ?>style="display:none;"<?php } ?>><?php echo $error_warning_agreement ?></div>
                <?php } ?>
                <?php if (!$block_order) { ?>
                    <div class="simplecheckout-button-block buttons" id="buttons">
                        <div class="simplecheckout-button-right">
                            <?php if ($display_agreement_checkbox) { ?><span id="agreement_checkbox"><label><input type="checkbox" name="agreement" value="1" <?php if ($agreement == 1) { ?>checked="checked"<?php } ?> /><?php echo $text_agreement; ?></label>&nbsp;</span><?php } ?>
                            <?php if ($steps_count > 1) { ?>
                            <a class="button btn-default button_oc btn" data-onclick="nextStep" id="simplecheckout_button_next"><span><?php echo $button_next; ?></span></a>
                            <?php } ?>
                            <a class="button btn-default button_oc btn" data-onclick="createOrder" id="simplecheckout_button_confirm"><span><?php echo $button_order; ?></span></a>
                        </div>
                        <div class="simplecheckout-button-left">
                            <?php if ($display_back_button) { ?>
                            <a class="button btn-default button_oc btn" data-onclick="backHistory" id="simplecheckout_button_back"><span><?php echo $button_back; ?></span></a>
                            <?php } ?>
                            <?php if ($steps_count > 1) { ?>
                            <a class="button btn-default button_oc btn" data-onclick="previousStep" id="simplecheckout_button_prev"><span><?php echo $button_prev; ?></span></a>
                            <?php } ?>
                        </div>
                    </div>
                <?php }*/ ?>
            <?php } else { ?>
                <div class="content"><?php echo $text_error; ?></div>
                <div style="display:none;" id="simplecheckout_cart_total"><?php echo $cart_total ?></div>
                <?php if ($display_weight) { ?>
                    <div style="display:none;" id="simplecheckout_cart_weight"><?php echo $weight ?></div>
                <?php } ?>
                <div class="simplecheckout-button-block buttons">
                    <div class="simplecheckout-button-right right"><a href="<?php echo $continue; ?>" class="button btn-default button_oc btn"><span><?php echo $button_continue; ?></span></a></div>
                </div>
            <?php } ?>
        </div>
    </div>
<?php if (!$ajax && !$popup && !$as_module) { ?>
</div>
<?php include $simple_footer ?>
<?php } ?>