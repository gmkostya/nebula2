<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<!--[if lte IE 9]>
<link rel="stylesheet" href="/reject/reject.css" media="all" />
<script type="text/javascript" src="/reject/reject.min.js"></script>
<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="format-detection" content="telephone=no">
<meta name="theme-color" content="#fff">
<meta name="yandex-verification" content="35e8360be998bdcc" />
<title><?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($meta) { ?>
<meta name="robots" content="<?php echo $meta; ?>"/>
<?php } ?>
<?php if (isset($robots) && $robots) { ?>
<meta name="robots" content="<?php echo $robots; ?>"/>
<?php } ?>
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<meta property="og:title" content="<?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
<meta property="og:type" content="website" />
<meta property="og:url" content="<?php echo $og_url; ?>" />
<?php if ($og_image) { ?>
<meta property="og:image" content="<?php echo $og_image; ?>" />
<?php } else { ?>
<meta property="og:image" content="<?php echo $logo; ?>" />
<?php } ?>
<meta property="og:site_name" content="<?php echo $name; ?>" />

<meta property="url" content="<?php echo $og_url; ?>" />
<meta property="title" content="<?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
<?php if ($og_image) { ?>
<meta property="image" content="<?php echo $og_image; ?>" />
<?php } else { ?>
<meta property="image" content="<?php echo $logo; ?>" />
<?php } ?>

<?php /*
<meta itemprop="url" content="<?php echo $og_url; ?>">
<meta itemprop="name" content="<?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
<?php if ($og_image) { ?>
<meta itemprop="image" content="<?php echo $og_image; ?>">
<?php } else { ?>
<meta itemprop="image" content="<?php echo $logo; ?>">
<?php }*/ ?>

<?php if($gtm) { ?>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?
id='+i+dl;f.parentNode.insertBefore(j,f);
 })(window,document,'script','dataLayer','<?php echo $gtm; ?>');</script>
<!-- End Google Tag Manager -->
<?php } ?>
<?php /* посилань на альтернативні мовні версії сайту в шапці не потрібно, вони повині бути в сайтмапі лише, але покищо залишив*/?>
<?php /*foreach ($alter_lang as $lang=>$href) { ?>
    <link href="<?php echo $href; ?>" hreflang="<?php echo $lang; ?>" rel="alternate" />
<?php }*/ ?>
<link rel="shortcut icon" href="/catalog/view/theme/default/html/build/img/favicon.png" type="image/x-icon">
<link rel="icon" href="/catalog/view/theme/default/html/build/img/favicon.png" type="image/x-icon">
<link rel="stylesheet" media="all" href="/catalog/view/theme/default/html/build/css/style.css" >
<script type="application/ld+json">
{
    "@context": "http://schema.org",
    "@type": "Organization",
    "url": "<?php echo $base; ?>",
    "description" : "<?php echo $name; ?>",
    "logo": "<?php echo $logo; ?>",
    "address": {
        "@type": "PostalAddress",
        "addressCountry": "<?= $config_country ?>",
        "addressLocality": "<?= $config_city ?>",
        "postalCode": "<?= $config_postcode ?>",
        "streetAddress": "<?= $config_address ?>",
        "email": "<?= $email; ?>",
        "telephone": "<?= $telephone; ?>",
        "name": "<?php echo $title; ?>"
    },
    <?php if($social_count){ ?>
    "sameAs" : [
        <?php $k=0; foreach($social as $soc){ ?>
          "<?= $soc; ?>"<?= ($k<$social_count) ? ',' : '' ?>
        <?php $k++; } ?>
      ]
    <?php } ?>
}
</script>
<?php foreach ($links as $link) { ?>
    <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php /*foreach ($styles as $style) { ?>
    <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="/catalog/view/theme/default/html/build/js/app.js?v=0.1"></script>
<?php foreach ($scripts as $script) { ?>
    <script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php }*/ ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>
<script src="//code.jivosite.com/widget.js" jv-id="PGHjaN1neN" async></script>

<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(54610891, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/54610891" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

</head>
<body class="<?php echo $class; ?>">
<?php if($gtm) { ?>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo $gtm; ?>>"
                      height="0" width="0"
                      style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
<?php } ?>
    <header class="header">
        <div class="container">
            <div class="header__main flex-container flex-between flex-middle">
                <div class="header__logo-nav flex-container flex-left flex-middle">
                    <?php if ($home == $og_url) { ?>
                        <div class="header__logo">
                            <div class="header__logo-pct">
                                <i class="icon-logo"></i>
                            </div>
                            <div class="header__logo-text">
                                <div class="header__logo-name">NEBULATOY</div>
                                <div class="header__logo-desc">итнернет-магазин для взрослых</div>
                            </div>
                        </div>
                    <?php }else{ ?>
                        <a href="<?= $home ?>" class="header__logo">
                            <div class="header__logo-pct">
                                <i class="icon-logo"></i>
                            </div>
                            <div class="header__logo-text">
                                <div class="header__logo-name">NEBULATOY</div>
                                <div class="header__logo-desc">итнернет-магазин для взрослых</div>
                            </div>
                        </a>
                    <?php } ?>

                    <div class="header__nav-toggle flex-container">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                    <div class="header__nav-main">
                        <div class="container flex-container flex-left flex-top">
                            <?php if ($categories) { ?>
                                <?php foreach ($categories as $category) { ?>
                                    <ul class="flex-container flex-between flex-top">
                                            <li class="back"><a href="javascript:void(0)">Назад</a></li>
                                        <?php foreach ($category['children'] as $children) { ?>
                                            <li<?= ($og_url==$category['href']) ? ' class="active"' : ''?>><a href="<?= $children['href'] ?>"><?= $children['name'] ?></a></li>
                                        <?php } ?>
                                    </ul>
                                <?php } ?>
                            <?php } ?>
                            <ul>
                                <li><a href="<?= $gallery ?>"><?= $text_gallery ?></a></li>
                                <li><a href="<?= $informations[4]['href'] ?>"><?= $informations[4]['title'] ?></a></li>
                                <li><a href="<?= $review ?>"><?= $text_review ?></a></li>
                                <li><a href="<?= $news ?>"><?= $text_news ?></a></li>
                                <li><a href="<?= $informations[6]['href'] ?>"><?= $informations[6]['title'] ?></a></li>
                                <li><a href="<?= $contact ?>"><?= $text_contact ?></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="header__contact-profile flex-container flex-right flex-top">
                    <div class="header__contact">
                        <a class="header__phone" href="tel:<?php echo str_replace(array(' ','(',')','-'),'',$telephone);?>"><?= $telephone ?></a><br>
                        <span class="header__callback" data-toggle="modal" data-target="#callbackModal">Заказать звонок</span>
                    </div>
                    <div class="header__profile">
                        <?php if($is_logged){ ?>
                            <a href="<?= $account_edit?>" class="btn btn-black">МОЙ Nebulatoy</a>
                        <?php }else{ ?>
                            <span class="btn btn-black" data-toggle="modal" data-target="#profileModal">Авторизация</span>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="header__internal flex-container flex-between flex-middle">
                <div class="header__logo-nav flex-container flex-left flex-middle">
                    <div class="header__logo">
                        <div class="header__logo-pct">
                            <i class="icon-logo"></i>
                        </div>
                        <div class="header__logo-text">
                            <div class="header__logo-name">NEBULATOY</div>
                            <div class="header__logo-desc">итнернет-магазин для взрослых</div>
                        </div>
                    </div>
                </div>
                <div class="header__nav-toggle flex-container">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <div class="header__nav">
                    <ul>
                        <?php if ($categories) { ?>
                            <?php foreach ($categories as $category) { ?>
                                <li class="has-child<?= ($og_url==$category['href']) ? ' active' : ''?>">
                                    <a href="<?= $category['href']; ?>"><?php echo $category['name']; ?></a>
                                    <?php if ($category['children']) { ?>
                                    <?php $count_children = count($category['children']); ?>
                                    <div class="drop-down">
                                        <ul>
                                            <li class="back"><a href="javascript:void(0)">Назад</a></li>
                                            <?php for($i=0;$i<$count_children;$i = $i+2){ ?>
                                                <li<?= ($og_url==$category['children'][$i]['href']) ? ' class="active"' : ''?>><a href="<?= $category['children'][$i]['href']; ?>"><?= $category['children'][$i]['name']; ?></a></li>
                                                <?php if(isset($category['children'][$i+1])){?>
                                                    <li<?= ($og_url==$category['children'][$i+1]['href']) ? ' class="active"' : ''?>><a href="<?= $category['children'][$i+1]['href']; ?>"><?= $category['children'][$i+1]['name']; ?></a></li>
                                                <?php } ?>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                    <?php } ?>
                                </li>
                            <?php } ?>
                        <?php } ?>
                        <li><a href="<?= $gallery ?>"><?= $text_gallery ?></a></li>
                        <li><a href="<?= $informations[4]['href'] ?>"><?= $informations[4]['title'] ?></a></li>
                        <li><a href="<?= $review ?>"><?= $text_review ?></a></li>
                        <li><a href="<?= $news ?>"><?= $text_news ?></a></li>
                        <li><a href="<?= $informations[6]['href'] ?>"><?= $informations[6]['title'] ?></a></li>
                        <li><a href="<?= $contact ?>"><?= $text_contact ?></a></li>
                    </ul>
                </div>
                <div class="header__icons flex-container flex-right flex-middle">
                    <?php echo $search; ?>
                    <?php if(!$total_compare){ ?>
                        <a href="<?= $compare ?>" id="compare-total"><i class="icon-libra"></i><span></span></a>
                    <?php }else{ ?>
                        <a href="<?= $compare ?>" id="compare-total" class="active"><i class="icon-libra"></i><span><?= $total_compare ?></span></a>
                    <?php } ?>
                    <?php if(!$total_wishlist){ ?>
                        <a href="<?= $wishlist ?>" id="wishlist-total"><i class="icon-wishlist"></i><span></span></a>
                    <?php }else{ ?>
                        <a href="<?= $wishlist ?>" id="wishlist-total" class="active"><i class="icon-wishlist"></i><span><?= $total_wishlist ?></span></a>
                    <?php } ?>
                    <?= $cart ?>
                    <?php if($is_logged){ ?>
                        <a href="<?= $account_edit ?>"><i class="icon-log-in"></i></a>
                    <?php }else{ ?>
                        <a href="javascript:void(0)" data-toggle="modal" data-target="#profileModal"><i class="icon-log-in"></i></a>
                    <?php } ?>
                    <a href="javascript:void(0)" data-toggle="modal" data-target="#callbackModal"><i class="icon-call-back"></i></a>
                    <div class="header__alert" id="header_alert">
                        <div class="header__alert-message"></div>
                        <a href="javascript:void(0)" class="header__alert-button"><span></span></a>
                    </div>
                </div>
            </div>
        </div>
    </header>
