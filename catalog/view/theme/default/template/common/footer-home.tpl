<footer class="footer">
    <div class="container">
        <div class="footer__grid">
            <?php if ($categories) { ?>
                <?php $key = 0; ?>
                <?php foreach ($categories as $category) { ?>
                    <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / 3)) as $children) { ?>
                        <div class="footer__col">
                            <?php if(!$key){ ?>
                                <a href="<?= $category['href']; ?>" class="footer__title"><?= $text_shop ?></a>
                            <?php }else{ ?>
                                <a href="#" class="footer__title"></a>
                            <?php } ?>
                            <ul>
                                <?php foreach ($children as $child) { ?>
                                    <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                                <?php } ?>
                            </ul>
                        </div>
                    <?php $key++; } ?>
                <?php } ?>
            <?php } ?>
            <div class="footer__col titles-list">
                <a href="<?= $gallery ?>" class="footer__title"><?= $text_gallery ?></a>
                <a href="<?= $informations[4]['href'] ?>" class="footer__title"><?= $informations[4]['title'] ?></a>
                <a href="<?= $review ?>" class="footer__title"><?= $text_review ?></a>
                <a href="<?= $news ?>" class="footer__title"><?= $text_news ?></a>
                <a href="<?= $informations[6]['href'] ?>" class="footer__title"><?= $informations[6]['title'] ?></a>
                <a href="<?= $contact ?>" class="footer__title"><?= $text_contact ?></a>
            </div>
            <div class="footer__col">
                <?php if($is_logged){ ?>
                    <a href="<?= $account_edit ?>" class="footer__title"><?= $text_account ?></a>
                    <ul>
                        <li><a href="<?= $account_edit ?>"><?= $text_edit?></a></li>
                        <li><a href="<?= $wishlist ?>"><?= $text_wishlist ?></a></li>
                        <li><a href="<?= $order ?>"><?= $text_order ?></a></li>
                        <li><a href="<?= $logout ?>"><?= $text_exit ?></a></li>
                    </ul>
                <?php }else{ ?>
                    <a href="javascript:void(0)" data-toggle="modal" data-target="#profileModal" class="footer__title"><?= $text_account ?></a>
                <?php } ?>
            </div>
            <div class="footer__col">
                <a href="<?= $contact ?>" class="footer__title"><?= $text_contact ?></a>
                <a href="tel:<?php echo str_replace(array(' ','(',')','-'),'',$telephone);?>" class="footer__tel"><?php echo $telephone ?></a>
                <div class="footer__address"><?= $config_city . ', ' . $config_address ?></div>
                <a href="mailto:<?php echo $email; ?>" class="footer__mail"><?php echo $email; ?></a>
            </div>
        </div>
        <div class="footer__grid-mob">
            <div>
                <a href="javascript:void(0)" class="footer__title">Магазин</a>
                <a href="<?= $informations[4]['href'] ?>" class="footer__title"><?= $informations[4]['title'] ?></a>
                <a href="<?= $news ?>" class="footer__title"><?= $text_news ?></a>
            </div>
            <div>
                <a href="<?= $informations[6]['href'] ?>" class="footer__title"><?= $informations[6]['title'] ?></a>
                <a href="<?= $contact ?>" class="footer__title"><?= $text_contact ?></a>
                <?php if($is_logged){ ?>
                    <a href="<?= $account_edit ?>" class="footer__title"><?= $text_account ?></a>
                <?php }else{ ?>
                    <a href="javascript:void(0)" data-toggle="modal" data-target="#profileModal" class="footer__title"><?= $text_account ?></a>
                <?php } ?>
            </div>
            <div>
                <a href="tel:<?php echo str_replace(array(' ','(',')','-'),'',$telephone);?>" class="footer__tel"><?php echo $telephone ?></a>
                <div class="footer__address"><?= $config_city . ', ' . $config_address ?></div>
                <a href="mailto:<?php echo $email; ?>" class="footer__mail"><?php echo $email; ?></a>
            </div>
        </div>
        <div class="footer__bottom">
            <div class="footer__copyright"><?php echo $powered ?></div>
            <div class="footer__socials flex-container flex-left flex-middle">
                <?php foreach($social as $key => $soc){ ?>
                    <a href="<?= $soc ?>" target="_blank"><i class="icon-<?= $key ?>"></i></a>
                <?php } ?>
            </div>
            <a href="http://web-systems.solutions" target="_blank" class="footer__wss"><i class="icon-wss"></i></a>
        </div>
    </div>
</footer>
<div class="overlay"></div>
<?php echo $form_callback ?>
<div class="modal fade" id="formForgotten" tabindex="-1" role="dialog" aria-labelledby="formForgotten" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                <div class="modal-callback__form">
                    <div class="section__heading">
                        <div class="section__title">Восстановление пароля</div>
                        <div class="section__subtitle">Введите адрес электронной почты Вашей учетной записи. Нажмите кнопку "Востановить пароль", чтобы получить пароль по электронной почте. </div>
                    </div>
                    <form id="forgotten__form" method="post">
                        <div class="input-wrap">
                            <input type="text" id="forgotten-email" name="email">
                            <label for="forgotten-email"><i class="icon-mail"></i><span>E-mail</span></label>
                        </div>
                        <button class="btn btn-default btn-center"><span>Востановить пароль</span></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="thankYou" tabindex="-1" role="dialog" aria-labelledby="thankYou" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-profile">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                <div class="modal-body__thankYou">
                    <div class="page-heading-simple">
                        <div class="page-heading-simple__subtitle">Спасибо за покупку</div>
                        <div class="page-heading-simple__title">Это было прекрасно...</div>
                    </div>
                    <div class="modal-body__text">
                        Мы благодарны за Ваш выбор.<br>
                        Делайте свою жизнь разнообразной вместе с нами
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="atention" tabindex="-1" role="dialog" aria-labelledby="atention" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-atention__pct"><img src="image/ikonka18.png" alt="atention 18 +">
                    <p class="row-attention-text">
                        Вам уже єсть 18 лет?
                    </p>
                </div>
            </div>
            <div class="footer__bottom">

                <div class="row-attention">
                    <div class="col-left">
                        <button class="btn btn-default btn-center atentionyes" onclick="attention('yes')"><span>Да</span></button>
                    </div>
                    <div class="col-right">
                        <button class="btn btn-default btn-center atentionno"  onclick="attention('no')"><span>Нет</span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<?php echo $quicksignup ?>
<?php foreach ($styles as $style) { ?>
    <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<?php $v='0.6'; ?>
<script src="/catalog/view/javascript/jquery/jquery-2.1.1.min.js?v=<?= $v;?>"></script>
<script src="/catalog/view/javascript/mf/iscroll.js?v=<?= $v ?>"></script>
<script src="/catalog/view/theme/default/html/build/js/app.js?v=<?= $v;?>"></script>
<?php foreach ($scripts as $script) { ?>
    <script src="<?php echo $script; ?>"></script>
<?php } ?>
<script src="/catalog/view/javascript/common.js?v=<?= $v;?>"></script>
<script src="/catalog/view/javascript/social_auth.js?v=<?= $v;?>"></script>
</body></html>