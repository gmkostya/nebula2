<div class="modal fade" id="profileModal" tabindex="-1" role="dialog" aria-labelledby="profileModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-profile">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                <div id="quick-login" class="modal-profile__side modal-profile__side-auth">
                    <div class="modal-profile__desc">Чтобы зарегистрироваться, просто войдите через свой любимый аккаунт в соцсети</div>
                    <div class="modal-profile__socials">
                        <a href="javascript:void(0)" onclick="social_auth.facebook(this)" class="modal-profile__social modal-profile__social-fb"><i class="icon-face2"></i>Вход через Facebook</a>
                        <a href="javascript:void(0)" onclick="social_auth.googleplus(this)" class="modal-profile__social modal-profile__social-gg"><i class="icon-google"></i>Вход через Google</a>
                    </div>
                    <div class="section__heading">
                        <div class="section__title">Вход через логин и пароль</div>
                        <div class="section__subtitle">всегда рады...</div>
                    </div>
                    <div class="form-group">
                        <div class="input-wrap">
                            <input type="text" name="email" value="" id="login-input-email">
                            <label for="login-input-email"><i class="icon-mail"></i><span><?php echo $entry_email; ?></span></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-wrap">
                            <input type="password" name="password" value="" id="login-input-password">
							<label for="login-input-password"><i class="icon-pass"></i><span><?php echo $entry_password; ?></span></label>
                        </div>
						<a href="javascript:void(0)" id="forgot-pass" class="forgot-pass">Забыли пароль?</a>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-default btn-center loginaccount"><span><?php echo $button_login ?></span></button>
                    </div>
                </div>
                <div id="quick-register" class="modal-profile__side modal-callback__form">
                    <div class="section__heading">
                        <div class="section__title">Регистрация</div>
                        <div class="section__subtitle">без лишних слов...</div>
                    </div>
                    <div class="input-wrap">
                        <input type="text" name="name" value="" id="reg-input-name">
                        <label for="reg-input-name"><i class="icon-log-in"></i><span><?php echo $entry_name; ?></span></label>
                    </div>
                    <div class="input-wrap">
                        <input type="text" name="email" value="" id="reg-input-email">
                        <label for="reg-input-email"><i class="icon-mail"></i><span><?php echo $entry_email; ?></span></label>
                    </div>
                    <div class="input-wrap">
                        <input type="password" name="password" value="" id="reg-input-password">
                        <label for="reg-input-password"><i class="icon-pass"></i><span><?php echo $entry_password; ?></span></label>
                    </div>
                    <div class="input-wrap">
                        <input type="password" name="password2" value="" id="reg-input-password2">
                        <label for="reg-input-password2"><i class="icon-pass"></i><span><?php echo $entry_password2; ?></span></label>
                    </div>
                    <div class="check-wrap">
                        <input type="checkbox" name="newsletter" value="1" id="notifications">
                        <label for="notifications">Я ХОЧУ ... получать уведомления про интересные предложения и новинки</label>
                    </div>
                    <div class="check-wrap">
                        <input type="checkbox" name="notifications" id="register_notifications">
                        <label for="register_notifications"><?= $text_notifications ?></label>
                    </div>
                    <button class="btn btn-default btn-center createaccount"><span><?php echo $button_register; ?></span></button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php /*
<div class="modal fade" id="modal-login" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
		<div class="modal-content" id="quick-login">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title main-heading"><?php echo $text_returning ?></h4>
			</div>
			<div class="modal-body">
				<div class="form-group required">
					<label class="control-label" for="input-email"><?php echo $entry_email; ?></label>
					<input type="text" name="email" value=""  id="input-email" class="form-control" />
				</div>
				<div class="form-group required">
					<label class="control-label" for="input-password"><?php echo $entry_password; ?></label>
					<input type="password" name="password" value="" id="input-password" class="form-control" />
				</div>
				<div class="form-group">
					<a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a>
				</div>
			</div>
			<div class="modal-footer"> 
              	<button type="button" class="btn btn-primary loginaccount"  data-loading-text="<?php echo $text_loading; ?>"><?php echo $button_login ?></button>
          	</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-register" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
		<div class="modal-content" id="quick-register">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title main-heading"><?php echo $text_new_customer ?></h4>
			</div>
			<div class="modal-body">
				<div class="form-group required">
					<label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
					<input type="text" name="name" value="" id="input-name" class="form-control" />
				</div>
				<div class="form-group required">
					<label class="control-label" for="input-email"><?php echo $entry_email; ?></label>
					<input type="text" name="email" value="" id="input-email" class="form-control" />
				</div>
				<div class="form-group required">
					<label class="control-label" for="input-telephone"><?php echo $entry_telephone; ?></label>
					<input type="text" name="telephone" value="" id="input-telephone" class="form-control" />
				</div>
				<div class="form-group required">
					<label class="control-label" for="input-password"><?php echo $entry_password; ?></label>
					<input type="password" name="password" value="" id="input-password" class="form-control" />
				</div>
			</div>
			<div class="modal-footer"> 
				<?php if ($text_agree) { ?>
					<input type="checkbox" name="agree" value="1" />&nbsp;<?php echo $text_agree; ?><br><br>
					<button type="button" class="btn btn-primary createaccount"  data-loading-text="<?php echo $text_loading; ?>" ><?php echo $button_continue; ?></button>
				<?php }else{ ?>
					<button type="button" class="btn btn-primary createaccount" data-loading-text="<?php echo $text_loading; ?>" ><?php echo $button_continue; ?></button>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript"><!--
$(document).delegate('.quick_login', 'click', function(e) {
	$('#modal-login').modal('show');
});
$(document).delegate('.quick_register', 'click', function(e) {
	$('#modal-register').modal('show');
});
//--></script>
<script type="text/javascript"><!--
$('#quick-register input').on('keydown', function(e) {
	if (e.keyCode == 13) {
		$('#quick-register .createaccount').trigger('click');
	}
});
$('#quick-register .createaccount').click(function() {
	$.ajax({
		url: 'index.php?route=common/quicksignup/register',
		type: 'post',
		data: $('#quick-register input[type=\'text\'], #quick-register input[type=\'password\'], #quick-register input[type=\'checkbox\']:checked'),
		dataType: 'json',
		beforeSend: function() {
			$('#quick-register .createaccount').button('loading');
			$('#modal-register .alert-danger').remove();
		},
		complete: function() {
			$('#quick-register .createaccount').button('reset');
		},
		success: function(json) {
			$('#modal-register .form-group').removeClass('has-error');
			
			if(json['islogged']){
				 window.location.href="index.php?route=account/account";
			}
			if (json['error_name']) {
				$('#quick-register #input-name').parent().addClass('has-error');
				$('#quick-register #input-name').focus();
			}
			if (json['error_email']) {
				$('#quick-register #input-email').parent().addClass('has-error');
				$('#quick-register #input-email').focus();
			}
			if (json['error_telephone']) {
				$('#quick-register #input-telephone').parent().addClass('has-error');
				$('#quick-register #input-telephone').focus();
			}
			if (json['error_password']) {
				$('#quick-register #input-password').parent().addClass('has-error');
				$('#quick-register #input-password').focus();
			}
			if (json['error']) {
				$('#modal-register .modal-header').after('<div class="alert alert-danger" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}
			
			if (json['now_login']) {
				$('.quick-login').before('<li class="dropdown"><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_account; ?></span> <span class="caret"></span></a><ul class="dropdown-menu dropdown-menu-right"><li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li><li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li><li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li><li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li><li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li></ul></li>');
				
				$('.quick-login').remove();
			}
			if (json['success']) {
				$('#modal-register .main-heading').html(json['heading_title']);
				success = json['text_message'];
				success += '<div class="buttons"><div class="text-right"><a onclick="loacation();" class="btn btn-primary">'+ json['button_continue'] +'</a></div></div>';
				$('#modal-register .modal-body').html(success);
			}
		}
	});
});
//--></script>
<script type="text/javascript"><!--
$('#quick-login input').on('keydown', function(e) {
	if (e.keyCode == 13) {
		$('#quick-login .loginaccount').trigger('click');
	}
});
$('#quick-login .loginaccount').click(function() {
	$.ajax({
		url: 'index.php?route=common/quicksignup/login',
		type: 'post',
		data: $('#quick-login input[type=\'text\'], #quick-login input[type=\'password\']'),
		dataType: 'json',
		beforeSend: function() {
			$('#quick-login .loginaccount').button('loading');
			$('#modal-login .alert-danger').remove();
		},
		complete: function() {
			$('#quick-login .loginaccount').button('reset');
		},
		success: function(json) {
			$('#modal-login .form-group').removeClass('has-error');
			if(json['islogged']){
				 window.location.href="index.php?route=account/account";
			}
			
			if (json['error']) {
				$('#modal-login .modal-header').after('<div class="alert alert-danger" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
				$('#quick-login #input-email').parent().addClass('has-error');
				$('#quick-login #input-password').parent().addClass('has-error');
				$('#quick-login #input-email').focus();
			}
			if(json['success']){
				loacation();
				$('#modal-login').modal('hide');
			}
			
		}
	});
});
//--></script>
<script type="text/javascript"><!--
function loacation() {
	location.reload();
}
//--></script>
*/?>