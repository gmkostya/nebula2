<a href="javascript:void(0)" class="search-toggle"><i class="icon-search"></i></a>
<div class="header__search">
    <div id="search" class="flex-container flex-left flex-middle">
        <input type="search" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_search; ?>">
        <button type="submit" class="btn btn-default btn-small"><span><?php echo $button_search; ?></span></button>
    </div>
</div>
<?php /*
<div id="search" class="input-group">
  <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_search; ?>" class="form-control input-lg" />
  <span class="input-group-btn">
    <button type="button" class="btn btn-default btn-lg"><i class="fa fa-search"></i></button>
  </span>
</div>*/?>