<?php
/**
 * @var $header
 * @var $column_left
 * @var $content_top
 * @var $content_bottom
 * @var $column_right
 */
?>
<?php echo $header; ?>
    <main>
        <?php echo $column_left; ?>
        <?php echo $content_top; ?>
        <div class="section section-grey">
            <div class="container">
                <?php echo $content_bottom; ?>
                <?php echo $column_right; ?>
            </div>
        </div>
    </main>
<?php echo $footer; ?>