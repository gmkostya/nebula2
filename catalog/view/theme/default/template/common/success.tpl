<?php echo $header; ?>
<main class="profile">
  <div class="page-heading" style="background-image: url('/catalog/view/theme/default/html/build/img/catalog-heading-bg.jpg');">
    <div class="breadcrumbs container">
      <?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>
    </div>
    <div class="container">
        <h1 class="page-heading__title"><?php echo $heading_title; ?></h1>
        <!-- <div class="page-heading__subtitle">Большой выбор интересных товаров для ярких впечетлений</div> -->
    </div>
</div>
<div class="profile-wrap container">
  <?php echo $column_right; ?>
  <?php echo $column_left; ?>
  <?php if ($column_left && $column_right) { ?>
  <?php $class = 'col-sm-6'; ?>
  <?php } elseif ($column_left || $column_right) { ?>
  <?php $class = 'col-sm-9'; ?>
  <?php } else { ?>
  <?php $class = 'col-sm-12'; ?>
  <?php } ?>
  <div class="profile-body"><?php echo $content_top; ?>
      <?php echo $text_message; ?>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-default"><span><?php echo $button_continue; ?></span></a></div>
      </div>
      <?php echo $content_bottom; ?>
  </div>
  <?php echo $column_right; ?>
</div>
</main>
<?php echo $footer; ?>