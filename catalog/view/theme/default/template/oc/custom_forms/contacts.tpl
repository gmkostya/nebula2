<div class="container contacts__wrap">
    <div class="section__heading  section__heading-left">
        <div class="section__title"><?= $element['form_text']['title'] ?></div>
        <div class="section__subtitle"><?= $element['form_text']['text'] ?></div>
    </div>
    <form id="contacts__form" class="oc-custom-forms contacts__form" action="<?php echo $action; ?>" method="post">
        <div style="display:none">
            <input type="text" name="name" value="" />
            <input type="checkbox" name="confirm" value="1" <?php echo ((isset($confirm) && $confirm != '')? 'checked="checked"':'')?> />
        </div>
        <div class="flex-container flex-between flex-middle">
            <div class="input-wrap">
                <input type="text" id="contacts-first_name" name="first_name" value="<?php echo $element['first_name']['value']; ?>">
                <label for="contacts-first_name"><i class="icon-log-in"></i><span><?php echo $element['first_name']['placeholder']; ?></span></label>
            </div>
            <div class="input-wrap">
                <input type="tel" id="contacts-phone" name="phone" value="<?php echo $element['phone']['value']; ?>">
                <label for="contacts-phone"><i class="icon-phone-kontaktu"></i><span><?php echo $element['phone']['placeholder']; ?></span></label>
            </div>
            <div class="input-wrap">
                <input type="text" id="contacts-email" name="email" value="<?php echo $element['email']['value']; ?>">
                <label for="contacts-email"><i class="icon-mail"></i><span><?php echo $element['email']['placeholder']; ?></span></label>
            </div>
        </div>
        <div class="input-wrap">
            <textarea placeholder="<?php echo $element['message']['placeholder']; ?>" name="message" id="contacts-message" cols="30" rows="10"></textarea>
        </div>
        <div class="input-captcha flex-container flex-left flex-middle">
            <?php echo $captcha; ?>
        </div>
        <div class="check-wrap">
            <input type="checkbox" name="notifications" id="contacts_notifications">
            <label for="contacts_notifications"><?= $text_notifications ?></label>
        </div>
        <button type="submit" class="btn btn-default"><span><?php echo $element['button']['label']; ?></span></button>
    </form>
</div>
<script>
// window.onload = function() {
    if ($('form [name=notifications]:checked')) {
        $('#contacts__form button').prop('disabled', true);
    } else {
        $('#contacts__form button').prop('disabled', false);
    }
    $('form [name=notifications]').on('change', function () {
        if (this.checked) {
            $('#contacts__form button').prop('disabled', false);
        } else {
            $('#contacts__form button').prop('disabled', true);
        }
    });
// }
</script>