<div class="modal fade" id="callbackModal" tabindex="-1" role="dialog" aria-labelledby="callbackModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                <div class="modal-callback__pct"><img src="/catalog/view/theme/default/html/build/img/modal-callback.jpg" alt="callback"></div>
                <div class="modal-callback__form">
                    <div class="section__heading">
                        <div class="section__title"><?php echo $element['form_text']['title'] ?></div>
                        <div class="section__subtitle"><?php echo $element['form_text']['text'] ?></div>
                    </div>
                    <form id="callback__form" class="oc-custom-forms" action="<?php echo $action; ?>" method="post">
                        <div style="display:none">
                            <input type="text" name="name" value="" />
                            <input type="checkbox" name="confirm" value="1" <?php echo ((isset($confirm) && $confirm != '')? 'checked="checked"':'')?> />
                        </div>
                        <div class="input-wrap">
                            <input type="text" name="first_name" id="callback-first_name" value="<?php echo $element['first_name']['value']; ?>">
                            <label for="callback-first_name"><i class="icon-log-in"></i><span><?php echo $element['first_name']['label']; ?></span></label>
                        </div>
                        <div class="input-wrap">
                            <input type="tel" name="phone" id="callback-phone" value="<?php echo $element['phone']['value']; ?>">
                            <label for="callback-phone"><i class="icon-call-back"></i><span><?php echo $element['phone']['label']; ?></span></label>
                        </div>
                        <div class="input-wrap">
                            <textarea name="message" id="callback-message" placeholder="<?php echo $element['message']['label']; ?>"></textarea>
                        </div>
                        <div class="check-wrap">
                            <input type="checkbox" name="notifications" id="callback_notifications">
                            <label for="callback_notifications"><?= $text_notifications ?></label>
                        </div>
                        <button class="btn btn-default btn-center"><span><?php echo $element['button']['label']; ?></span></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
window.onload = function() {
    if ($('form [name=notifications]:checked')) {
        $('#callback__form button').prop('disabled', true);
    } else {
        $('#callback__form button').prop('disabled', false);
    }
    $('form [name=notifications]').on('change', function () {
        if (this.checked) {
            $('#callback__form button').prop('disabled', false);
        } else {
            $('#callback__form button').prop('disabled', true);
        }
    });
}
</script>