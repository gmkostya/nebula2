<form class="box-phone__form werror wk-form" action="<?php echo $action; ?>" method="post">
    <div style="display:none">
        <input type="text" name="name" value="" />
        <input type="checkbox" name="confirm" value="1" <?php echo ((isset($confirm) && $confirm != '')? 'checked="checked"':'')?> />
        <input id="page_name" name="page_name" type="hidden" value="<?php echo $values['page_name'];?>">
        <input id="page_url" name="page_url" type="hidden" value="<?php echo $values['page_url'];?>">
    </div>
    <div class="box-phone__form-title"><?php echo $element['title'] ?></div>
    <div class="form-group mod-form-group">
        <label class="sr-only"><?php echo $element['phone']['label']; ?><?php echo $element['phone']['required'] ? ' <span class="required">*</span>' : ''; ?></label>
        <input type="tel" name="phone" placeholder="<?php echo $element['phone']['placeholder']; ?><?php echo $element['phone']['required'] ? ' *' : ''; ?>" class="form-control" value="<?php echo $element['phone']['value']; ?>"/>
    </div>
    <div class="form-group mod-form-group">
        <label class="sr-only"><?php echo $element['first_name']['label']; ?><?php echo $element['first_name']['required'] ? ' <span class="required">*</span>' : ''; ?></label>
        <input type="text" name="first_name" placeholder="<?php echo $element['first_name']['placeholder']; ?><?php echo $element['first_name']['required'] ? ' *' : ''; ?>" class="form-control" value="<?php echo $element['first_name']['value']; ?>"/>
    </div>
    <div class="box-phone__form-box-submit">
        <button type="submit" class="btn"<?php echo $element['button']['onclick'] ? ' onclick="'.$element['button']['onclick'] . '"' : ''; ?>><?php echo $element['button']['label']; ?></button>
    </div>
</form>