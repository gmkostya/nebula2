<?php echo $header; ?>
<main class="contacts">
    <div class="page-heading" style="background-image: url('/catalog/view/theme/default/html/build/img/contacts-heading-bg.jpg');">
        <div class="breadcrumbs container">
            <?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>
        </div>
        <div class="container">
            <h1 class="page-heading__title"><?php echo $heading_title; ?></h1>
            <div class="page-heading__subtitle"><?php // $text_title ?></div>
        </div>
    </div>
    <div class="container contacts__wrap">
        <p><?php echo $text_email; ?></p>
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
            <div class="input-wrap">
                <input type="text" id="forgten-email" name="email">
                <label for="forgten-email"><i class="icon-mail"></i><span><?php echo $entry_email; ?></span></label>
                <?php if ($error_warning) { ?>
                    <label for="forgten-email" class="error"><i class="icon-mail"></i><span><?php echo $error_warning; ?></span></label>
                <?php } ?>
            </div>
            <button type="submit" class="btn btn-default"><span><?php echo $button_continue; ?></span></button>
        </form>
    </div>
</main>
<script>
    $(document).on('focus', 'form input', function () {
        $(this).parent('.input-wrap').find('label.error').remove();
    });
</script>
<?php /*
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <p><?php echo $text_email; ?></p>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
        <fieldset>
          <legend><?php echo $text_your_email; ?></legend>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-email"><?php echo $entry_email; ?></label>
            <div class="col-sm-10">
              <input type="text" name="email" value="" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
            </div>
          </div>
        </fieldset>
        <div class="buttons clearfix">
          <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></div>
          <div class="pull-right">
            <input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary" />
          </div>
        </div>
      </form>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>*/?>
<?php echo $footer; ?>