<?php echo $header; ?>
<main class="profile">
    <div class="page-heading" style="background-image: url('/catalog/view/theme/default/html/build/img/checkout-main-bg.jpg');">
      <div class="breadcrumbs container">
        <?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>
      </div>
      <div class="container">
          <h1 class="page-heading__title"><?php echo $heading_title; ?></h1>
          <!-- <div class="page-heading__subtitle">Большой выбор интересных товаров для ярких впечетлений</div> -->
      </div>
  </div>
  <div class="profile-wrap container">
    <?php echo $column_right; ?>
    <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div class="profile-body"><?php echo $content_top; ?>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
        <div class="input-wrap required">
            <input type="password" name="password" value="<?php echo $password; ?>" id="input-password" />
            <label for="input-password"><i class="icon-pass"></i><span><?php echo $entry_password; ?></span></label>
            <?php if ($error_password) { ?>
            <label class="error" for="input-password"><i class="icon-pass"></i><span><?php echo $error_password; ?></span></label>
            <?php } ?>
        </div>
        <div class="input-wrap required">
            <input type="password" name="confirm" value="<?php echo $confirm; ?>" id="input-confirm" class="form-control" />
            <label for="input-confirm"><i class="icon-pass"></i><span><?php echo $entry_confirm; ?></span></label>
            <?php if ($error_confirm) { ?>
            <label class="error" for="input-confirm"><i class="icon-pass"></i><span><?php echo $error_confirm; ?></span></label>
            <?php } ?>
        </div>
        <button type="submit" class="btn btn-default"><span><?php echo $button_continue; ?></span></button>
      </form>
      <?php echo $content_bottom; ?>
    </div>
  </div>
</main>
<?php echo $footer; ?>