<?php echo $header; ?>
<main class="profile">
    <div class="page-heading" style="background-image: url('/catalog/view/theme/default/html/build/img/checkout-main-bg.jpg');">
      <div class="breadcrumbs container">
        <?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>
      </div>
      <div class="container">
          <h1 class="page-heading__title"><?php echo $heading_title; ?> #<?php echo $order_id; ?></h1>
          <div class="page-heading__subtitle"><?php echo $date_added; ?></div>
      </div>
  </div>
  <div class="profile-wrap container">
    <?php echo $column_right; ?>
    <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div class="profile-body">
      <h2>Список товаров</h2>
      <div class="order-single__grid">
        <?php foreach ($products as $product) { ?>
          <div class="product-layout non-hover">
              <a href="<?php echo $product['href']; ?>" class="product-layout__pct">
                  <img src="<?php echo $product['image']; ?>" alt="">
              </a>
              <a href="<?php echo $product['href']; ?>" class="product-layout__name" title="<?php echo $product['name']; ?>"><?php echo $product['name']; ?></a>
              <div class="product-layout__price"><?php echo $product['price']; ?></div>
              <div class="order-single__product-quantity"><i class="icon-close"></i> <span><?php echo $product['quantity']; ?></span></div>
              <div class="order-single__product-total"><?php echo $product['total']; ?></div>
              <?php /*
              <div class="product-layout__actions flex-container flex-center flex-top">
                  <a href="<?php echo $product['reorder']; ?>" data-toggle="tooltip" title="<?php echo $button_reorder; ?>" class="btn btn-default"><span>Заказать еще</span></a>
                  <a href="<?php echo $product['return']; ?>" data-toggle="tooltip" title="<?php echo $button_return; ?>" class="more-link">Вернуть товар</a>
              </div>*/?>
          </div>
          <!-- <div class="order-single__product">
            <div class="order-single__product-pct"></div>
            <div class="order-single__product-name"><?php echo $product['name']; ?></div>
            <div class="order-single__product-price"><?php echo $product['price']; ?></div>
            <div class="order-single__product-quantity"><i class="icon-close"></i> <span><?php echo $product['quantity']; ?></span></div>
            <div class="order-single__product-total"><?php echo $product['total']; ?></div>
            <div class="order-single__product-buttons">
                <?php if ($product['reorder']) { ?>
                  <a href="<?php echo $product['reorder']; ?>" data-toggle="tooltip" title="<?php echo $button_reorder; ?>" class="btn btn-primary"><i class="fa fa-shopping-cart"></i></a>
                  <?php } ?>
                  <a href="<?php echo $product['return']; ?>" data-toggle="tooltip" title="<?php echo $button_return; ?>" class="btn btn-danger"><i class="fa fa-reply"></i></a></td>
            </div>
          </div> -->
          <?php } ?>
      </div>
      <div class="order-details-wrap flex-container flex-between flex-top">
        <div class="order-details__side">
          <h2><?php echo $text_order_detail; ?></h2>
          <div class="order-heading__total order-deteils">
            <?php if ($payment_method): ?>
              <label class="order-deteils__label"><?php echo $text_payment_method; ?></label>
              <span class="order-deteils__value"><?php echo $payment_method; ?></span>
            <?php endif; ?>
            <?php if ($shipping_method): ?>
              <label class="order-deteils__label"><?php echo $text_shipping_method; ?></label>
              <span class="order-deteils__value"><?php echo $shipping_method; ?></span>
            <?php endif; ?>
              <label class="order-deteils__label"><?php echo $text_payment_address; ?>:</label>
              <span class="order-deteils__value"><?php echo $payment_address; ?></span>
            <?php /*if ($shipping_address): ?>
              <label class="order-deteils__label"><?php echo $text_shipping_address; ?>:</label>
              <span class="order-deteils__value"><?php echo $shipping_address; ?></span>
            <?php endif;*/ ?>
            <?php if ($comment): ?>
              <label class="order-deteils__label"><?php echo $text_comment; ?>:</label>
              <span class="order-deteils__value"><?php echo $comment; ?></span>
            <?php endif; ?>
          </div>
        </div>
        <div class="order-details__side">
          <h2>Цена заказа</h2>
          <div class="order-heading">
            <div class="order-heading__total">
                <?php foreach ($totals as $total) { ?>
                    <?php if($total['code'] == 'total'){ ?>
                        <label>Стоимость заказа<?php //echo $total['title']; ?>:</label> <strong><?php echo $total['text']; ?></strong>
                    <?php } ?>
                <?php } ?>
            </div>
          </div>
        </div>
      </div>
      <?php echo $content_bottom; ?>
    </div>
  </div>
</main>
<?php echo $footer; ?>