<?php echo $header; ?>
<main class="profile">
    <div class="page-heading" style="background-image: url('/catalog/view/theme/default/html/build/img/checkout-main-bg.jpg');">
      <div class="breadcrumbs container">
        <?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>
      </div>
      <div class="container">
          <h1 class="page-heading__title"><?php echo $heading_title; ?></h1>
          <!-- <div class="page-heading__subtitle">Большой выбор интересных товаров для ярких впечетлений</div> -->
      </div>
  </div>
  <div class="profile-wrap container">
    <?php echo $column_right; ?>
    <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div class="profile-body">
      <?php echo $content_top; ?>
      <?php if ($orders) { ?>
        <div class="orders-list">
          <?php foreach ($orders as $order) { ?>
            <?php switch($order['status']) {
              case 'Ожидание':
                $status = ' procesing';
                break;
              case 'В обработке':
                $status = ' procesing';
                break;
              case 'Доставлено':
                $status = ' complite';
                break;
              case 'Сделка завершена':
                $status = ' complite';
                break;
              case 'Отменено':
                $status = ' cencled';
                break;
              case 'Возврат':
                $status = ' cencled';
                break;
              case 'Отмена и аннулирование':
                $status = ' cencled';
                break;
              case 'Неудавшийся':
                $status = ' cencled';
                break;
              case 'Полностью измененный':
                $status = ' cencled';
                break;
              case 'Полный возврат':
                $status = ' cencled';
                break;
              default: '';
              break;
            }?>
            <div class="orders-list__item<?= $status; ?>">
              <a href="<?php echo $order['href']; ?>" class="orders-list__title">Заказ #<?php echo $order['order_id']; ?></a>
              <div class="orders-list__status"><?php echo $order['status']; ?></div>
              <div class="orders-list__date"><?php echo $order['date_added']; ?></div>
              <div class="product-layout__price"><?php echo $order['total']; ?></div>
              <a href="<?php echo $order['href']; ?>" class="more-link">Посмотреть заказ</a>
            </div>
          <?php } ?>
        </div>
      <div class="text-right"><?php echo $pagination; ?></div>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <?php } ?>
      <?php echo $content_bottom; ?>
    </div>
  </div>
</main>
<?php echo $footer; ?>