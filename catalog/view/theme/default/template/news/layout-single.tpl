<?php echo $header; ?>
<main class="news-single">
    <div class="breadcrumbs container">
        <?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>
    </div>
    <div style="display: none" itemscope itemtype="http://schema.org/BlogPosting">
        <div class="" itemprop="articleBody">
            <div itemprop="headline"><?php echo $heading_title; ?></div>
            <span itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
            <span itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                <img alt="<?php echo $config_name ?>" itemprop="image url" src="<?php echo $logo_url ?>"/>
                <meta itemprop="width" content="63">
                <meta itemprop="height" content="71">
            </span>
            <meta itemprop="telephone" content="<?php echo $telephone ?>">
            <meta itemprop="address" content="<?php echo $email ?>">
            <meta itemprop="name" content="<?php echo $home ?>">
        </span>
            <span itemprop="author" itemscope itemtype="http://schema.org/Person">
            <meta itemprop="name" content="<?php echo $author_name; ?>">
        </span>
            <span itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
        <img itemprop="image url" alt="<?php echo $heading_title; ?>" width="600" height="600" src="<?php echo $popup ?>"/>
        <meta itemprop="width" content="600">
        <meta itemprop="height" content="600">
    </span>
            <link itemprop="mainEntityOfPage" itemscope href="<?php echo $page_url?>" />
            <meta itemprop="datePublished" content="<?php echo $date_added ?>">
            <meta itemprop="dateModified" content="<?php echo $date_updated ?>">
        </div>
    </div>
    <div class="container">
        <h1 class="news-single__title"><?php echo $heading_title; ?></h1>
        <?php echo $content_top; ?>
        <?php echo $description; ?>
    </div>
</main>
<?php echo $content_bottom; ?>
<?php echo $footer; ?> 