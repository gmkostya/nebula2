$( document ).ready( function() {
	$( 'input' ).each(function() {
		if ( $( this ).val().length > 0 ) {
			$( this ).addClass( 'filled' );
		} else {
			$( this ).removeClass( 'filled' );
		}
	});

	var selector = document.querySelectorAll( '[type="tel"]' );
	Inputmask("+7 (999) 999-99-99").mask(selector);

	$( '.product__slider' ).slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		asNavFor: '.product__nav',
		draggable: false
	});
	$( '.product__nav' ).slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		asNavFor: '.product__slider',
		dots: false,
		centerMode: true,
		centerPadding: '0px',
		focusOnSelect: true
	});

	$( '.products-carousel:not(.products-carousel-single):not(.products-carousel-similar)' ).slick({
		infinite: false,
		slidesToShow: 3,
		slidesToScroll: 3,
		arrows: false,
		responsive: [
			{
				breakpoint: 770,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
					dots: true
				}
			}
		]
	});

	$( '.compare-list__carousel' ).slick({
		infinite: false,
		slidesToShow: 3,
		slidesToScroll: 1,
		arrows: false,
		responsive: [
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					dots: true
				}
			}
		]
	});

	$( '.products-carousel-single' ).slick({
		infinite: false,
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		autoplay: true,
		autoplaySpeed: 3000
	});

	$( '.products-carousel-similar' ).slick({
		infinite: false,
		slidesToShow: 4,
		slidesToScroll: 1,
		arrows: false,
		autoplay: true,
		autoplaySpeed: 3000,
		responsive: [
			{
				breakpoint: 991,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});

	$( '.news-carousel' ).slick({
		infinite: false,
		slidesToShow: 3,
		slidesToScroll: 3,
		arrows: false,
		responsive: [
			{
				breakpoint: 770,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 480,
				settings: "unslick"
			}
		]
	});

	$( '.catalog-carousel' ).slick({
		infinite: false,
		slidesToShow: 6,
		slidesToScroll: 6,
		arrows: false,
		responsive: [
			{
				breakpoint: 1600,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 4
				}
			},
			{
				breakpoint: 770,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
					dots: true
				}
			}
		]
	});


	if ( $( window ).width() < 1200 ) {
		// filter toggle
		$( 'body' ).on('click', '.catalog-filter__toggle:not(.active)', function( e ) {
			$( '.catalog-filter-wrap' ).addClass( 'active' );
			$( this ).addClass( 'active' );
		});

		$( 'body' ).on('click', '.catalog-filter__toggle.active, .catalog-filter__title', function( e ) {
			$( '.catalog-filter-wrap' ).removeClass( 'active' );
			$( this ).removeClass( 'active' );
		});

		$(document).mouseup(function ( e ) {
			var container = $( '.catalog-filter-wrap' );
			if ( container.has( e.target ).length === 0 ){
				container.removeClass( 'active' );
			}
		});

		// nav toggle
		$( 'body' ).on('click', '.header-visible .header__nav-toggle:not(.active), .header__internal .header__nav-toggle:not(.active)', function( e ) {
			$( '.header__nav' ).fadeIn();
			$( '.overlay' ).show(); 
		});

		$(document).mouseup(function ( e ) {
			var container = $( '.header__nav' );
			if ( container.has( e.target ).length === 0 ){
				container.fadeOut();
			}
		});

		$( 'body' ).on('click', '.header__nav ul li.has-child:not(.open)', function( e ) {
			$( this ).find( '.drop-down' ).fadeIn();
			$( this ).addClass( 'open' );
		});
		
		$( 'body' ).on('click', '.header .drop-down ul li.back', function( e ) {
			$( this ).closest( '.drop-down' ).fadeOut();
			$( this ).closest( '.has-child' ).removeClass( 'open' );
		});
	}

});

$( window ).on( 'scroll', function() {
	if ( $( window ).width() > 768 ) {
		if ( $( window ).scrollTop() > 0 ) {
			$( '.header:not(.header-visible)' ).addClass( 'scrolled' );
			$( '.header__main' ).slideUp();
			$( '.header__internal:not(.header__internal-visible)' ).fadeIn().addClass('visible');
		} else {
			$( '.header:not(.header-visible)' ).removeClass( 'scrolled' );
			$( '.header__main' ).slideDown();
			$( '.header__internal:not(.header__internal-visible)' ).fadeOut().removeClass('visible');
		}
	}
	if ( $( window ).width() < 768 && $( window ).scrollTop() + $( window ).height() > $( 'footer' ).offset().top ) {
		$( '.catalog-actions ' ).fadeOut();
	} else {
		$( '.catalog-actions ' ).fadeIn();
	}
});

$( '.ranzh' ).each( function() {
	var valRanzh = ranzh( $( this ).text());
	$( this ).text(valRanzh);
});

$( 'body' ).on('keyup', 'input', function() {
	if ( $( this ).val().length > 0 ) {
		$( this ).addClass( 'filled' );
	} else {
		$( this ).removeClass( 'filled' );
	}
});

$( 'body' ).on('change', '.input-rating input', function() {
	$( this ).parent().prevAll().addClass( 'active' );
	$( this ).parent().nextAll().removeClass( 'active' );
});

$( 'body' ).on('click', '.product-reviews__toggle', function() {
	$( this ).toggleClass( 'active' );
	$( '.product-reviews' ).toggleClass( 'active' );

	$( '.product-reviews__wrap' ).slideToggle({
		start: function () {
			$( this ).css({
				display: "flex"
			});
		}
	});
});

$( 'body' ).on('click', '.next-slide', function() {
	$( 'body, html' ).stop().animate( { scrollTop: $( '.main-banner' ).height() - 100 }, 500, 'swing', function() {
	});
});

// $( 'body' ).on('click', '.carousel-nav .icon-arrow-left', function() {
// 	$( this ).closest( '.carousel-wrap' ).find( '.slick-slider' ).slick( 'slickPrev' );
// });

// $( 'body' ).on('click', '.carousel-nav .icon-arrow-right', function() {
// 	$( this ).closest( '.carousel-wrap' ).find( '.slick-slider' ).slick( 'slickNext' );
// });

$( 'body' ).on('click', '.search-toggle', function( e ) {
	e.preventDefault();
	$( '.header__search' ).addClass('active');
	$( '#search' ).trigger( 'focus' );
	$( '.header__nav' ).addClass('hide');
});

$( 'body' ).on('click', '.catalog-view span', function( e ) {
	$( '.catalog-view span' ).removeClass( 'active' );
	$( this ).addClass( 'active' );
	$( '.catalog-container' ).removeClass( 'x4 x3 x2' );
	$( '.catalog-container' ).addClass( $( this ).data( 'view' ));
});

$( 'body' ).on('click', '.header__nav-toggle:not(.active)', function( e ) {
	$( this ).addClass( 'active' );
	$( '.header__nav-main' ).fadeIn();
});

$( 'body' ).on('click', '.header__nav-toggle.active', function( e ) {
	$( this ).removeClass( 'active' );
	$( '.header__nav-main' ).fadeOut();
});

$( 'body' ).on('click', '.has-child > a', function( e ) {
	if ( $( window ).width() < 1200 ) {
		e.preventDefault();
	}
});

$( 'body' ).on('click', '.main-about', function( e ) {
	$( this ).addClass( 'active' );
});

$( 'body' ).on('click', '.overlay', function( e ) {
	$( this ).hide();
});

$( 'body' ).on('click', 'a[onclick^="get_popup_purchase"], button[onclick^="get_popup_purchase"]', function( e ) {
	setTimeout(function() {
		$( '.modal-callback__form input' ).each(function() {
			if ( $( this ).val().length > 0 ) {
				$( this ).addClass( 'filled' );
			} else {
				$( this ).removeClass( 'filled' );
			}
		});
	},500);
});

$(document).mouseup(function ( e ) {
	var container = $( '.header__main' );
	if ( container.has( e.target ).length === 0 ){
		$( '.header__nav-main' ).fadeOut();
		$( '.header__main .header__nav-toggle.active' ).removeClass( 'active' );
	}
});

$( document ).on( 'click', '.product__qty .bnt_plus', function() {
	var num = parseInt( $( '.product__qty input[name=quantity]' ).val());
	var available_qty = parseInt( $( '.product__qty input[name=quantity]' ).data( 'qty' ));
	var min = parseInt( $( '.product__qty input[name=quantity]' ).data( 'min' ));

	if( num == available_qty ){
		$( '.product__qty .wrapp_input' ).addClass( 'show_avaliable' );
		setTimeout(function () {
			$( '.product__qty .wrapp_input' ).removeClass( 'show_avaliable' );
		}, 4000);
	}else{
		$( '.product__qty input[name=quantity]' ).val( num + min );
	}
});
$( document ).on( 'click', '.product__qty .bnt_minus', function() {
	if( $( '.product__qty input[name=quantity]' ).val() > $( '.product__qty input[name=quantity]' ).data( 'min' )) {
		var num = $( '.product__qty input[name=quantity]' ).val();
		$( '.product__qty input[name=quantity]' ).val( parseInt(num) - parseInt($('.product__qty input[name=quantity]' ).data( 'min' )));
	}
});

$(document).mouseup(function ( e ) {
	var container = $( '.header__search' );
	if ( container.has( e.target ).length === 0 ){
		container.removeClass( 'active' );
		$( '.header__nav' ).removeClass( 'hide' );
	}
});

function ranzh( val ) {
	var res = '';
	var length = val.length - 1;
	for ( var i = length; i >= 0; i-- ) {
		if ( ( length - i ) % 3 === 0 && length - i !== 0 ) {
			res = ' ' + res;
		}
		res = val[ i ] + res;
	}
	return res;
}

function getTimeRemaining(endtime) {
	var t = Date.parse(endtime) - Date.parse(new Date());
	var seconds = Math.floor((t / 1000) % 60);
	var minutes = Math.floor((t / 1000 / 60) % 60);
	var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
	var days = Math.floor(t / (1000 * 60 * 60 * 24));
	return {
		'total': t,
		'days': days,
		'hours': hours,
		'minutes': minutes,
		'seconds': seconds
	};
}

function initializeClock(elem, endtime) {
	var clock = $( elem );
	var daysSpan = clock.find( '.days' );
	var hoursSpan = clock.find('.hours');
	var minutesSpan = clock.find('.minutes');
	var secondsSpan = clock.find('.seconds');

	function updateClock() {
		var t = getTimeRemaining(endtime);

		daysSpan.html(t.days);
		hoursSpan.html(('0' + t.hours).slice(-2));
		minutesSpan.html(('0' + t.minutes).slice(-2));
		secondsSpan.html(('0' + t.seconds).slice(-2));

		if (t.total <= 0) {
			clearInterval(timeinterval);
		}
	}

	updateClock();
	var timeinterval = setInterval(updateClock, 1000);
}

$( '.product-layout__counter' ).each( function() {
	initializeClock( $( this ), $( this ).data( 'deadline' ) );
});

$(function() {
	$('.profile select').selectric({
		maxHeight: 120,
		disableOnMobile: false,
		nativeOnMobile: true,
		onInit: function(e) {
			var select = $( this );
			select.closest('.selectric-wrapper').find('.selectric-items').addClass(select.attr('id'));
			new IScroll( '.' + select.attr('id'), {
				bounce: false,
				scrollbars: true,
				mouseWheel: true,
				interactiveScrollbars: true,
				mouseWheelSpeed: 120
			});
		},
	});
	$('.catalog-sorting select').selectric({
		disableOnMobile: false,
		nativeOnMobile: true
	});
});

// subscribe

