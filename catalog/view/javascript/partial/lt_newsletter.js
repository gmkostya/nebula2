$(document).ready(function($) {
    $('#lt_newsletter_form input').on('focus', function(e) {
        $(this).parent('.input-wrap').find('label.error').remove();
    });
    $('#lt_newsletter_form').submit(function(){
        $.ajax({
            type: 'post',
            url: 'index.php?route=module/lt_newsletter/subscribe',
            data:$("#lt_newsletter_form").serialize(),
            dataType: 'json',
            beforeSend: function() {
                // $('.btn-newsletter').attr('disabled', true).button('loading');
            },
            complete: function() {
                // $('.btn-newsletter').attr('disabled', false).button('reset');
            },
            success: function(json) {
                $('.alert, .text-danger').remove();
                $('.form-group').removeClass('has-error');

                if (json.error) {
                    // $('#lt_newsletter_form').after('<div class="alert alert-danger newsletter-msg">' + json.error + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    $('#lt_newsletter_email').parent().append('<label for="lt_newsletter_email" class="error"><i class="icon-mail"></i><span>'+ json.error +'</span></label>');
                } else {
                    $('#lt_newsletter_form').find("input").removeClass('filled');
                    $('#lt_newsletter_email').val('');

                    $('#thankYou .page-heading-simple__subtitle').html(json.subscribed);
                    $('#thankYou .page-heading-simple__title').html(json.text_title);
                    $('#thankYou .modal-body__text').html(json.success);
                    // $('#profileModal').modal('hide');
                    $('#thankYou').modal('show');
                    // $('#lt_newsletter_form').after('<div class="alert alert-success newsletter-msg">' + json.success + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    // $('#lt_newsletter_email').val('');
                }
            }

        });
        return false;
    });
});