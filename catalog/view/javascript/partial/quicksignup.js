/*$(document).delegate('.quick_login', 'click', function(e) {
    $('#modal-login').modal('show');
});
$(document).delegate('.quick_register', 'click', function(e) {
    $('#modal-register').modal('show');
});*/
$('#quick-register input').on('keydown', function(e) {
    if (e.keyCode == 13) {
        $('#quick-register .createaccount').trigger('click');
    }
});
$('#quick-register input').on('focus', function(e) {
    $(this).parent('.input-wrap').find('label.error').remove();
});
$('#quick-register .createaccount').click(function() {
    $.ajax({
        url: 'index.php?route=common/quicksignup/register',
        type: 'post',
        data: $('#quick-register input[type=\'text\'], #quick-register input[type=\'password\'], #quick-register input[type=\'checkbox\']:checked'),
        dataType: 'json',
        beforeSend: function() {
            $('#quick-register .createaccount').button('loading');
            $('#modal-register .alert-danger').remove();
        },
        complete: function() {
            $('#quick-register .createaccount').button('reset');
        },
        success: function(json) {
            $('#quick-register input').removeClass('error');
            $('#quick-register span.error').remove();

            if(json['islogged']){
                window.location.href="index.php?route=account/account";
            }
            if(json['error']){
                if (json['error']['error_name']) {
                    $('#quick-register #reg-input-name').addClass('error');
                    $('#quick-register #reg-input-name').parent().append('<label for="reg-input-name" class="error"><i class="icon-log-in"></i><span>'+json['error']['error_name']+'</span></label>');
                    // $('#quick-register #reg-input-name').focus();
                }
                if (json['error']['error_email']) {
                    $('#quick-register #reg-input-email').addClass('error');
                    $('#quick-register #reg-input-email').parent().append('<label for="reg-input-email" class="error"><i class="icon-mail"></i><span>'+json['error']['error_email']+'</span></label>');
                    // $('#quick-register #reg-input-email').focus();
                }
                if (json['error']['error_password']) {
                    $('#quick-register #reg-input-password').addClass('error');
                    $('#quick-register #reg-input-password').parent().append('<label for="reg-input-password" class="error"><i class="icon-pass"></i><span>'+json['error']['error_password']+'</span></label>');
                    // $('#quick-register #reg-input-password').focus();
                }
                if (json['error']['error_password2']) {
                    $('#quick-register #reg-input-password2').addClass('error');
                    $('#quick-register #reg-input-password2').parent().append('<label for="reg-input-password2" class="error"><i class="icon-pass"></i><span>'+json['error']['error_password2']+'</span></label>');
                    // $('#quick-register #reg-input-password2').focus();
                }
            }
            /*if (json['error']) {
                $('#modal-register .modal-header').after('<div class="alert alert-danger" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
            }

            if (json['now_login']) {
                $('.quick-login').before('<li class="dropdown"><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_account; ?></span> <span class="caret"></span></a><ul class="dropdown-menu dropdown-menu-right"><li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li><li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li><li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li><li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li><li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li></ul></li>');

                $('.quick-login').remove();
            }*/
            if (json['success']) {
                /*$('#modal-register .main-heading').html(json['heading_title']);
                success = json['text_message'];
                success += '<div class="buttons"><div class="text-right"><a onclick="loacation();" class="btn btn-primary">'+ json['button_continue'] +'</a></div></div>';
                $('#modal-register .modal-body').html(success);*/
                $('#quick-register').find("input").removeClass('filled');
                $('#quick-register').find("input[type=text], input[type=password]").val("");
                $('#quick-register').find("input[type=checkbox]").prop( "checked", false );
                $('#thankYou .page-heading-simple__subtitle').html(json['heading_title']);
                $('#thankYou .page-heading-simple__title').html(json['text_title']);
                $('#thankYou .modal-body__text').html(json['text_message']);
                $('#profileModal').modal('hide');
                $('#thankYou').modal('show');
            }
        }
    });
});
$('#quick-login input').on('keydown', function(e) {
    if (e.keyCode == 13) {
        $('#quick-login .loginaccount').trigger('click');
    }
});
$('#quick-login input').on('focus', function(e) {
    $(this).parent('.input-wrap').find('label.error').remove();
});
$('#quick-login .loginaccount').click(function() {
    $.ajax({
        url: 'index.php?route=common/quicksignup/login',
        type: 'post',
        data: $('#quick-login input[type=\'text\'], #quick-login input[type=\'password\']'),
        dataType: 'json',
        beforeSend: function() {
            $('#quick-login .loginaccount').button('loading');
            $('#modal-login .alert-danger').remove();
        },
        complete: function() {
            $('#quick-login .loginaccount').button('reset');
        },
        success: function(json) {
            // $('#modal-login .form-group').removeClass('has-error');
            if(json['islogged']){
                window.location.href="index.php?route=account/account";
            }

            if(json['error']){
                if (json['error']['error_email']) {
                    $('#quick-login #login-input-email').addClass('error');
                    $('#quick-login #login-input-email').parent().append('<label for="login-input-email" class="error"><i class="icon-mail"></i><span>'+json['error']['error_email']+'</span></label>');
                    // $('#quick-register #reg-input-email').focus();
                }
                if (json['error']['error_password']) {
                    $('#quick-login #login-input-password').addClass('error');
                    $('#quick-login #login-input-password').parent().append('<label for="login-input-password" class="error"><i class="icon-pass"></i><span>'+json['error']['error_password']+'</span></label>');
                    // $('#quick-register #reg-input-password').focus();
                }
            }

            /*if (json['error']) {
                $('#modal-login .modal-header').after('<div class="alert alert-danger" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
                $('#quick-login #input-email').parent().addClass('has-error');
                $('#quick-login #input-password').parent().addClass('has-error');
                $('#quick-login #input-email').focus();
            }*/
            if(json['success']){
                loacation();
                $('#profileModal').modal('hide');
            }

        }
    });
});
function loacation() {
    location.reload();
}