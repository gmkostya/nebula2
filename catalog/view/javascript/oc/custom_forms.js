
/*function werror() {
    //$.fancybox.close('All');
    $.fancybox.open($('#popap-error'));
    /!*setTimeout(function () {
        $.fancybox.close('All');
    },3000);*!/
    jQuery(function($){
    $(document).ready(function (){
        $('form.wss_form').on('submit', function(e){

            e.preventDefault();

            var form = $(this);

            var data = $(form).serialize();
            if(data){

                $.ajax({
                    url: '/wp-admin/admin-ajax.php',
                    data: 'action=wss_form&'+data,
                    type:'POST',
                    beforeSend: function( xhr ) {
                    },
                    success:function(data){
                        var key;
                        form.find('.error').removeClass('error');
                        obj = JSON.parse(data);

                        console.log(obj);

                        if(obj.status === 'error'){
                            errors = '';
                            for (key in obj.errors) {
                                form.find('[name=' + key + ']').addClass("error");
                            }
                            $('#formError').modal('show');
                        }else{
                            $('#formSuccess').modal('show');
                        }
                    }
                });
            }
        });
    });
});
}*/
// window.onload = function() {
jQuery(function($) {
	$('.oc-custom-forms input, .oc-custom-forms textarea').on('focus', function(e) {
		$(this).parent('.input-wrap').find('label.error').remove();
	});
	$(document).ready(function () {
		$(document).on('submit', ".oc-custom-forms", function (e) {
			e.preventDefault();
			var form = this;
			$.ajax({
				type: 'POST',
				url: form.action,
				data: $(form).serialize(),
				dataType: 'json',
				beforeSend: function (xhr) {
				},
				success: function (data) {

                    $(form).find('label.error').remove();
					var id;
					var label;
					var clone;
					if (data.status === 'error') {
						for (var key in data.errors) {
							id = $(form).find('[name='+key+']').attr('id');
                            if(key === 'message'){
                                // console.log(key);
                                $('#'+ id).after('<label for="'+ id +'" class="error"><span>' + data.errors[key] + '</span></label>');
                            } else if(key === 'captcha'){
								// console.log(key);
								$('#'+ id).removeClass('filled');
								$('#'+ id).after('<label for="'+ id +'" class="error"><span>' + data.errors[key] + '</span></label>');
							}else{
								$(form).find('[for=' + id +']').after($(form).find('[for=' + id +']').clone().addClass('error'));
								$(form).find('[for=' + id +'].error span').text(data.errors[key]);
							}
						}
					} else if (data.status === 'success') {

						$('.oc-custom-forms').find("input").removeClass('filled');
						$('.oc-custom-forms').find("input[type=text], input[type=tel], textarea").val("");
						$('#thankYou .page-heading-simple__subtitle').html(data.subtitle);
						$('#thankYou .page-heading-simple__title').html(data.title);
						$('#thankYou .modal-body__text').html(data.message);
						$('#callbackModal').modal('hide');
						$('#thankYou').modal('show');

						//jQuery(form).children('.error_text').html('');
						//jQuery(form).find('.msg-block').html(data.message);
						// $.wsh.resetForm(form);
						/*$('.popup_make_order').modal('hide');
						setTimeout(function () {
							data.message = '<p>' + data.message + '</p>';
							$('#modal_default .modal-body').html(data.message);

							$('#modal_default').modal('show');
						}, 800);*/


						//success();
						/*$.material.init();

                        $(window).scroll();
                        $(window).resize();
                        */

						//$('.modal').modal('hide');
						//$('#popap-successfully').modal('show');

						//jQuery(form).find('.rate_box .checked').removeClass('checked');
					}
				},
			});
			// }
			// console.log('111');
			// $.wsh.submitForm(this);

			/*if (jQuery(this).hasClass('ascroll')) {
                jQuery.wsh.scrollTo(this, 700);
            }*/
		});
	});
});

	/*
	$.wsh = {

		scrollTo: function(elem, speed){
			$("html, body").animate({ scrollTop: $(elem).offset().top - 30 }, speed);
		},

		submitForm: function(form)
		{
			var errors = '';

			$.ajax({
				type: 'POST',
				url: form.action,
				data: $(form).serialize(),
				dataType: 'json',
				beforeSend: function(xhr) {
					//jQuery(form).loadmask();
					$(form).addClass('loading');
				},
				success: function(data)
				{
					$(form).find('span.form-control-feedback').remove();
					$(form).find('.form-error').remove();
					$(form).find('.error').removeClass('error');

					if ('error' == data.status) {

						if ($(form).hasClass('werror')) {
							errors = '';
							for (var key in data.errors) {
								errors += '<p>' + data.errors[key] + '</p>';
								if(key != 'rating'){
									$(form).find('[name=' + key + ']').addClass("error");
								}
							}
							$('#modal_default .modal-header h3').text(data.title);
							$('#modal_default .modal-body').html(errors);
							$('#modal_default').modal('show');

							//$.fancybox.open($('#popap-error'));
							//$('.modal').modal('hide');
							$('#modal-error').modal('show');

							//jQuery.wsh.resetForm(form);

						} else {
							for (var key in data.errors) {

								errors = '<span class="error">' + data.errors[key] + '</span>';

								$(form).find('[name=' + key + ']').addClass("error");
								/*if (key == 'rating') {
                                    errors = '' +
                                        '<div class="form-error">' + data.errors[key] + '</div>';
                                    jQuery(form).find('.reviews-form__box-rating').addClass("has-error").append(errors);
                                } else {
                                    errors = '<span class="material-icons form-control-feedback"><i class="fa fa-exclamation-circle" aria-hidden="true"></i></span>' +
                                        '<span class="form-error">' + data.errors[key] + '</span>';
                                    jQuery(form).find('[name=' + key + ']').closest('.mod-form-group').addClass("has-error").append(errors);
                                }*
							}
						}

					} else if ('success' == data.status) {

						//jQuery(form).children('.error_text').html('');
						//jQuery(form).find('.msg-block').html(data.message);
						$.wsh.resetForm(form);
						$('.popup_make_order').modal('hide');
						setTimeout(function () {
							data.message = '<p>' + data.message + '</p>';
							$('#modal_default .modal-body').html(data.message);

							$('#modal_default').modal('show');
						},800)


						//success();
						/*$.material.init();

                        $(window).scroll();
                        $(window).resize();
                        *

						//$('.modal').modal('hide');
						//$('#popap-successfully').modal('show');

						//jQuery(form).find('.rate_box .checked').removeClass('checked');
					}
				},
				complete: function(xhr) {
					//jQuery(form).loadunmask();
					$(form).removeClass('loading');
				}
			});

			return false;
		},

		loadMore: function(btn)
		{
			$.ajax({
				type: 'POST',
				url: $(btn).parent().attr('data-ajaxurl'),
				//data: jQuery(form).serialize(),
				dataType: 'json',
				beforeSend: function(xhr) {
					$(btn).addClass('loading');
				},
				success: function(data) {
					$(btn).parent().remove();
					$($(btn).parent().attr('data-conteiner')).append(data.html);
				},
				complete: function(xhr) {
					$(btn).removeClass('loading');
				}
			});

			return false;
		},

		resetForm: function(form)
		{
			$(form).find('.rating .rate-star > div.checked').removeClass('checked');
			form.reset();
			$(form).find('.error').removeClass('error');

			$(form).find('.label-floating').each(function(){

				$(this).addClass('is-empty');

			});



		}

	};*/
// }

