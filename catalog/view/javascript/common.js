function getURLVar(key) {
	var value = [];

	var query = String(document.location).split('?');

	if (query[1]) {
		var part = query[1].split('&');

		for (i = 0; i < part.length; i++) {
			var data = part[i].split('=');

			if (data[0] && data[1]) {
				value[data[0]] = data[1];
			}
		}

		if (value[key]) {
			return value[key];
		} else {
			return '';
		}
	}
}


function setCookie(name, value, days, secure) {
	var expires = '';
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		expires = '; expires='+date.toGMTString();
	}
	var domain = 'nebulatoy.ru';
	document.cookie = name + '='+escape(value) + expires + '; path=/' + (domain ? '; domain=.' + domain : '') + ((secure && locProtocol == 'https:') ? '; secure' : '');
}


function getCookie(name) {
	var cookie = " " + document.cookie;
	var search = " " + name + "=";
	var setStr = null;
	var offset = 0;
	var end = 0;
	if (cookie.length > 0) {
		offset = cookie.indexOf(search);
		if (offset != -1) {
			offset += search.length;
			end = cookie.indexOf(";", offset)
			if (end == -1) {
				end = cookie.length;
			}
			setStr = unescape(cookie.substring(offset, end));
		}
	}
	return(setStr);
}


function attention(key) {

	setCookie('cookie_attention', key, 30);
	if (key == 'yes') {
		$('#atention').modal('hide');
	} else {
		window.location.replace("https://www.youtube.com/channel/UCVF9-aelWI3oGRtmIKZueaA");
	}
}

function get_popup_purchase(product_id) {
	var url = 'index.php?route=module/popup_purchase&product_id=' + product_id;

	if (url.indexOf('#') == 0) {
		$(url).modal('open');
	} else {
		$.get(url, function (data) {
			$('' + data + '').modal();
		}).success(function () {
			setTimeout(function() {
				new Inputmask("+7 (999) 999-99-99").mask('#purchase_telephone');
				$('#popup-checkout-button').prop('disabled', true);
				/*if ($('#purchase-form [name=notifications]:checked')) {
					console.log('111');
					$('#popup-checkout-button').prop('disabled', false);
				} else {
					console.log('222');
					$('#popup-checkout-button').prop('disabled', true);
				}*/
			}, 500);
		});
	}
}
function cartRemove(product_id){
	$('#header_alert .header__alert-message').html('Подтвердить удаление товара');
	$('#header_alert .header__alert-button span').text('Подтвердить');
	$('#header_alert .header__alert-button').attr("href", 'javascript:void(0);');
	$('#header_alert .header__alert-button').attr("onclick", 'cartRemoved('+product_id+')');
	$('#header_alert').show();
	setTimeout(function () {
		$('#header_alert').hide();
	}, 4000);
}
function cartRemoved(product_id){
	$('#remove-product-'+product_id).click();
	$('#header_alert').hide();
}
$( 'body' ).on('click', '.carousel-nav .icon-arrow-left', function() {
	$( this ).closest( '.carousel-wrap' ).find( '.slick-slider' ).slick( 'slickPrev' );
});

$( 'body' ).on('click', '.carousel-nav .icon-arrow-right', function() {
	$( this ).closest( '.carousel-wrap' ).find( '.slick-slider' ).slick( 'slickNext' );
});
$(document).on('click','#popup-purchase-wrapper .close', function(){
	// $('#popup-purchase-wrapper').remove();
	setTimeout(function() {
		$('#popup-purchase-wrapper').remove();
	},1000);
});
$(document).on('change','#purchase-form [name=notifications]', function(){
	console.log('!!!');
	if (this.checked) {
		$('#popup-checkout-button').prop('disabled', false);
	} else {
		$('#popup-checkout-button').prop('disabled', true);
	}
});
$(document).ready(function() {

	console.log(getCookie("cookie_attention"));

	var show =true;

	var strGET = window.location.search.replace( '?', '');
	if (strGET.indexOf('ad=1') !== -1) {
		var show = false
		attention('yes');
	};

	console.log(show);

	if (show) {
		if (getCookie("cookie_attention") == null || getCookie("cookie_attention") == undefined || getCookie("cookie_attention") == 'no') {
			$('#atention').modal({
				keyboard: true,
				backdrop: 'static'
			});
		}
	}


	if($('#quick-register [name=notifications]:checked')){
		$('#quick-register button').prop('disabled',true);
	}else{
		$('#quick-register button').prop('disabled', false);
	}
	$(document).on('change','#quick-register [name=notifications]',function () {
		if(this.checked){
			$('#quick-register button').prop('disabled', false);
		}else{
			$('#quick-register button').prop('disabled',true);
		}
	});
	if($('#purchase-form [name=notifications]:checked')){
		$('#purchase-form button').prop('disabled',true);
	}else{
		$('#purchase-form button').prop('disabled', false);
	}
	$(document).on('change','#purchase-form [name=notifications]',function () {
		if(this.checked){
			$('#purchase-form button').prop('disabled', false);
		}else{
			$('#purchase-form button').prop('disabled',true);
		}
	});

	$(document).on('click','#forgot-pass',function () {
		$('#profileModal').modal('hide');
		$('#formForgotten').modal('show');
	});
	$(document).on('click','#formForgotten button',function (e) {
		e.preventDefault();
		// $('#profileModal').modal('hide');
		// $('#formForgotten').modal('show');
		$.ajax({
			type: 'post',
			url: 'index.php?route=account/forgotten/action_validate',
			dataType: 'json',
			data: $('#forgotten__form').serialize(),
			success: function (json) {
				$('#forgotten__form').find('label.error').remove();
				if(json.status === 'error'){
					if (json['errors']) {
						for (var key in json['errors']) {
							id = $('#forgotten__form').find('[name='+key+']').attr('id');
							$('#forgotten__form').find('[for=' + id +']').after($('#forgotten__form').find('[for=' + id +']').clone().addClass('error'));
							$('#forgotten__form').find('[for=' + id +'].error span').text(json.errors[key]);
						}
					}
				} else if (json.status === 'success') {
					$('#forgotten__form').find("input").removeClass('filled');
					$('#forgotten__form').find("input[type=text]").val("");
					$('#thankYou .page-heading-simple__subtitle').html(json.subtitle);
					$('#thankYou .page-heading-simple__title').html(json.title);
					$('#thankYou .modal-body__text').html(json.message);
					$('#formForgotten').modal('hide');
					$('#thankYou').modal('show');
				}

			}
		});
	});
	$(document).on('focus', 'input', function () {
		$(this).parent('.input-wrap').find('label.error').remove();
	});
	$(document).on('click', '#popup-checkout-button', function (e) {
		e.preventDefault();
		$.ajax({
			type: 'post',
			url: 'index.php?route=module/popup_purchase/make_order',
			dataType: 'json',
			data: $('#purchase-form').serialize(),
			success: function (json) {
				var id;
				$('#purchase-form').find('label.error').remove();
				if (json['error']) {
					if (json['error']['field']) {
						// masked('#popup-purchase-wrapper', false);
						// $('.text-danger').remove();
						$.each(json['error']['field'], function (i, val) {
							id = $('#purchase-form').find('[name='+i+']').attr('id');
							$('#purchase-form').find('[for=' + id +']').after($('#purchase-form').find('[for=' + id +']').clone().addClass('error'));
							$('#purchase-form').find('[for=' + id +'].error span').text(val);
							// $('[name="' + i + '"]').addClass('error_style').after('<div class="text-danger">' + val + '</div>');
						});
					}

					if (json['error']['option']) {
						for (i in json['error']['option']) {
							masked('#popup-purchase-wrapper', false);
							$('.required .text-danger').remove();
							var element = $('#input-option' + i.replace('_', '-'));
							element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						}
					}

					if (json['error']['recurring']) {
						$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
					}

				} else {
					if (json['output']) {
						$('#popup-purchase-wrapper').find("input").removeClass('filled');
						$('#popup-purchase-wrapper').find("input[type=text], input[type=tel]").val("");
						$('#thankYou .page-heading-simple__subtitle').html(json['subtitle']);
						$('#thankYou .page-heading-simple__title').html(json['title']);
						$('#thankYou .modal-body__text').html(json['output']);
						$('#popup-purchase-wrapper').modal('hide');
						$('#thankYou').modal('show');

					}
				}
			}
		});
	});
	// Highlight any found errors
	$('.text-danger').each(function() {
		var element = $(this).parent().parent();

		if (element.hasClass('form-group')) {
			element.addClass('has-error');
		}
	});

	// Currency
	$('#currency .currency-select').on('click', function(e) {
		e.preventDefault();

		$('#currency input[name=\'code\']').attr('value', $(this).attr('name'));

		$('#currency').submit();
	});

	// Language
	$('#language a').on('click', function(e) {
		e.preventDefault();

		$('#language input[name=\'code\']').attr('value', $(this).attr('href'));

		$('#language').submit();
	});

	/* Search */
	$('#search input[name=\'search\']').parent().find('button').on('click', function() {
		url = $('base').attr('href') + 'index.php?route=product/search';

		var value = $('header input[name=\'search\']').val();

		if (value) {
			url += '&search=' + encodeURIComponent(value);
		}

		location = url;
	});

	$('#search input[name=\'search\']').on('keydown', function(e) {
		if (e.keyCode == 13) {
			$('header input[name=\'search\']').parent().find('button').trigger('click');
		}
	});

	// Menu
	$('#menu .dropdown-menu').each(function() {
		var menu = $('#menu').offset();
		var dropdown = $(this).parent().offset();

		var i = (dropdown.left + $(this).outerWidth()) - (menu.left + $('#menu').outerWidth());

		if (i > 0) {
			$(this).css('margin-left', '-' + (i + 5) + 'px');
		}
	});

	// Product List
	/*$('#list-view').click(function() {
		$('#content .product-grid > .clearfix').remove();

		//$('#content .product-layout').attr('class', 'product-layout product-list col-xs-12');
		$('#content .row > .product-grid').attr('class', 'product-layout product-list col-xs-12');

		localStorage.setItem('display', 'list');
	});

	// Product Grid
	$('#grid-view').click(function() {
		// What a shame bootstrap does not take into account dynamically loaded columns
		cols = $('#column-right, #column-left').length;

		if (cols == 2) {
			$('#content .product-list').attr('class', 'product-layout product-grid col-lg-6 col-md-6 col-sm-12 col-xs-12');
		} else if (cols == 1) {
			$('#content .product-list').attr('class', 'product-layout product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12');
		} else {
			$('#content .product-list').attr('class', 'product-layout product-grid col-lg-3 col-md-3 col-sm-6 col-xs-12');
		}

		 localStorage.setItem('display', 'grid');
	});

	if (localStorage.getItem('display') == 'list') {
		$('#list-view').trigger('click');
	} else {
		$('#grid-view').trigger('click');
	}*/

	// Checkout
	$(document).on('keydown', '#collapse-checkout-option input[name=\'email\'], #collapse-checkout-option input[name=\'password\']', function(e) {
		if (e.keyCode == 13) {
			$('#collapse-checkout-option #button-login').trigger('click');
		}
	});

	// tooltips on hover
	// $('[data-toggle=\'tooltip\']').tooltip({container: 'body',trigger: 'hover'});

	// Makes tooltips work on ajax generated content
	// $(document).ajaxStop(function() {
	// 	$('[data-toggle=\'tooltip\']').tooltip({container: 'body'});
	// });

	$("body").on("click",".catalog-view span",function(e){
		$(".catalog-view span").removeClass("active");
		$(this).addClass("active");
		$(".catalog-container").removeClass("x4 x3 x2");
		$(".catalog-container").addClass($(this).data("view"));
		localStorage.setItem('display', $(this).data("view"));
	});
	function fix_display() {
		var display = localStorage.getItem('display');
		if(!display){
			display = 'x4';
		}
		$('.catalog-view span[data-view="' + display + '"]').addClass("active");
		$(".catalog-container").removeClass("x4 x3 x2");
		$(".catalog-container").addClass(display);
	}
	fix_display();
});

// Cart add remove functions
var cart = {
	'add': function(product_id, quantity) {
		$.ajax({
			url: 'index.php?route=checkout/cart/add',
			type: 'post',
			data: 'product_id=' + product_id + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('loading');
			},
			complete: function() {
				$('#cart > button').button('reset');
			},
			success: function(json) {
				$('.alert, .text-danger').remove();

				if (json['redirect']) {
					location = json['redirect'];
				}

				if (json['success']) {
					/*$('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

					// Need to set timeout otherwise it wont update the total
					setTimeout(function () {
						$('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
					}, 100);

					//$('html, body').animate({ scrollTop: 0 }, 'slow');

					// $('#cart > ul').load('index.php?route=common/cart/info ul li');*/
					// $('#header_alert .section__title').html('Мы благодарим за Ваш выбор!');
					if(json['total']){
						$('#cart').attr('href', 'index.php?route=checkout/cart');
						if(!$('#cart').hasClass('active')){
							$('#cart').addClass('active');
						}
					}
					$('#header_alert .header__alert-message').html(json['success']);
					$('#header_alert .header__alert-button span').text('Оформить заказ');
					$('#header_alert .header__alert-button').attr("href", "index.php?route=checkout/simplecheckout");
					$('#header_alert').show();
					setTimeout(function () {
						$('#header_alert').hide();
					}, 5000);
					// $('#formSuccess').modal('show');
					$('#cart').load('index.php?route=common/cart/info #cart > *');
				}
			},
	        error: function(xhr, ajaxOptions, thrownError) {
	            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	        }
		});
	},
	'update': function(key, quantity) {
		$.ajax({
			url: 'index.php?route=checkout/cart/edit',
			type: 'post',
			data: 'key=' + key + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('loading');
			},
			complete: function() {
				$('#cart > button').button('reset');
			},
			success: function(json) {
				// Need to set timeout otherwise it wont update the total
				setTimeout(function () {
					// console.log('111');
					// $('#cart').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
					$('#cart').load('index.php?route=common/cart/info #cart > *');
				}, 100);

				/*if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
					location = 'index.php?route=checkout/cart';
				} else {
					$('#cart').load('index.php?route=common/cart/info #cart > *');
				}*/
			},
	        error: function(xhr, ajaxOptions, thrownError) {
	            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	        }
		});
	},
	'remove': function(key) {
		$.ajax({
			url: 'index.php?route=checkout/cart/remove',
			type: 'post',
			data: 'key=' + key,
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('loading');
			},
			complete: function() {
				$('#cart > button').button('reset');
			},
			success: function(json) {
				// Need to set timeout otherwise it wont update the total
				setTimeout(function () {
					$('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
				}, 100);

				if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
					location = 'index.php?route=checkout/cart';
				} else {
					$('#cart > ul').load('index.php?route=common/cart/info ul li');
				}
			},
	        error: function(xhr, ajaxOptions, thrownError) {
	            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	        }
		});
	}
}

var voucher = {
	'add': function() {

	},
	'remove': function(key) {
		$.ajax({
			url: 'index.php?route=checkout/cart/remove',
			type: 'post',
			data: 'key=' + key,
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('loading');
			},
			complete: function() {
				$('#cart > button').button('reset');
			},
			success: function(json) {
				// Need to set timeout otherwise it wont update the total
				setTimeout(function () {
					$('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
				}, 100);

				if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
					location = 'index.php?route=checkout/cart';
				} else {
					$('#cart > ul').load('index.php?route=common/cart/info ul li');
				}
			},
	        error: function(xhr, ajaxOptions, thrownError) {
	            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	        }
		});
	}
}

var wishlist = {
	'add': function(product_id) {
		$.ajax({
			url: 'index.php?route=account/wishlist/add',
			type: 'post',
			data: 'product_id=' + product_id,
			dataType: 'json',
			success: function(json) {
				// $('.alert').remove();

				if (json['redirect']) {
					location = json['redirect'];
				}

				if (json['success']) {
					$('#wishlist-total span').html(json['total']);
					$('#wishlist-total').attr('href', 'index.php?route=account/wishlist');
					if(json['total']){
						if(!$('#wishlist-total').hasClass('active')){
							$('#wishlist-total').addClass('active');
						}
					}

					var list, index;
					list = document.getElementsByClassName('wishlist-'+product_id);
					for (index = 0; index < list.length; ++index) {
						list[index].removeAttribute('onclick');
						list[index].setAttribute('class','active');
					}

					$('#header_alert .header__alert-message').html(json['success']);
					$('#header_alert .header__alert-button span').text('Список желаний');
					$('#header_alert .header__alert-button').attr("href", json['url']);
					$('#header_alert').show();
					setTimeout(function () {
						$('#header_alert').hide();
					}, 5000);
				}
				// if (json['success']) {
				// 	$('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
				// }
				//
				// $('#wishlist-total span').html(json['total']);
				// $('#wishlist-total').attr('title', json['total']);
				//
				// $('html, body').animate({ scrollTop: 0 }, 'slow');
			},
	        error: function(xhr, ajaxOptions, thrownError) {
	            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	        }
		});
	},
	'remove': function() {

	}
}

var compare = {
	'add': function(product_id) {
		$.ajax({
			url: 'index.php?route=product/compare/add',
			type: 'post',
			data: 'product_id=' + product_id,
			dataType: 'json',
			success: function(json) {

				if (json['success']) {
					$('#compare-total span').html(json['total']);
					$('#compare-total').attr('href', 'index.php?route=product/compare');
					if(json['total']){
						if(!$('#compare-total').hasClass('active')){
							$('#compare-total').addClass('active');
						}
					}

					var list, index;
					list = document.getElementsByClassName('compare-'+product_id);
					for (index = 0; index < list.length; ++index) {
						list[index].removeAttribute('onclick');
						list[index].setAttribute('class','active');
					}
					console.log(json['url']);

					$('#header_alert .header__alert-message').html(json['success']);
					$('#header_alert .header__alert-button span').text('Сравнение');
					$('#header_alert .header__alert-button').attr("href", json['url']);
					$('#header_alert').show();
					setTimeout(function () {
						$('#header_alert').hide();
					}, 5000);
				}
				/*$('.alert').remove();

				if (json['success']) {
					$('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

					$('#compare-total').html(json['total']);

					$('html, body').animate({ scrollTop: 0 }, 'slow');
				}*/
			},
	        error: function(xhr, ajaxOptions, thrownError) {
	            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	        }
		});
	},
	'remove': function() {

	}
}

/* Agree to Terms */
$(document).delegate('.agree', 'click', function(e) {
	e.preventDefault();

	$('#modal-agree').remove();

	var element = this;

	$.ajax({
		url: $(element).attr('href'),
		type: 'get',
		dataType: 'html',
		success: function(data) {
			html  = '<div id="modal-agree" class="modal">';
			html += '  <div class="modal-dialog">';
			html += '    <div class="modal-content">';
			html += '      <div class="modal-header">';
			html += '        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
			html += '        <h4 class="modal-title">' + $(element).text() + '</h4>';
			html += '      </div>';
			html += '      <div class="modal-body">' + data + '</div>';
			html += '    </div';
			html += '  </div>';
			html += '</div>';

			$('body').append(html);

			$('#modal-agree').modal('show');
		}
	});
});

// Autocomplete */
(function($) {
	$.fn.autocomplete = function(option) {
		return this.each(function() {
			this.timer = null;
			this.items = new Array();

			$.extend(this, option);

			$(this).attr('autocomplete', 'off');

			// Focus
			$(this).on('focus', function() {
				this.request();
			});

			// Blur
			$(this).on('blur', function() {
				setTimeout(function(object) {
					object.hide();
				}, 200, this);
			});

			// Keydown
			$(this).on('keydown', function(event) {
				switch(event.keyCode) {
					case 27: // escape
						this.hide();
						break;
					default:
						this.request();
						break;
				}
			});

			// Click
			this.click = function(event) {
				event.preventDefault();

				value = $(event.target).parent().attr('data-value');

				if (value && this.items[value]) {
					this.select(this.items[value]);
				}
			}

			// Show
			this.show = function() {
				var pos = $(this).position();

				$(this).siblings('ul.dropdown-menu').css({
					top: pos.top + $(this).outerHeight(),
					left: pos.left
				});

				$(this).siblings('ul.dropdown-menu').show();
			}

			// Hide
			this.hide = function() {
				$(this).siblings('ul.dropdown-menu').hide();
			}

			// Request
			this.request = function() {
				clearTimeout(this.timer);

				this.timer = setTimeout(function(object) {
					object.source($(object).val(), $.proxy(object.response, object));
				}, 200, this);
			}

			// Response
			this.response = function(json) {
				html = '';

				if (json.length) {
					for (i = 0; i < json.length; i++) {
						this.items[json[i]['value']] = json[i];
					}

					for (i = 0; i < json.length; i++) {
						if (!json[i]['category']) {
							html += '<li data-value="' + json[i]['value'] + '"><a href="#">' + json[i]['label'] + '</a></li>';
						}
					}

					// Get all the ones with a categories
					var category = new Array();

					for (i = 0; i < json.length; i++) {
						if (json[i]['category']) {
							if (!category[json[i]['category']]) {
								category[json[i]['category']] = new Array();
								category[json[i]['category']]['name'] = json[i]['category'];
								category[json[i]['category']]['item'] = new Array();
							}

							category[json[i]['category']]['item'].push(json[i]);
						}
					}

					for (i in category) {
						html += '<li class="dropdown-header">' + category[i]['name'] + '</li>';

						for (j = 0; j < category[i]['item'].length; j++) {
							html += '<li data-value="' + category[i]['item'][j]['value'] + '"><a href="#">&nbsp;&nbsp;&nbsp;' + category[i]['item'][j]['label'] + '</a></li>';
						}
					}
				}

				if (html) {
					this.show();
				} else {
					this.hide();
				}

				$(this).siblings('ul.dropdown-menu').html(html);
			}

			$(this).after('<ul class="dropdown-menu"></ul>');
			$(this).siblings('ul.dropdown-menu').delegate('a', 'click', $.proxy(this.click, this));

		});
	}
})(window.jQuery);
