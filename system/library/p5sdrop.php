<?php
final class P5sdrop
{
    private $testMode = 0;
    private $ApiKey = '5c5987a483cf84.56629944';


    public function __construct($registry)
    {
        $this->config = $registry->get('config');
        $this->db = $registry->get('db');
        $this->request = $registry->get('request');
        $this->session = $registry->get('session');

    }

    public function Curl($url) {

        $ch = curl_init ($url) ;
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1) ;
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec ($ch) ;

        curl_close ($ch) ;
        return $result;
    }

    public function addOrderToP2s($params = array())
    {

        wss_log('addOrderToP2s');
        wss_log(print_r($params,1));
        $service_url = 'http://api.ds-platforma.ru/ds_order.php';
        $service_url .= '?'.http_build_query($params);
        $result = $this->curl($service_url);
        wss_log('addOrderToP2s - result');
        wss_log(print_r($result,1));

        return $result;
    }

    public function GetOrderToP2s($params = array())
    {
        wss_log('GetOrderToP2s');
        wss_log(print_r($params,1));
        $service_url = 'http://api.ds-platforma.ru/ds_get_order_data.php';
        $service_url .= '?'.http_build_query($params);

      //  echo $service_url; exit;

        $result = $this->curl($service_url);
        return $result;
    }

}