<?php

    if (!function_exists('wss_log')) {
        function wss_log($message,$file_name = 'wss_log',$file_type='log'){
            $file = DIR_LOGS . $file_name.'.'.$file_type;
            $handle = fopen($file, 'a');

            flock($handle, LOCK_EX);
            if(is_array($message) || is_object($message)){
                fwrite($handle, date('Y-m-d G:i:s') . "\n" .print_r($message, true). "\n");
            }else{
                fwrite($handle, date('Y-m-d G:i:s') . ' - ' . $message. "\n");
            }
            fflush($handle);
            flock($handle, LOCK_UN);
            fclose($handle);
        }
    }

    if (!function_exists('wss_validate')) {
        function wss_validate($string = "", $filter = 2) {
            $filters = array(
                0 => FILTER_VALIDATE_BOOLEAN,
                1 => FILTER_VALIDATE_INT,
                2 => FILTER_VALIDATE_EMAIL,
                3 => FILTER_VALIDATE_URL
            );

            return filter_var($string, $filters[$filter]);
        }
    }

?>